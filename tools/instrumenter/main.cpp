/*******************************************************************************
 * This file is distributed under the MIT License. See LICENSE.TXT for details.
 *******************************************************************************/

#include "instGenerator.h"
#include "preprocessing.h"
#include "splitBasicBlock.h"

#include "llvm/PassManager.h"
#include "llvm/IRReader/IRReader.h"
#include "llvm/Support/CommandLine.h"
#include "llvm/Support/Debug.h"
#include "llvm/Support/ManagedStatic.h"
#include "llvm/Support/PrettyStackTrace.h"
#include "llvm/Support/Signals.h"
#include "llvm/Support/SourceMgr.h"
#include "llvm/Support/FileSystem.h"
#include "llvm/Support/ToolOutputFile.h"
#include "llvm/Bitcode/ReaderWriter.h"
#include "llvm/Analysis/LoopInfo.h"
#include "llvm/CodeGen/Passes.h"

llvm::cl::opt<std::string>
InputFilename(
    llvm::cl::Positional,
    llvm::cl::desc("<input LLVM bitcode file>"),
    llvm::cl::Required, llvm::cl::value_desc("filename"));

static llvm::cl::opt<std::string>
OutputFilename("o",
    llvm::cl::desc("Override output filename"),
    llvm::cl::init(""),
    llvm::cl::value_desc("filename"));

static llvm::cl::opt<std::string>
DefaultDataLayout("default-data-layout",
    llvm::cl::desc("data layout string to use if not specified by module"),
    llvm::cl::init(""),
    llvm::cl::value_desc("layout-string"));


// removes extension from filename if there is one
std::string getFileName(const std::string &str) {
  std::string filename {str};
  size_t lastdot = str.find_last_of(".");
  if (lastdot != std::string::npos)
    filename = str.substr(0, lastdot);
  return filename;
}

int main(int argc, char **argv) {
  llvm::llvm_shutdown_obj shutdown;  // calls llvm_shutdown() on exit
  llvm::cl::ParseCommandLineOptions(argc, argv, "LLSPLAT - Bug Finder for C Programs\n");

  llvm::sys::PrintStackTraceOnErrorSignal();
  llvm::PrettyStackTraceProgram PSTP(argc, argv);
  llvm::EnableDebugBuffering = true;

  if (OutputFilename.empty()) {
    OutputFilename = getFileName(InputFilename) + ".instr.bc";
  }

  std::error_code error_msg;
  llvm::SMDiagnostic err;
  std::unique_ptr<llvm::Module> module = parseIRFile(InputFilename, err, llvm::getGlobalContext());

  if (!module) {
    if (llvm::errs().has_colors()) llvm::errs().changeColor(llvm::raw_ostream::RED);
    llvm::errs() << "error: " << "Bitcode was not properly read; " << err.getMessage() << "\n";
    if (llvm::errs().has_colors()) llvm::errs().resetColor();
    return EXIT_FAILURE;
  }

  if (error_msg) {
    if (llvm::errs().has_colors()) llvm::errs().changeColor(llvm::raw_ostream::RED);
    llvm::errs() << "error: OutputFile " << error_msg.message() << "\n";
    if (llvm::errs().has_colors()) llvm::errs().resetColor();
    return EXIT_FAILURE;
  }

  ///////////////////////////////
  // initialize and run passes //
  ///////////////////////////////

  llvm::PassManager pass_manager;
  llvm::PassRegistry &Registry = *llvm::PassRegistry::getPassRegistry();
  initializeAnalysis(Registry);

  // add an appropriate DataLayout instance for the module
  const llvm::DataLayout *dl = nullptr;
  const std::string &moduleDataLayout = module.get()->getDataLayoutStr();
  if (!moduleDataLayout.empty())
    dl = new llvm::DataLayout(moduleDataLayout);
  else if (!DefaultDataLayout.empty())
    dl = new llvm::DataLayout(moduleDataLayout);
  if (dl) {
    module->setDataLayout(dl);
    pass_manager.add(new llvm::DataLayoutPass());
  }

  pass_manager.add(llvm::createUnreachableBlockEliminationPass());
  pass_manager.add(new llsplat::SplitBasicBlock());
  pass_manager.add(new llsplat::Preprocessing());
  pass_manager.add(new llvm::LoopInfo());

  std::unique_ptr<llvm::tool_output_file> output(
      new llvm::tool_output_file(OutputFilename.c_str(), error_msg, llvm::sys::fs::F_None));

  pass_manager.add(new llsplat::InstGenerator());

  pass_manager.run(*module);
  llvm::WriteBitcodeToFile(module.get(), output->os());
  output->keep();

  return EXIT_SUCCESS;
}
