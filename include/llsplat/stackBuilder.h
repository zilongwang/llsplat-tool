/*******************************************************************************
 * This file is distributed under the MIT License. See LICENSE.TXT for details.
 *******************************************************************************/

#ifndef STACKBUILDER_H_
#define STACKBUILDER_H_

#include "instGenerator.h"
#include "llvm/IR/InstVisitor.h"
#include "llvm/IR/IRBuilder.h"

namespace llsplat {

class StackBuilder : public llvm::InstVisitor<StackBuilder> {
public:
  StackBuilder(llvm::Module& m, llvm::LLVMContext& context,
      llvm::IRBuilder<>& builder, std::map<FuncType, llvm::Function*>& fmap) :
    mod_(m),
    context_(context),
    builder_(builder),
    fmap_(fmap) {
  }

public:
  void visitBinaryOperator(llvm::BinaryOperator& I);
  void visitAllocaInst(llvm::AllocaInst& I);
  void visitLoadInst(llvm::LoadInst& I);
  void visitGetElementPtrInst(llvm::GetElementPtrInst& I);
  void visitPHINode(llvm::PHINode& I);
  void visitSelectInst(llvm::SelectInst& I);

  void visitTruncInst(llvm::TruncInst& I);
  void visitZExtInst(llvm::ZExtInst& I);
  void visitSExtInst(llvm::SExtInst& I);
  void visitPtrToIntInst(llvm::PtrToIntInst& I);
  void visitIntToPtrInst(llvm::IntToPtrInst& I);
  void visitBitCastInst(llvm::BitCastInst &I);


  void visitICmpInst(llvm::ICmpInst& I);
//  void visitCastInst(llvm::CastInst& I);
  void visitCallInst(llvm::CallInst& I);
  void visitInstruction(llvm::Instruction& I);

  void visitValue(llvm::Value& V);
  void visitConstant(llvm::Constant& C);
  void visitArgument(llvm::Argument& A);

private:
  llvm::Module& mod_;
  llvm::LLVMContext& context_;
  llvm::IRBuilder<>& builder_;
  std::map<FuncType, llvm::Function*>& fmap_;
};


}  // namespace llsplat


#endif /* STACKBUILDER_H_ */
