/*******************************************************************************
 * This file is distributed under the MIT License. See LICENSE.TXT for details.
 *******************************************************************************/
#ifndef SRC_INCLUDE_LLSPLAT_Z3UTILS_H_
#define SRC_INCLUDE_LLSPLAT_Z3UTILS_H_

#include <z3++.h>
#include <vector>

z3::expr getBigOr(std::vector<z3::expr>& v, z3::context& ctx);

z3::expr getBigAnd(std::vector<z3::expr>& v, z3::context& ctx);

#endif /* SRC_INCLUDE_LLSPLAT_Z3UTILS_H_ */
