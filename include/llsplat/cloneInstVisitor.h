/*******************************************************************************
 * This file is distributed under the MIT License. See LICENSE.TXT for details.
 *******************************************************************************/
#ifndef SRC_INCLUDE_LLSPLAT_CLONEINSTVISITOR_H_
#define SRC_INCLUDE_LLSPLAT_CLONEINSTVISITOR_H_


#include "instGenerator.h"
#include "llvm/IR/InstVisitor.h"
#include "llvm/IR/IRBuilder.h"
namespace llsplat {

//class VarInfoBuilderHelper: public llvm::InstVisitor<VarInfoBuilderHelper> {
//public:
//  VarInfoBuilderHelper(llvm::Module& m,
//      llvm::IRBuilder<>& builder,
//      std::map<FuncType, llvm::Function*>& fmap,
////      const llvm::SmallVector<llvm::BasicBlock*, 16>& EDom
//  ) :
//    mod_(m),
//    context_(m.getContext()),
//    builder_(builder),
//    fmap_(fmap),
//    resultInst_(nullptr),
//    ptr_size_(m.getDataLayout()->getPointerSizeInBits()),
////    EDom_(EDom),
//    numOfAddr_(0)
//
//{
////    cloner_(mod_, builder_);
//
//}
//
//public:
//  void visitBinaryOperator(llvm::BinaryOperator& I);
//  void visitAllocaInst(llvm::AllocaInst&);
//  void visitLoadInst(llvm::LoadInst&);
//  void visitStoreInst(llvm::StoreInst&);
//  void visitGetElementPtrInst(llvm::GetElementPtrInst& I);
//  void visitPHINode(llvm::PHINode& I);
//  void visitSelectInst(llvm::SelectInst& I);
//
//
//  void visitTruncInst(llvm::TruncInst& I);
//  void visitZExtInst(llvm::ZExtInst& I);
//  void visitSExtInst(llvm::SExtInst& I);
//  void visitPtrToIntInst(llvm::PtrToIntInst &I);
//  void visitBitCastInst(llvm::BitCastInst &I);
////  void visitCastInst(llvm::CastInst& I);
//
//  void visitICmpInst(llvm::ICmpInst& I);
//  void visitInstruction(llvm::Instruction& I);
//
//  void visitValue(llvm::Value& V);
//  void visitConstant(llvm::Constant& c);
//  void visitArgument(llvm::Argument& a);
//
//private:
//  llvm::Module& mod_;
//  llvm::LLVMContext& context_;
//  llvm::IRBuilder<> builder_;
//  std::map<FuncType, llvm::Function*>& fmap_;
//  llvm::Value* resultInst_;
//  std::string varName_;
//  unsigned ptr_size_;
////  const llvm::SmallVector<llvm::BasicBlock*, 16>& EDom_;
////  std::map<llvm::BasicBlock*, unsigned> num_of_visited_;
//  int numOfAddr_;
//
//  std::set<std::string> visited_;
////  CloneInstVisitor cloner_;
//
//};


/////////////////////// CloneInstVisitor ///////////////////////////////////


class CloneInstVisitor: public llvm::InstVisitor<CloneInstVisitor> {
public:
  CloneInstVisitor(llvm::Module& m,
      llvm::IRBuilder<>& builder) :
    mod_(m),
    context_(m.getContext()),
    builder_(builder),
    ptr_size_(m.getDataLayout()->getPointerSizeInBits()),
    last_cloned_inst_(nullptr)
    {
    }

public:
  void visitBinaryOperator(llvm::BinaryOperator& I);
  void visitAllocaInst(llvm::AllocaInst& I);
  void visitLoadInst(llvm::LoadInst& I);
  void visitGetElementPtrInst(llvm::GetElementPtrInst& I);
  void visitPHINode(llvm::PHINode& I);
  void visitSelectInst(llvm::SelectInst& I);

  void visitTruncInst(llvm::TruncInst& I);
  void visitZExtInst(llvm::ZExtInst& I);
  void visitSExtInst(llvm::SExtInst& I);

  void visitICmpInst(llvm::ICmpInst& I);
  void visitCastInst(llvm::CastInst& I);
  void visitCallInst(llvm::CallInst& I);
  void visitInstruction(llvm::Instruction& I);

  void visitValue(llvm::Value& V);
  void visitConstant(llvm::Constant& c);
  void visitArgument(llvm::Argument& a);

  llvm::Value* getLastClonedInst() { return last_cloned_inst_; }
private:
  llvm::Module& mod_;
  llvm::LLVMContext& context_;
  llvm::IRBuilder<> builder_;
  unsigned ptr_size_;

  llvm::Value* last_cloned_inst_;
};


}  // namespace llsplat


#endif /* SRC_INCLUDE_LLSPLAT_CLONEINSTVISITOR_H_ */
