/*******************************************************************************
 * This file is distributed under the MIT License. See LICENSE.TXT for details.
 *******************************************************************************/

#ifndef SRC_INCLUDE_LLSPLAT_VARINFOBUILDER_H_
#define SRC_INCLUDE_LLSPLAT_VARINFOBUILDER_H_

#include "instGenerator.h"
//#include "varinfoBuilderHelper.h"
#include "llvm/IR/InstVisitor.h"
#include "llvm/IR/IRBuilder.h"
#include "irUtils.h"

namespace llsplat {

class VarInfoBuilder: public llvm::InstVisitor<VarInfoBuilder> {
public:
  VarInfoBuilder(llvm::Module& m,
      llvm::IRBuilder<>& builder,
      std::map<FuncType, llvm::Function*>& fmap,
      const llvm::SmallVector<llvm::BasicBlock*, 16>& EDom
  ) :
    EDom_(EDom)
//    helper_(m, builder, fmap, EDom)
  {
//    for (auto& BB : EDom_)
//      num_of_visited_.insert(std::make_pair(BB, 0));


  }

public:
  void visitBranchInst(llvm::BranchInst& I);
  void visitSwitchInst(llvm::SwitchInst& I);
  void visitStoreInst(llvm::StoreInst&);
  void visitBasicBlock(llvm::BasicBlock* BB);

private:
  const llvm::SmallVector<llvm::BasicBlock*, 16>& EDom_;
//  std::map<llvm::BasicBlock*, unsigned> num_of_visited_;
  VarInfoBuilderHelper helper_;
};

}  // namespace llsplat

#endif /* SRC_INCLUDE_LLSPLAT_VARINFOBUILDER_H_ */
