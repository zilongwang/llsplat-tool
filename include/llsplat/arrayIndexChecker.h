/*
 * addressNameFinder.h
 *
 *  Created on: May 22, 2015
 *      Author: zilong
 */

#ifndef SRC_INCLUDE_LLSPLAT_ARRAYINDEXCHECKER_H_
#define SRC_INCLUDE_LLSPLAT_ARRAYINDEXCHECKER_H_
#include "baseTypes.h"
#include "llvm/IR/InstVisitor.h"

class ArrayIndexChecker : public llvm::InstVisitor<ArrayIndexChecker> {
public:
  ArrayIndexChecker(
      std::map<index_t, address_t>& idx2addr,
      std::map<address_t, int>& addr2version,
      llvm::SmallVector<llvm::Value*, 32>& ptr_value_vec
  ) :
    idx2addr_(idx2addr),
    addr2version_(addr2version),
    ptr_value_vec_(ptr_value_vec)
    {
    }

public:
  void visitBinaryOperator(llvm::BinaryOperator& I);
  void visitAllocaInst(llvm::AllocaInst&);
  void visitLoadInst(llvm::LoadInst&);
  void visitStoreInst(llvm::StoreInst&);
  void visitGetElementPtrInst(llvm::GetElementPtrInst& I);
  void visitPHINode(llvm::PHINode& I);
  void visitSelectInst(llvm::SelectInst& I);

  void visitTruncInst(llvm::TruncInst& I);
  void visitZExtInst(llvm::ZExtInst& I);
  void visitSExtInst(llvm::SExtInst& I);

  void visitICmpInst(llvm::ICmpInst& I);
  void visitCastInst(llvm::CastInst& I);
  void visitInstruction(llvm::Instruction& I);

  void visitValue(llvm::Value& V);
  void visitConstant(llvm::Constant& c);
  void visitArgument(llvm::Argument& a);

private:
  std::map<index_t, address_t>& idx2addr_;
  std::map<address_t, int>& addr2version_;
  llvm::SmallVector<llvm::Value*, 32>& ptr_value_vec_;
};


#endif /* SRC_INCLUDE_LLSPLAT_ARRAYINDEXCHECKER_H_ */
