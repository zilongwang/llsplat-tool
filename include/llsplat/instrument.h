/*******************************************************************************
 * This file is distributed under the MIT License. See LICENSE.TXT for details.
 *******************************************************************************/
#ifndef _INSTRUMENT_H
#define _INSTRUMENT_H

#include "baseTypes.h"

#ifdef __cplusplus
extern "C" {
#endif
  void llsplatInitInput(address_t);
  void llsplatInitInputInternal(address_t, width_t);
  void llsplatPushVar(address_t, bv_t, width_t);
  void llsplatPushC(bv_t, width_t);
  void llsplatPushBop(binop_t);
  void llsplatPushICmp(bv_t bv, width_t nbits, predicate_t);
  void llsplatPushSExt(bv_t, width_t);
  void llsplatPushZExt(bv_t, width_t);
  void llsplatPushTrunc(bv_t, width_t);
  void llsplatPushPtrToInt(bv_t, width_t, width_t);
  void llsplatPushSelect();
  void llsplatPushPhi(instruction_t);
  void llsplatPushLocalAddrRange(address_t, address_t);
  void llsplatPopLocalAddrRange();
  void llsplatUpdatePhiMap(instruction_t);
  void llsplatUpdateSymbolicStore(address_t);
  void llsplatAddPathConstraint(bool, const char*, const char*);
  void llsplatAddPathConstraintSwitch(index_t, const char*, const char*);
  void llsplatSolve();

  // BMC related functions
  void llsplatStartBMC(const char*, const char*, const char*);
  void llsplatEndBMC(const char*, const char*, const char*);
  void llsplatSaveVarInfo(index_t, address_t, bv_t, width_t);

  // Coverage related functions
  void llsplatUpdateBranchCoverage(const char*, const char*, index_t, index_t, bool);

#ifdef __cplusplus
}
#endif

#endif
