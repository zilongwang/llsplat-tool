#ifndef SRC_INCLUDE_LLSPLAT_BMCBUILDERHELPER_H_
#define SRC_INCLUDE_LLSPLAT_BMCBUILDERHELPER_H_

#include "baseTypes.h"
#include "llvm/IR/InstVisitor.h"
#include "state.h"
#include "z3++.h"
#include <string>
#include <map>

class BmcBuilderHelper: public llvm::InstVisitor<BmcBuilderHelper> {
public:
  BmcBuilderHelper(z3::context& context, unsigned bmc_no,
		  std::map<index_t, address_t>& idx2addr,
		  std::map<address_t, std::string>& addr2varname,
		  std::map<address_t, std::unique_ptr<VarInfo>>& addr2varinfo,
			std::map<address_t, int>& addr2version,
			llvm::SmallVector<llvm::Value*, 32>& ptr_value_vec,
			std::map<llvm::BasicBlock*, z3::expr>& ctx_map_edom,
		  std::map<llvm::BasicBlock*, std::vector<z3::expr>>& ctx_expr_map_edom,
		  std::map<llvm::BasicBlock*, std::vector<z3::expr>>& ctx_expr_map_dest,
		  std::map<llvm::BasicBlock*, std::vector<z3::expr>>& formula_map_edom,
		  llvm::SmallVector<llvm::BasicBlock *, 16>& SortedEDom,
		  const llvm::BasicBlock* Dominator,
		  const z3::expr& ctx
      ) :
    context_(context), bmc_no_(bmc_no),
    result_(context.bool_val(true)),
		idx2addr_(idx2addr),
		addr2varname_(addr2varname),
    addr2varinfo_(addr2varinfo),
		addr2version_(addr2version),
		ptr_value_vec_(ptr_value_vec),
		ctx_map_edom_(ctx_map_edom),
		ctx_expr_map_edom_(ctx_expr_map_edom),
		ctx_expr_map_dest_(ctx_expr_map_dest),
		formula_map_edom_(formula_map_edom),
		SortedEDom_(SortedEDom),
    Dominator_(Dominator),
    ctx_(ctx)
    {
    }

public:
  void visitAllocaInst(llvm::AllocaInst&);
  void visitGetElementPtrInst(llvm::GetElementPtrInst& I);
  void visitSelectInst(llvm::SelectInst& I);

  void visitBinaryOperator(llvm::BinaryOperator& I);
  void visitICmpInst(llvm::ICmpInst& I);
  void visitLoadInst(llvm::LoadInst&);

  void visitTruncInst(llvm::TruncInst& I);
  void visitZExtInst(llvm::ZExtInst& I);
  void visitSExtInst(llvm::SExtInst& I);

  void visitSwitchInst(llvm::SwitchInst& I);
  void visitBranchInst(llvm::BranchInst& I);
  void visitStoreInst(llvm::StoreInst& I);

  void visitValue(llvm::Value& V);
  void visitConstant(llvm::Constant& c);
  void visitArgument(llvm::Argument& a);

  const z3::expr getAndResetResult() { z3::expr ret{result_}; result_ = context_.bool_val(true); return ret; }
//  index_t getNumOfAddr() { return numOfAddr_; }
  void printIdx2Name() {
    for (auto& kvp : idx2name_)
      std::cout << "// " << addr2varname_[idx2addr_[kvp.first]] << " --> " << kvp.second << "\n";
  }



private:
  const unsigned bmc_no_;
  z3::context& context_;
	std::map<index_t, address_t>& idx2addr_;
  std::map<address_t, std::unique_ptr<VarInfo>>& addr2varinfo_;
	std::map<address_t, std::string>& addr2varname_;
  std::map<address_t, int>& addr2version_;
	llvm::SmallVector<llvm::Value*, 32>& ptr_value_vec_;
  std::map<llvm::BasicBlock*, z3::expr>& ctx_map_edom_;
  std::map<llvm::BasicBlock*, std::vector<z3::expr>>& ctx_expr_map_edom_;
  std::map<llvm::BasicBlock*, std::vector<z3::expr>>& ctx_expr_map_dest_;
  std::map<llvm::BasicBlock*, std::vector<z3::expr>>& formula_map_edom_;
  llvm::SmallVector<llvm::BasicBlock *, 16>& SortedEDom_;
  const llvm::BasicBlock* Dominator_;
  const z3::expr& ctx_;
  std::map<index_t, std::string> idx2name_;
  z3::expr result_;
};

#endif /* SRC_INCLUDE_LLSPLAT_BMCBUILDERHELPER_H_ */
