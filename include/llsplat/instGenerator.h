/*******************************************************************************
 * This file is distributed under the MIT License. See LICENSE.TXT for details.
 *******************************************************************************/

#ifndef INSTGENERATOR_H_
#define INSTGENERATOR_H_

#include "llvm/IR/InstVisitor.h"
#include "llvm/Analysis/LoopInfo.h"
#include <set>

namespace llsplat {
typedef enum {
  PushVar,
  PushC,
  PushBop,
  PushZExt,
  PushSExt,
  PushTrunc,
  PushPtrToInt,
  PushICmp,
  UpdateSymbolicStore,
  AddPathConstraint,
  Solve,
  StartBMC,
  EndBMC,
  SaveVarInfo,
  UpdatePhiMap,
  PushPhi,
  PushSelect,
  AddPathConstraintSwitch,
  UpdateBranchCoverage,
  PushLocalAddrRange,
  PopLocalAddrRange,
  SaveGlovalVar
} FuncType;

class InstGenerator : public llvm::InstVisitor<InstGenerator>, public llvm::ModulePass {
public:
  static char ID; // Pass identification, replacement for typeid
  InstGenerator() : llvm::ModulePass(ID) {}

public:
  virtual void getAnalysisUsage(llvm::AnalysisUsage& AU) const {
    AU.setPreservesCFG();
    AU.addRequired<llvm::LoopInfo>();
  }

  bool runOnModule(llvm::Module& m);

  void visitReturnInst(llvm::ReturnInst& I);
  void visitBranchInst(llvm::BranchInst& I);
  void visitSwitchInst(llvm::SwitchInst& I);
  void visitPHINode(llvm::PHINode& I);
  void visitSelectInst(llvm::SelectInst& I);

  void visitStoreInst(llvm::StoreInst& I);
  void visitCmpInst(llvm::CmpInst& I);

  void visitCallInst(llvm::CallInst& I);

  void visitFunction(llvm::Function& Fn);
  void visitInstruction(llvm::Instruction& I);

private:
  void init_(llvm::Module& );
  void makePushVar_();
  void makePushC_();
  void makePushBop_();
  void makePushICmp_();
  void makePushZExt_();
  void makePushSExt_();
  void makePushTrunc_();
  void makePushPtrToInt_();
  void makePushSelect_();
  void makePushPhi_();
  void makeUpdatePhiMap_();
  void makeUpdateSymbolicStore_();
  void makeAddPathConstraint_();
  void makeAddPathConstraintSwitch_();
  void makeSolve_();
  void makeBMC_();
  void makeUpdateBranchCoverage_();

  void makeLocalAddrRange_();

  // Coverage related functions
  bool hasReturnUsageWithinBlock_(llvm::Instruction& I, llvm::BasicBlock& BB);
  bool hasNoCondBranchUsageWithinBlock_(llvm::Instruction& I, llvm::BasicBlock& BB);
  void coverageBranchInst(llvm::BranchInst& I);
  void coverageSwitchInst(llvm::SwitchInst& I);
  void coverageSelectInst(llvm::SelectInst& I);
  void coveragePHINode(llvm::PHINode& I);


  // BMC related functions
  void tryBMC_(llvm::BasicBlock* BB);
  void instrumentBMC_(llvm::Function*, llvm::BasicBlock*,
      llvm::SmallVector<llvm::BasicBlock *, 16>&, std::set<llvm::BasicBlock*>&);
  //
private:
  llvm::Module* mod_ = nullptr;
  llvm::LLVMContext* context_ = nullptr;
  std::map<FuncType, llvm::Function*> fmap_;
  unsigned ptr_size_;
  unsigned total_br_;


private: // helper functions
  bool hasAllocaInst_(llvm::BasicBlock& BB);
};

}  // namespace llsplat



#endif /* INSTGENERATOR_H_ */
