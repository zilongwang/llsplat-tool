/*******************************************************************************
 * This file is distributed under the MIT License. See LICENSE.TXT for details.
 *******************************************************************************/
#ifndef _STATE_H_
#define _STATE_H_
#include "symstack.h"
#include "pipe.h"
#include <string>
#include <vector>
#include <map>
#include <set>
#include <chrono>
#include <iostream>
#include <fstream>
#include <z3++.h>
#include "baseTypes.h"
#include "llvm/IR/BasicBlock.h"
#include "llvm/IR/Module.h"
#include "llvm/IR/LLVMContext.h"
#include "llvm/IRReader/IRReader.h"
#include "llvm/Support/SourceMgr.h"
#include "llvm/Support/raw_ostream.h"

extern llvm::cl::opt<bool> Enable_BMC;
extern llvm::cl::opt<std::string> InputFileName;
extern llvm::cl::opt<int> MaxLength;
class BranchInfo {
public:
  BranchInfo(bool branch, llvm::BasicBlock* BB) :
    is_bmc(false), is_switch(false), branch(branch), done(false), dominator(nullptr), to_cover(nullptr),
    current_case(0), to_cover_case(0), BB(BB)
  {

  }
  BranchInfo(llvm::BasicBlock* dominator, std::vector<llvm::BasicBlock*>& Dests_vec) :
    is_bmc(true), is_switch(false), branch(false), done(false), dominator(dominator), to_cover(nullptr),
    current_case(0), to_cover_case(0), BB(dominator)
  {
    for (auto& dest : Dests_vec)
      Dests.insert(std::make_pair(dest, false));
  }
  BranchInfo(index_t current_case, std::vector<z3::expr>& vec, llvm::BasicBlock* BB) :
    is_bmc(false), is_switch(true), branch(false), done(false), dominator(nullptr), to_cover(nullptr),
    current_case(current_case), to_cover_case(0), BB(BB)
  {
    index_t idx = 0;
    for (auto e = vec.begin(), E = vec.end(); e != E; ++e, ++idx) {
      switchmap.push_back(std::make_pair(current_case == idx, *e));
    }
  }

  void print() {
    cout << "/////////////////////\n";
    cout << "is_bmc = " << is_bmc << "\n";
    cout << "is_switch = " << is_switch << "\n";
    cout << "branch = " << branch << "\n";
    cout << "in function " << BB->getParent()->getName().data()
        << "'s basic block " << BB->getName().data() << "\n";

    if (is_bmc) {
      cout << "dominator = " << dominator->getName().data() << "\n";
      cout << "in function " << dominator->getParent()->getName().data() << "\n";
      cout << "to_cover = " << to_cover->getName().data() << "\n";
    }

  }
public:
  bool is_bmc; // Indicate if we handle the conditional in the normal way or in the BMC way.
  bool is_switch; // Indicate if we handle a switch case in the normal case

  bool branch; // used in normal way only.

  llvm::BasicBlock* dominator; // used in BMC way only.
  llvm::BasicBlock* to_cover; // used in BMC way only.
  std::map<llvm::BasicBlock*, bool> Dests; // used in BMC way only.
  std::map<llvm::BasicBlock*, std::vector<z3::expr>> ctxmap; // used in BMC way only.

  bool done; // used in BOTH ways.
  std::vector<std::pair<bool, z3::expr>> switchmap; // used to handle switch inst only.
  index_t to_cover_case; // used to handle switch inst only.
  index_t current_case;

  // members for debugging
  llvm::BasicBlock* BB;
};

class VarInfo {
public:
  VarInfo(address_t addr, bv_t bv, width_t nbits) : addr(addr), bv(bv), nbits(nbits) {}
  address_t addr;
  bv_t bv;
  width_t nbits;
};

class StateManager {
public:
  static StateManager* getStateManager();
  z3::expr getSymbolicExpr(address_t);
  bool existSymbolicExpr(address_t);
  void insertToSymbolicStore(address_t, z3::expr);
  void eraseFromSymbolicStore(address_t);

  void addPathConstraint(z3::expr);
  void addPathConstraint(z3::expr, z3::expr);

  void compareCurrentPathWithPrediction(bool, std::string, std::string);
  void compareCurrentPathWithPrediction(index_t, index_t, std::vector<z3::expr>&, std::string, std::string);

  bool isInLogicInputMap(index_t);
  bv_t getValueInLogicInputMap(index_t);
  void updateLogicInputMap(index_t, bv_t);

  bv_t getRamdomNumber(width_t);
  z3::context& getContext();
  llsplat_status_t solve();
  void printStatistics();
  unsigned getIterations();
  unsigned incrementIterations();
  index_t getSymCnt();

  z3::expr bvToZ3Expr(bv_t bv, width_t nbits) {
    return getStateManager()->getContext().bv_val(bv, nbits);
  }

  void reachMaxLength() {
    if (MaxLength >= 0 && path_constraint_.size() > MaxLength)
      throw solve();
  }
  void startTimer();
  void endTimer();
  void updatePhiMap(instruction_t i, SymNode*);

  std::map<instruction_t, SymNode*>& getPhiMap();

  void pushLocalAddrRange(address_t start, address_t end);
  void popLocalAddrRange();
  void prepareForNextIteration();

  void setValue(address_t address, bv_t bv, width_t nbits);

  // BMC related function
  llvm::BasicBlock* getCurrDominator();
  llvm::BasicBlock* getBasicBlock(llvm::StringRef fn, llvm::StringRef bb);
  void coverDestination(llvm::BasicBlock* Dest);

  void doBMC(llvm::BasicBlock* Dominator);
  llvm::Module& getModule() {return *mod_;}
  int getBmcNum() {return bmc_no_;}
  std::string getInitialSymbol() {return "llsplatX_"+to_string(++fresh_var_index_);}
  void incrementBMCNum() {bmc_no_++;}
  void startBMC(llvm::BasicBlock*, std::vector<llvm::BasicBlock*>& Dests);
  void endBMC();
  bool isDoingBMC();
  void saveOldSymbolicExpr(address_t addr);
  void restoreOldSymbolicExpr();
  void addInitFormula(const z3::expr& e) {init_part_formula_.push_back(e);}


private:
  StateManager();
  void negateLast(size_t last);
  index_t exprToLogicalAddress(std::string e);

  // BMC related functions
  bool coveredAllOtherDestinations_(llvm::BasicBlock* Dest);

  // functions that deal with switch cases
  bool coveredAllOtherCases_(index_t current_case);

  // Debugging functions for lldb
public:
  void print_branch_history(size_t i);
  void print_z3_expr(z3::expr e);
public:
  static const std::string sym_prefix_;


private:
  std::map<index_t, bv_t> logical_input_map_;
  std::vector<std::pair<z3::expr, z3::expr>> path_constraint_;
  std::vector<std::pair<address_t, address_t>> local_address_stack_;
  std::map<address_t, z3::expr> symbolic_store_;
  std::map<address_t, z3::expr> old_symbolic_store_;
  std::set<address_t> addr_to_be_deleted_when_bmc_fails_;
  std::vector<std::unique_ptr<BranchInfo>> branch_history_;
  z3::context context_;
  unsigned runs_;
  unsigned fresh_no_;
  unsigned sym_cnt_;
  std::chrono::steady_clock::time_point start_;
  std::chrono::steady_clock::time_point end_;
  std::map<instruction_t, SymNode*> phi_map_;

  // BMC related members
  llvm::BasicBlock* currDominator_;
  std::unique_ptr<llvm::Module> mod_;
  bool doing_bmc_;
  unsigned bmc_no_;
  std::unique_ptr<BranchInfo> currBranchInfo_;
  std::map<llvm::BasicBlock*, z3::expr> ctx_map_edom_;
  std::map<llvm::BasicBlock*, std::vector<z3::expr>> ctx_expr_map_edom_;
  std::map<llvm::BasicBlock*, std::vector<z3::expr>> ctx_expr_map_dest_;
  std::map<llvm::BasicBlock*, std::vector<z3::expr>> formula_map_edom_;
  std::vector<z3::expr> init_part_formula_;

  std::map<llvm::BasicBlock*, llvm::SmallVector<llvm::BasicBlock *, 16> > dominatorVisited_;
  // For debugging only.
  std::vector<std::pair<z3::expr, z3::expr>> old_path_constraint_;


public:
  std::map<index_t, address_t> idx2addr_;
  std::map<address_t, std::string> addr2varname_;
  std::map<address_t, std::unique_ptr<VarInfo>> addr2varinfo_;
  std::map<address_t, int> addr2version_;
  int fresh_var_index_;



// Coverage related members and methods
public:
  void updateBranchCoverageStatEntry(llvm::BasicBlock*, index_t, index_t, bool);
  int getBranchCovered();
  void updateBMCBranches(llvm::BasicBlock*, llvm::BasicBlock*);

  std::vector<std::pair<z3::expr, z3::expr>>& getPathConstraint();
  std::map<llvm::BasicBlock*, std::map<index_t, bool> > branch_coverage_map_;
private:
  void printBranchCoverage_();
  void inBMCPath_(llvm::BasicBlock* Dominator,
                                llvm::BasicBlock* Dest,
                                std::vector<llvm::BasicBlock*>& visit_queue,
                                const llvm::SmallVector<llvm::BasicBlock *, 16>& EDom);
  void updateBMCBranchCoverageTable_(std::vector<llvm::BasicBlock*>&);
  int total_br_;

public:
  Pipe _pipe;
  void reportToParentProcess(llsplat_status_t status);
  void readBranchCoverageFromChildProcess(void* buf, size_t length_of_branch_coverage_map, size_t number_of_bytes_to_read);
  void readInputsFromChildProcess(void* buf, size_t number_of_inputs, size_t number_of_bytes_to_read);
  void readBranchHistoryFromChildProcess(void* buf, size_t length_of_history, size_t number_of_bytes_to_read);
  llsplat_status_t waiting();
  void resetParent();


// Garbage collection for the singleton instance
private:
  static StateManager* _instance;
  class GC {
  public:
    ~GC() {
      if (StateManager::_instance != NULL) {
        delete StateManager::_instance;
      }
    }
  };

  static GC gc;
};

inline void StateManager::startTimer() {
  start_ = std::chrono::steady_clock::now();
}

inline void StateManager::endTimer() {
  end_ = std::chrono::steady_clock::now();
}

inline StateManager* StateManager::getStateManager() {
  if(_instance == nullptr) {
    _instance = new StateManager();
  }
  return _instance;
}

inline z3::context& StateManager::getContext() {
  return context_;
}


// @Pre: addr may or may not be already in symbolic_store_. Thus we don't know if addr is global
// and need the third argument.
inline void StateManager::insertToSymbolicStore(address_t addr, z3::expr expr) {
  symbolic_store_.erase(addr);
  symbolic_store_.insert(std::make_pair(addr, expr));
}

inline void StateManager::eraseFromSymbolicStore(address_t addr) {
  symbolic_store_.erase(addr);
}

inline bool StateManager::existSymbolicExpr(address_t addr) {
  return symbolic_store_.find(addr) != symbolic_store_.end();
}

inline void StateManager::addPathConstraint(z3::expr e) {
  path_constraint_.push_back(std::make_pair(e, context_.bool_val(true)));
}

inline void StateManager::addPathConstraint(z3::expr e1, z3::expr e2) {
  path_constraint_.push_back(std::make_pair(e1, e2));
}

inline unsigned StateManager::getIterations() {
  return runs_;
}

inline unsigned StateManager::incrementIterations() {
  return runs_++;
}

inline void StateManager::resetParent() {
  logical_input_map_.clear();
  branch_history_.clear();
  branch_coverage_map_.clear();
}

inline index_t StateManager::getSymCnt() {
  return sym_cnt_++;
}

inline bool StateManager::isInLogicInputMap(index_t unique_id) {
  return logical_input_map_.find(unique_id) != logical_input_map_.end();
}

inline bv_t StateManager::getValueInLogicInputMap(index_t unique_id) {
  return logical_input_map_[unique_id];
}

inline void StateManager::updateLogicInputMap(index_t unique_id, bv_t bv) {
  logical_input_map_[unique_id] = bv;
}

inline void StateManager::negateLast(size_t last) {
  path_constraint_[last].first = !path_constraint_[last].first;
}



inline z3::expr StateManager::getSymbolicExpr(address_t addr) {
  auto iter = symbolic_store_.find(addr);
  assert(iter != symbolic_store_.end() && "Cannot the symbolic expr of the addr ");
  return iter->second;
}

inline llvm::BasicBlock* StateManager::getCurrDominator() {
  return currDominator_;
}

inline void StateManager::startBMC(llvm::BasicBlock* Dominator, std::vector<llvm::BasicBlock*>& Dests) {
  currDominator_ = Dominator;
  doing_bmc_ = true;

//  compareCurrentPathWithPrediction(Dominator, Dests);
//  std::string FnName = Dominator->getParent()->getName().data();
//  std::string BBName = Dominator->getName().data();
  currBranchInfo_ = std::unique_ptr<BranchInfo>(new BranchInfo(Dominator, Dests));
  try {
//    if (llvm::outs().has_colors()) llvm::outs().changeColor(llvm::raw_ostream::GREEN);
//    llvm::outs() << "Trying to BMC for dominator basic block " << Dominator->getName()
//        << " in function " << Dominator->getParent()->getName() << "\n";
//    if (llvm::outs().has_colors()) llvm::outs().resetColor();

    doBMC(Dominator);
//    if (llvm::outs().has_colors()) llvm::outs().changeColor(llvm::raw_ostream::GREEN);
//    llvm::outs() << "Successfully do BMC for dominator basic block " << Dominator->getName()
//        << " in function " << Dominator->getParent()->getName() << "\n";
//    if (llvm::outs().has_colors()) llvm::outs().resetColor();
  } catch (bmc_status_t& status) {
    if (llvm::outs().has_colors()) llvm::outs().changeColor(llvm::raw_ostream::BLUE);
    llvm::outs() << "Back to pure concolic for dominator basic block " << Dominator->getName()
        << " in function " << Dominator->getParent()->getName() << "\n";
    if (llvm::outs().has_colors()) llvm::outs().resetColor();
    restoreOldSymbolicExpr();
    endBMC();
  }
}

inline void StateManager::endBMC() {
  currDominator_ = nullptr;
  doing_bmc_ = false;
  fresh_var_index_ = 1;
  idx2addr_.clear();
  addr2version_.clear();
  addr2varinfo_.clear();
  addr2varname_.clear();
  old_symbolic_store_.clear();
  addr_to_be_deleted_when_bmc_fails_.clear();
  ctx_map_edom_.clear();
  ctx_expr_map_edom_.clear();
  ctx_expr_map_dest_.clear();
  formula_map_edom_.clear();
  init_part_formula_.clear();

}

inline bool StateManager::isDoingBMC() {
  return doing_bmc_;
}

inline StateManager::StateManager() : runs_(1), fresh_no_(1), sym_cnt_(1), bmc_no_(1),
start_(std::chrono::steady_clock::now()), currDominator_(nullptr),
mod_(nullptr),
currBranchInfo_(nullptr), fresh_var_index_(1),
doing_bmc_(false),
total_br_(0)
{
  // HACK: I should not have read the IR file in pure concolic testing. I did it for the sake of debugging.
  // If I test Enable_BMC, I'll get a seg fault in pure concolic testing mode.
//  if (Enable_BMC) {
    llvm::SMDiagnostic err;
    mod_ = llvm::parseIRFile(InputFileName+".bc", err, llvm::getGlobalContext());

    if (!mod_) {
      if (llvm::errs().has_colors()) llvm::errs().changeColor(llvm::raw_ostream::RED);
      throw std::runtime_error("error: Bitcode " + InputFileName+".bc" +
          " was not properly read;" + err.getMessage().data() + "\n");
      if (llvm::errs().has_colors()) llvm::errs().resetColor();
    }
//  }

  string line;
  std::ifstream covFile("coverage_"+InputFileName+".txt");
  if (covFile.is_open()) {
    getline(covFile, line);
    total_br_ = stoi(line);
    covFile.close();
  }
}

inline llvm::BasicBlock* StateManager::getBasicBlock(llvm::StringRef fn, llvm::StringRef bb) {
  llvm::Function* F = mod_->getFunction(fn);
  for (auto& BB : F->getBasicBlockList())
    if (BB.getName() == bb)
      return &BB;
  llvm::errs() << "fn = " << fn << ", bb = " << bb << "\n";
  llvm::llvm_unreachable_internal("BB must be found somewhere.");
}

inline void StateManager::updatePhiMap(instruction_t i, SymNode* node) {
  phi_map_[i] = node;
}

inline std::map<instruction_t, SymNode*>& StateManager::getPhiMap() {
  return phi_map_;
}

inline void StateManager::saveOldSymbolicExpr(address_t addr) {
  if (old_symbolic_store_.find(addr) != old_symbolic_store_.end())
    return;

  auto iter = symbolic_store_.find(addr);
  if (iter != symbolic_store_.end())
    old_symbolic_store_.insert(std::make_pair(addr, iter->second));
  else
    addr_to_be_deleted_when_bmc_fails_.insert(addr);
}

inline void StateManager::restoreOldSymbolicExpr() {
  for (auto& kvp : old_symbolic_store_)
    insertToSymbolicStore(kvp.first, kvp.second);
  for (auto& addr : addr_to_be_deleted_when_bmc_fails_)
    eraseFromSymbolicStore(addr);
}


inline void StateManager::pushLocalAddrRange(address_t start, address_t end) {
  if (start <= end)
    local_address_stack_.push_back(make_pair(start, end));
  else
    local_address_stack_.push_back(make_pair(end, start));

}

inline void StateManager::popLocalAddrRange() {
  const auto& lastIt = local_address_stack_.end() - 1;
  std::map<address_t, z3::expr>::iterator iter = symbolic_store_.begin();
  std::map<address_t, z3::expr>::iterator endIter = symbolic_store_.end();
  for(; iter != endIter; ) {
    if (lastIt->first <= iter->first && iter->first <= lastIt->second)  {
      symbolic_store_.erase(iter++);
    } else {
      ++iter;
    }
  }

  local_address_stack_.pop_back();
}

#endif
