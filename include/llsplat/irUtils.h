/*******************************************************************************
 * This file is distributed under the MIT License. See LICENSE.TXT for details.
 *******************************************************************************/

#ifndef SRC_INCLUDE_LLSPLAT_IRUTILS_H_
#define SRC_INCLUDE_LLSPLAT_IRUTILS_H_

#include "llvm/Analysis/CFG.h"
#include "llvm/IR/Instructions.h"
#include "llvm/Support/raw_ostream.h"
#include "llvm/Support/Debug.h"
#include "llvm/IR/Module.h"
#include "llvm/IR/Constants.h"
#include "llvm/ADT/APInt.h"

using llvm::BasicBlock;
using llvm::Function;
using llvm::SmallVector;
using llvm::errs;
using llvm::isa;
using llvm::cast;
using llvm::dyn_cast;
using llvm::CallInst;
using llvm::Instruction;
using llvm::Value;
using llvm::BinaryOperator;
using llvm::AllocaInst;
using llvm::LoadInst;
using llvm::GetElementPtrInst;
using llvm::PHINode;
using llvm::SelectInst;
using llvm::TruncInst;
using llvm::ZExtInst;
using llvm::SExtInst;
using llvm::ICmpInst;
using llvm::CastInst;
using llvm::CallInst;
using llvm::Instruction;
using llvm::Value;
using llvm::Constant;
using llvm::Argument;
using llvm::dbgs;
using llvm::ArrayRef;
using llvm::GlobalValue;
using llvm::GlobalVariable;
using llvm::ArrayType;
using llvm::ConstantInt;
using llvm::StringRef;
using llvm::IntegerType;
using llvm::ConstantDataArray;
using llvm::ConstantExpr;
using llvm::APInt;

bool isSuccessor(BasicBlock* FromBB, BasicBlock* ToBB);
//bool isAcyclic(Function& fn, SmallVector<BasicBlock*, 16>& v);
bool isSourceOrTargetOfBackEdge(Function& fn, BasicBlock& BB);
bool hasFnCall(BasicBlock* BB);
bool hasPhiNode(BasicBlock* BB);
int getNumPredecessors(BasicBlock* BB);
int getNumSuccessors(BasicBlock* BB);
int hasNoSuccessor(BasicBlock* BB);
SmallVector<BasicBlock *, 16>& getEffecitveDominance(SmallVector<BasicBlock *, 16>&DominatedBBs);
Instruction* getFirstNotOfPHiNode(BasicBlock* BB);
bool hasPointerAssignment(SmallVector<BasicBlock *, 16>&BBs);
Constant* createConstString(llvm::StringRef Name, llvm::Module* mod);

class TopologicalSort {
public:
  TopologicalSort(llvm::BasicBlock* Dominator,
      llvm::SmallVector<llvm::BasicBlock*, 16>& EDom) :
        EDom_(EDom)
{
    for (llvm::succ_iterator PI = succ_begin(Dominator), E = succ_end(Dominator); PI != E; ++PI)
      StartBBs_.push_back(*PI);

    for (auto& BB : EDom_)
      num_of_visited_.insert(std::make_pair(BB, 0));
  }
  llvm::SmallVector<llvm::BasicBlock*, 16> sort();

private:
  void visitBasicBlock(llvm::BasicBlock* BB);

private:
  const llvm::SmallVector<llvm::BasicBlock*, 16>& EDom_;
  llvm::SmallVector<llvm::BasicBlock*, 16> StartBBs_;
  std::map<llvm::BasicBlock*, unsigned> num_of_visited_;
  llvm::SmallVector<llvm::BasicBlock*, 16> result_;
};

#endif /* SRC_INCLUDE_LLSPLAT_IRUTILS_H_ */
