/*******************************************************************************
 * This file is distributed under the MIT License. See LICENSE.TXT for details.
 *******************************************************************************/
#ifndef _BASETYPES_
#define _BASETYPES_
#include <stdbool.h>
#include <stdint.h>

typedef enum Predicate {
  // Opcode              U L G E    Intuitive operation
  FCMP_FALSE =  0,  ///< 0 0 0 0    Always false (always folded)
  FCMP_OEQ   =  1,  ///< 0 0 0 1    True if ordered and equal
  FCMP_OGT   =  2,  ///< 0 0 1 0    True if ordered and greater than
  FCMP_OGE   =  3,  ///< 0 0 1 1    True if ordered and greater than or equal
  FCMP_OLT   =  4,  ///< 0 1 0 0    True if ordered and less than
  FCMP_OLE   =  5,  ///< 0 1 0 1    True if ordered and less than or equal
  FCMP_ONE   =  6,  ///< 0 1 1 0    True if ordered and operands are unequal
  FCMP_ORD   =  7,  ///< 0 1 1 1    True if ordered (no nans)
  FCMP_UNO   =  8,  ///< 1 0 0 0    True if unordered: isnan(X) | isnan(Y)
  FCMP_UEQ   =  9,  ///< 1 0 0 1    True if unordered or equal
  FCMP_UGT   = 10,  ///< 1 0 1 0    True if unordered or greater than
  FCMP_UGE   = 11,  ///< 1 0 1 1    True if unordered, greater than, or equal
  FCMP_ULT   = 12,  ///< 1 1 0 0    True if unordered or less than
  FCMP_ULE   = 13,  ///< 1 1 0 1    True if unordered, less than, or equal
  FCMP_UNE   = 14,  ///< 1 1 1 0    True if unordered or not equal
  FCMP_TRUE  = 15,  ///< 1 1 1 1    Always true (always folded)
  FIRST_FCMP_PREDICATE = FCMP_FALSE,
  LAST_FCMP_PREDICATE = FCMP_TRUE,
  BAD_FCMP_PREDICATE = FCMP_TRUE + 1,
  ICMP_EQ    = 32,  ///< equal
  ICMP_NE    = 33,  ///< not equal
  ICMP_UGT   = 34,  ///< unsigned greater than
  ICMP_UGE   = 35,  ///< unsigned greater or equal
  ICMP_ULT   = 36,  ///< unsigned less than
  ICMP_ULE   = 37,  ///< unsigned less or equal
  ICMP_SGT   = 38,  ///< signed greater than
  ICMP_SGE   = 39,  ///< signed greater or equal
  ICMP_SLT   = 40,  ///< signed less than
  ICMP_SLE   = 41,  ///< signed less or equal
  FIRST_ICMP_PREDICATE = ICMP_EQ,
  LAST_ICMP_PREDICATE = ICMP_SLE,
  BAD_ICMP_PREDICATE = ICMP_SLE + 1
} predicate_t;

typedef enum BinaryOps {
  #define  FIRST_BINARY_INST(N)             BinaryOpsBegin = N,
  #define  HANDLE_BINARY_INST(N, OPC, CLASS) OPC = N,
  #define  LAST_BINARY_INST(N)             BinaryOpsEnd = N+1
  #include "llvm/IR/Instruction.def"
} binop_t;

typedef enum {ProgramSafe=0, BugFound, Continue, Timeout, AssertFailure, UnknownBug} llsplat_status_t;
typedef enum {ArrayIndexIsNotConstant} bmc_status_t;

typedef uint64_t instruction_t;
typedef uint64_t address_t;
typedef uint64_t width_t;
#ifdef __linux
typedef unsigned long long bv_t;
#elif __APPLE__
typedef uint64_t bv_t;
#endif
typedef uint32_t index_t;
#endif

