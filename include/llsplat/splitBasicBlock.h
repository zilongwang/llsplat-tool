#ifndef SRC_INCLUDE_LLSPLAT_SPLITBASICBLOCK_H_
#define SRC_INCLUDE_LLSPLAT_SPLITBASICBLOCK_H_

#include "llvm/IR/IRBuilder.h"

namespace llsplat {

class SplitBasicBlock : public llvm::ModulePass {
public:
  static char ID; // Pass identification, replacement for typeid
  SplitBasicBlock() : llvm::ModulePass(ID) {}
  virtual void getAnalysisUsage(llvm::AnalysisUsage& AU) const {
    AU.addRequired<llvm::DataLayoutPass>();
  }

  virtual bool runOnModule(llvm::Module& m);
};

}  // namespace llsplat

#endif /* SRC_INCLUDE_LLSPLAT_SPLITBASICBLOCK_H_ */
