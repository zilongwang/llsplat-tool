/*******************************************************************************
 * This file is distributed under the MIT License. See LICENSE.TXT for details.
 *******************************************************************************/
#ifndef SRC_INCLUDE_LLSPLAT_POINTERVALUEEXTRACTOR_H_
#define SRC_INCLUDE_LLSPLAT_POINTERVALUEEXTRACTOR_H_



#include "instGenerator.h"
#include "irUtils.h"


class PointerValueExtractor: public llvm::InstVisitor<PointerValueExtractor> {
public:
  PointerValueExtractor(const llvm::SmallVector<llvm::BasicBlock*, 16>& EDom) :
    EDom_(EDom)
  {
  }

  llvm::SmallVector<llvm::Value*, 32> start();

private:
  const llvm::SmallVector<llvm::BasicBlock*, 16>& EDom_;
};


#endif /* SRC_INCLUDE_LLSPLAT_POINTERVALUEEXTRACTOR_H_ */
