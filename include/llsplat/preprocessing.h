/*******************************************************************************
 * This file is distributed under the MIT License. See LICENSE.TXT for details.
 *******************************************************************************/

#ifndef SRC_INCLUDE_LLSPLAT_PREPROCESSING_H_
#define SRC_INCLUDE_LLSPLAT_PREPROCESSING_H_

#include "splitBasicBlock.h"
#include "llvm/IR/InstVisitor.h"
#include "llvm/IR/IRBuilder.h"

namespace llsplat {

class Preprocessing : public llvm::ModulePass, public llvm::InstVisitor<Preprocessing> {
public:
  static char ID; // Pass identification, replacement for typeid
  Preprocessing() : llvm::ModulePass(ID) {}
  virtual void getAnalysisUsage(llvm::AnalysisUsage& AU) const {
    AU.setPreservesCFG();
    AU.addRequired<llvm::DataLayoutPass>();
    AU.addRequired<llsplat::SplitBasicBlock>();
  }

  virtual bool runOnModule(llvm::Module& m);

public:
  void visitCallInst(llvm::CallInst& i);

private:
  llvm::Module* mod_;
  llvm::LLVMContext* context_;
  llvm::Function* func_;

private:
  void init(llvm::Module& m);
  void unrollStruct_(llvm::IRBuilder<>& builder, llvm::Value* gep);
};

}  // namespace llsplat


#endif /* SRC_INCLUDE_LLSPLAT_PREPROCESSING_H_ */
