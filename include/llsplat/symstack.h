/*******************************************************************************
 * This file is distributed under the MIT License. See LICENSE.TXT for details.
 *******************************************************************************/
#ifndef _SYMSTACK_H
#define _SYMSTACK_H
#include <string>
#include <stack>
#include <memory>
#include <z3++.h>
#include "baseTypes.h"
using namespace std;



class SymStack;

class SymNode {
  friend class SymStack;

public:
  SymNode(bv_t, width_t, z3::expr);
  void print();
  z3::expr getSymExpr();
  bv_t getBV();
  width_t getNBits();
  bool isConcreteValue();
  bool isTrueOrFalse();

private:
  bv_t bv_;
  width_t nbits_;
  z3::expr sym_expr_;
};


class SymStack {
public:
  static SymStack* getSymStack();
  void push(SymNode *);
  SymNode *toppopNode();
  size_t size();
  ~SymStack();

private:
  SymStack();

  stack<SymNode*> sym_stack_;
  static SymStack* _instance;
  // Garbage collection for the singleton instance
  class GC {
  public:
    ~GC() {
      if (SymStack::_instance != NULL) {
        delete SymStack::_instance;
      }
    }
  };
  static GC _gc;
};


#endif
