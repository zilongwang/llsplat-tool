#ifndef _LLSPLAT_PIPE_H_
#define _LLSPLAT_PIPE_H_
#include <cstdio>
#include <cstdlib>
/* pipes are created *only* on calling reset, not on initialization.
*/
class Pipe {
  public:
    Pipe();
    ~Pipe();

    void setChild();
    void setParent();
    void closePipe();

    void read (void* , size_t);
    void write (void* , size_t);
    bool inIsEnd();
    void create();

  private:
    size_t pipeSize; // total memory piped
    size_t sz; // default read block
    int pipefd_c2d[2]; // pipefd[0] - read, pipefd[1] - write
    int pipefd_d2c[2]; // pipefd[0] - read, pipefd[1] - write
    FILE *out;
    FILE *in;
};

#endif
