# Configuration file for the 'lit' test runner.

import os
import sys
import platform
import subprocess

try:
   import lit.util
   import lit.formats
except ImportError:
   pass

# name: The name of this test suite.
config.name = 'LLSPLAT'

# testFormat: The test format to use to interpret tests.
config.test_format = lit.formats.ShTest(execute_external=False)

# suffixes: A list of file extensions to treat as test files
# Note this can be overridden by lit.local.cfg files
config.suffixes = ['.ll', '.c']

# test_source_root: The root path where tests are located.
config.test_source_root = os.path.dirname(__file__)

# test_exec_root: The root path where tests should be run.
llsplat_src_root = getattr(config, 'llsplat_src_root', None)
llsplat_obj_root = getattr(config, 'llsplat_obj_root', None)

if llsplat_obj_root is not None:
    config.test_exec_root = os.path.join(llsplat_obj_root, 'test')

# Tweak the PATH to include the tool dir.
if llsplat_obj_root is not None:
    llsplat_tools_dir = getattr(config, 'llsplat_tools_dir', None)
    if not llsplat_tools_dir:
        lit.fatal('No LLSPLAT tools dir set!')

    # Check LLVM tool directory
    llvm_tools_dir = getattr(config, 'llvm_tools_dir', None)
    if not llvm_tools_dir:
        lit.fatal('No LLVM tool directory set!')

    path = os.path.pathsep.join((llvm_tools_dir, llsplat_tools_dir, config.environment['PATH'] ))
    config.environment['PATH'] = path


# Propogate some environment variable to test environment.
def addEnv(name):
    if name in os.environ:
        config.environment[name] = os.environ[name]

addEnv('HOME')
addEnv('PWD')
addEnv('DYLD_LIBRARY_PATH')
addEnv('C_INCLUDE_PATH')
addEnv('CPLUS_INCLUDE_PATH')

# Check that the object root is known.
if config.test_exec_root is None:
    lit.fatal('test execution root not set!')

# Add substitutions from lit.site.cfg
subs = [ 'cc', 'cxx' ]
for name in subs:
    value = getattr(config, name, None)
    if value == None:
        lit.fatal('{0} is not set'.format(name))
    config.substitutions.append( ('%' + name, value))

# Set absolute paths and extra cmdline args for LLSPLAT's tools
config.substitutions.append( ("%llsplat", "llsplat"))
config.substitutions.append( ("%instrumenter", "instrumenter.exe"))

# Set %instrument
config.substitutions.append( ('%instrument', "{} {} {} {} {} {}".format(getattr(config, 'cc', None), \
    "-emit-llvm -O0 -S", \
    "-I", \
    config.llsplat_src_root + "/include/llsplat", \
    "-I", \
    config.llvm_include_dir)))

config.substitutions.append( ("%ARGS", r"-bmc %t"))

# Get LLSplat specific parameters passed on llvm-lit cmd line
# e.g. llvm-lit --param llsplat-opts=--help
try:
  lit.params
except AttributeError:
  llsplat_extra_params = lit_config.params.get('llsplat-opts',"")
else:
  llsplat_extra_params = lit.params.get('llsplat-opts',"")

if len(llsplat_extra_params) != 0:
    print("Passing extra llsplat command line args: {0}".format(llsplat_extra_params))

# Set %compile
proc = subprocess.Popen(['llvm-config', '--cxxflags', '--ldflags', '--libs', '--system-libs'], stdout=subprocess.PIPE)
output = proc.communicate()[0]
config.substitutions.append( ('%compile', "{} {} {} {} {} {}".format(getattr(config, 'cxx', None), \
    "%t.instr.bc", \
    config.llsplat_tools_dir + "/libllsplat.a", \
    output, \
    "-L" + config.z3_lib_dir + " -lz3", \
    "-o %t.exe")))


# Add feature for the LLVM version in use, so it can be tested in REQUIRES and
# XFAIL checks. We also add "not-XXX" variants, for the same reason.
known_llvm_versions = set(["2.9", "3.4", "3.6"])
current_llvm_version = "%s.%s" % (config.llvm_version_major,
                                  config.llvm_version_minor)
config.available_features.add("llvm-" + current_llvm_version)
for version in known_llvm_versions:
   if version != current_llvm_version:
      config.available_features.add("not-llvm-" + version)
