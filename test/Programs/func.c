// RUN: %instrument %s -o %t.ll
// RUN: %instrumenter %t.ll
// RUN: %compile
// RUN: %t.exe %ARGS | FileCheck %s

#include "instrument.h"
#include "assert.h"

int foo (int a, int b) {
  int res;
  if (a == b - 19) {
    res = a;
  }
  else {
    res = a;
  }
  return res;
}

void llsplatInstrumented(){
  int a;
  int b;
  llsplatInitInput((address_t) &a);
  llsplatInitInput((address_t) &b);

  int c = foo(a, b);

  assert (c != b - 19);
  return ;

  // CHECK: Llsplat: in 2th run  Instrumented code returned on abort.
  // CHECK: runs = 2
}

