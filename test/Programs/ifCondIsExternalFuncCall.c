// RUN: %instrument %s -o %t.ll
// RUN: %instrumenter %t.ll
// RUN: %compile
// RUN: %t.exe %ARGS | FileCheck %s
#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>

#include "instrument.h"
#include <assert.h>

void llsplatInstrumented(){
  char a;
  llsplatInitInput((address_t) &a);

  int b = 10;
  if (isalnum(a)) {
    b = 20;
  }
  else {
    b = 30;
  }

  return ;

  // CHECK: Llsplat is over. The program is safe. :-)
  // CHECK: runs = 1
}

