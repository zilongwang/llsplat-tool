// RUN: %instrument %s -o %t.ll
// RUN: %instrumenter %t.ll
// RUN: %compile
// RUN: %t.exe %ARGS | FileCheck %s

#include "instrument.h"
#include <assert.h>

void llsplatInstrumented(){
  int a;
  int b;
  llsplatInitInput((address_t) &a);
  llsplatInitInput((address_t) &b);

  a = (a == b - 19) ? b + 1 : b + 2;

  assert(a != b + 1);
  return ;

  // CHECK: Llsplat: in 2th run  Instrumented code returned on abort.
  // CHECK: runs = 2
}

