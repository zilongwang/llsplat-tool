// RUN: %instrument %s -o %t.ll
// RUN: %instrumenter %t.ll
// RUN: %compile
// RUN: %t.exe %ARGS | FileCheck %s

#include "instrument.h"
#include <assert.h>

int llsplatInstrumented(){
  int a;
  llsplatInitInput((address_t) &a);

  return (a == 1 || a == 2 || a == 3);

  // CHECK: Llsplat is over. The program is safe. :-)
  // CHECK: runs = 4
}

