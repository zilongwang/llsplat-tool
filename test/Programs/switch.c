// RUN: %instrument %s -o %t.ll
// RUN: %instrumenter %t.ll
// RUN: %compile
// RUN: %t.exe %ARGS | FileCheck %s


#include "instrument.h"
#include "assert.h"

void llsplatInstrumented(){
  int a;
  int b;
  llsplatInitInput((address_t) &a);
  llsplatInitInput((address_t) &b);

  switch (a) {
    case 1: {b = 10; break;}
    case 2: {b = 20; break;}
    case 3: {b = 30; break;}
    default: b = 0;
  }

  assert(b != 10);
  return ;
  // CHECK: Llsplat: in 2th run  Instrumented code returned on abort.
  // CHECK: runs = 2
}

