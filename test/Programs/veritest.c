// RUN: %instrument %s -o %t.ll
// RUN: %instrumenter %t.ll
// RUN: %compile
// RUN: %t.exe %ARGS | FileCheck %s

#include "instrument.h"
#include "assert.h"

#define N 100

void llsplatInstrumented(){

  char input[N];

  for (int i = 0; i < N; i++) {
    llsplatInitInput((address_t) &input[i]);
  }

  int counter = 0;

  for (int i = 0; i < N; i++) {
    if (input[i] == 'a') {
      counter++;
    }
  }

  if (counter == N-1)
    assert(0);
  return ;

  // CHECK: Llsplat: in 2th run  Instrumented code returned on abort.
  // CHECK: runs = 2
  // CHECK: covered 8 branches out of
}

