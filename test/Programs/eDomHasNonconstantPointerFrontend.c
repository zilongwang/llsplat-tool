// RUN: %instrument %s -o %t.ll
// RUN: %instrumenter %t.ll | FileCheck %s

#include "instrument.h"
#include <assert.h>

void llsplatInstrumented() {
  int a[3];

  for (int i = 0; i < 3; i++)
    llsplatInitInput((address_t) &a[i]);


  int idx = 0;
  int *p = &idx;
  a[1] = 20;
  if (a[*p] == 100) {
    p = &idx;
    a[*p+1] = 10;
  }

  assert(a[1] != 10);
  return ;


  // CHECK: EDominatedBBs has non-constant pointer.
}

