// RUN: %instrument %s -o %t.ll
// RUN: %instrumenter %t.ll
// RUN: %compile
// RUN: %t.exe %ARGS | FileCheck %s

#include "instrument.h"
#include <assert.h>

void llsplatInstrumented(){
  int a;
  int b;
  llsplatInitInput((address_t) &a);
  llsplatInitInput((address_t) &b);

  int *p = &a;

  *p = 30;
  if (p) {
    a = 20;
  }
  else {
    a = 20;
  }

  assert(a != 20);
  return ;

  // CHECK: Llsplat: in 1th run  Instrumented code returned on abort.
  // CHECK: runs = 1
}

