function(configure_lit_site_cfg input output)
  set(HOST_CC "${CMAKE_C_COMPILER} ${CMAKE_C_COMPILER_ARG1}")
  set(HOST_CXX "${CMAKE_CXX_COMPILER} ${CMAKE_CXX_COMPILER_ARG1}")

  set(LLSPLAT_SOURCE_DIR $ENV{LLSPLAT_SOURCE_DIR})
  set(LLSPLAT_BINARY_DIR $ENV{LLSPLAT_BUILD_DIR})
  set(LLSPLAT_TOOLS_DIR  $ENV{LLSPLAT_BUILD_DIR})
  set(LLVM_SOURCE_DIR ${LLVM_MAIN_SRC_DIR})
  set(LLVM_BINARY_DIR ${LLVM_BINARY_DIR})
  set(LLVM_INCLUDE_DIR  $ENV{LLVM_DIR}/install/include)
  set(LLVM_TOOLS_DIR $ENV{LLVM_DIR}/install/bin)
  set(Z3_LIB_DIR "$ENV{Z3_DIR}/install/lib")

  configure_file(${input} ${output} @ONLY)
endfunction()

# A raw function to create a lit target. This is used to implement the testuite
# management functions.
function(add_lit_target target comment)
  parse_arguments(ARG "PARAMS;DEPENDS;ARGS" "" ${ARGN})
  set(LIT_ARGS "\${LIT_ARGS} ${LLVM_LIT_ARGS}")
  separate_arguments(LIT_ARGS)
  if (LLVM_MAIN_SRC_DIR)
    set (LIT_COMMAND ${PYTHON_EXECUTABLE} ${LLVM_MAIN_SRC_DIR}/utils/lit/lit.py)
  else()
    find_program(LIT_COMMAND llvm-lit)
  endif ()
  list(APPEND LIT_COMMAND ${LIT_ARGS})

  if( ARG_DEPENDS )
    add_custom_target(${target}
      COMMAND ${LIT_COMMAND} ${ARG_DEFAULT_ARGS}
      COMMENT "${comment}"
      ${cmake_3_2_USES_TERMINAL}
      )
    add_dependencies(${target} ${ARG_DEPENDS})
  else()
    add_custom_target(${target}
      COMMAND ${CMAKE_COMMAND} -E echo "${target} does nothing, no tools built.")
    message(STATUS "${target} does nothing.")
  endif()

  # Tests should be excluded from "Build Solution".
  set_target_properties(${target} PROPERTIES EXCLUDE_FROM_DEFAULT_BUILD ON)
endfunction()

configure_lit_site_cfg(
  ${CMAKE_CURRENT_SOURCE_DIR}/lit.site.cfg.in
  ${CMAKE_CURRENT_BINARY_DIR}/lit.site.cfg
  )

# HACK: LLVM requires that a user specify a target dependency which I
# I do not need. However, in order to reuse LLVM's regression CMakeLists.txt,
# I just randomly specify the dependency below.
set(LLVM_TEST_DEPENDS
         instrumenter.exe
         llsplat
        )

add_lit_target(check "Running the LLSPLAT regression tests"
  ${CMAKE_CURRENT_BINARY_DIR}
  PARAMS llvm_site_config=${CMAKE_CURRENT_BINARY_DIR}/lit.site.cfg
  DEPENDS ${LLVM_TEST_DEPENDS}
  )
