LLSPLAT
===================


*LLSPLAT* is a conclic+BMC testing tool for C programs.

----------

Setup
-------------

*LLSPLAT* relies on the following packages

- Z3-4.3.2
- llvm-3.6.0
- cfe-3.6.0
- libcxx-3.6.0
- compiler-rt-3.6.0


We offer a script **build_llsplat.sh** under the directory `utils/`. It can download and install Z3, LLVM3.6 and *LLSPLAT* based on the settings in the script.  Software packages needed in the script include 

- g++
- git
- cmake-3.2 or higher

We recommend to set **INSTALL\_Z3**, **INSTALL\_LLVM** and **INSTALL\_LLSPLAT** flags to 1.

After the script finishes installation, it prints out some environmental variables in the terminal. Please copy  these environmental variables to `bashrc` . 

----------


Testing a program
-------------------



We offer a Makefile in the directory `examples/` to test a program using *LLSPLAT*.


As an example, to test a program `foo.c` in `examples/`, run

> cd examples

> make TARGET=foo

An executable **llsplat-foo.exe** is created in `examples/`. 

Then type in

> ./llsplat-foo.exe <OPTIONS> foo

Available options are:

- **-bmc**  enables *LLSPLAT* in concolic+BMC mode, if specified. Otherwise, *LLSPLAT* runs in pure concolic mode .

- **-max-len=<num>** specifies the length of the path constraint. If not specified, there is no limit to the length of the path constraint.

- **-max-iter=<num>** specifies the maximum number of program paths explored by *LLSPLAT*.  If not specified, there is no limit to the number of program paths to explore.





