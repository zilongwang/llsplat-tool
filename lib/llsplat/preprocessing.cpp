/*******************************************************************************
 * This file is distributed under the MIT License. See LICENSE.TXT for details.
 *******************************************************************************/

#define DEBUG_TYPE "frontend-preprocessing"
#include "preprocessing.h"
#include "llvm/Support/Debug.h"
#include "llvm/IR/IRBuilder.h"


using namespace llvm;

namespace llsplat {

void Preprocessing::unrollStruct_(IRBuilder<>& builder, Value* gep) {
  unsigned j = 0;
  const unsigned size = mod_->getDataLayout()->getPointerSizeInBits();
  Type* ty = gep->getType()->getContainedType(0);
  DEBUG(dbgs() << "ty = " << *ty << "\n");
  assert(isa<StructType>(ty) && "ty is not a structType.");
  for (auto field_ty : cast<StructType>(ty)->elements()) {
    DEBUG(dbgs() << "filed_ty = " << *field_ty << "\n");
    Value* new_gep = builder.CreateStructGEP(gep, j++);
    if (field_ty->isStructTy()) {
      unrollStruct_(builder, new_gep);
    } else {
      Value* pti = builder.CreatePtrToInt(new_gep, builder.getIntNTy(size));
      unsigned actural_size = mod_->getDataLayout()->getTypeSizeInBits(field_ty);
      Value* concrete_value = builder.getIntN(size, actural_size);

      ArrayRef<Value*> Args({pti, concrete_value});
      builder.CreateCall(func_, Args);
    }
  }
}

void  Preprocessing::visitCallInst(CallInst& I) {
  DEBUG(dbgs() << "Preprocessing: visiting CallInst " << I << "\n");
  // Replace llsplatInitInput with llspaltInitInputInternal.
  // 1. Unroll struct
  // 2. Add the number of bits each input (or struct's field) occupies.
  if (I.getCalledFunction()==NULL || I.getCalledFunction()->getName() != "llsplatInitInput")
    return;

  Value* arg = I.arg_operands().begin()->get();

  if (isa<PtrToIntInst>(arg)) {
    Value* ptr = cast<PtrToIntInst>(arg)->getPointerOperand();
    Type* ty = ptr->getType()->getContainedType(0);

    const unsigned size = mod_->getDataLayout()->getPointerSizeInBits();

    if (ty->isStructTy()) {
      IRBuilder<> builder(cast<PtrToIntInst>(arg));
      // ptr is the start address of the struct that is being unrolled.
      unrollStruct_(builder, ptr);
      I.eraseFromParent();
      cast<PtrToIntInst>(arg)->eraseFromParent();
    } else {
      IRBuilder<> builder(&I);
      unsigned actural_size = mod_->getDataLayout()->getTypeSizeInBits(ty);
      Value* concrete_value = builder.getIntN(size, actural_size);
      ArrayRef<Value*> Args({arg, concrete_value});
      builder.CreateCall(func_, Args);
      I.eraseFromParent();
    }
  } else if (ConstantExpr* CE = dyn_cast<ConstantExpr>(arg)) {
    if (!CE->isCast())
      llvm_unreachable("arg is neither a PtrToIntInst or a constantExpr");

    Value* ptr = CE->getOperand(0);
    Type* ty = ptr->getType()->getContainedType(0);

    const unsigned size = mod_->getDataLayout()->getPointerSizeInBits();

    if (ty->isStructTy()) {
//      IRBuilder<> builder(cast<PtrToIntInst>(arg));
      // ptr is the start address of the struct that is being unrolled.
//      unrollStruct_(builder, ptr);
//      I.eraseFromParent();
//      cast<PtrToIntInst>(arg)->eraseFromParent();
      llvm_unreachable("I assume global variables have simple types.");
    } else {
      IRBuilder<> builder(&I);
      unsigned actural_size = mod_->getDataLayout()->getTypeSizeInBits(ty);
      Value* concrete_value = builder.getIntN(size, actural_size);
      ArrayRef<Value*> Args({arg, concrete_value});
      builder.CreateCall(func_, Args);
      I.eraseFromParent();
    }

  } else {
    llvm_unreachable("arg is neither a PtrToIntInst or a constantExpr");
  }

  DEBUG(dbgs() << "Preprocessing: visited CallInst\n");
}

char Preprocessing::ID = 0;
RegisterPass<Preprocessing> P("Preprocessing", "Preprocessing", true, false);

void Preprocessing::init(Module& m) {
  mod_ = &m;
  context_ = &m.getContext();
  unsigned size = mod_->getDataLayout()->getPointerSizeInBits();
  Type* address_t = Type::getIntNTy(*context_, size);
  ArrayRef<Type*> Args_t({address_t, address_t});
  FunctionType *FT = FunctionType::get(Type::getVoidTy(*context_), Args_t, false);
  func_ = Function::Create(FT, Function::ExternalLinkage, "llsplatInitInputInternal", mod_);
}

bool Preprocessing::runOnModule(llvm::Module& m) {
  init(m);
  visit(m);
//  assert(!verifyModule(m) && "Error in Proprocessing: module failed verification.\n");
  return true;
}

}  // namespace llsplat


