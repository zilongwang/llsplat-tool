/*******************************************************************************
 * This file is distributed under the MIT License. See LICENSE.TXT for details.
 *******************************************************************************/

#define DEBUG_TYPE "frontend-splitbasicblock"
#include "splitBasicBlock.h"
#include "irUtils.h"
#include "llvm/Support/Debug.h"
#include "llvm/IR/BasicBlock.h"
#include "llvm/IR/Module.h"
#include "llvm/IR/CFG.h"
#include "llvm/IR/Verifier.h"
#include "llvm/IR/IRBuilder.h"
#include "llvm/Bitcode/ReaderWriter.h"
#include "llvm/Support/FileSystem.h"
#include "llvm/Support/ToolOutputFile.h"

#include <string>

using namespace llvm;

extern llvm::cl::opt<std::string> InputFilename;
extern std::string getFileName(const std::string &str);

namespace llsplat {
char SplitBasicBlock::ID = 0;
RegisterPass<SplitBasicBlock> SplitBB("SplitBasicBlock", "SplitBasicBlock");


bool SplitBasicBlock::runOnModule(llvm::Module& m) {
  // After this pass, if a basic block BB has more than one successor,
  // then every successor has only one predecessor, namely, BB.
  int cnt = 0;
  for (auto& Fn : m) {
    for (auto& BB : Fn) {
      auto I = BB.getTerminator();
      if (I->getNumSuccessors() > 1) {
        auto idx = 0;
        for (auto SI = succ_begin(&BB), E = succ_end(&BB); SI != E; ++SI, ++idx) {
          BasicBlock *SuccBB = *SI;
          if (getNumPredecessors(SuccBB) > 1) {

            // Connect InterBB to SuccBB
            BasicBlock* InterBB = BasicBlock::Create(m.getContext(), "interBB_" + std::to_string(++cnt), &Fn);
            IRBuilder<> builder(InterBB);
            builder.CreateBr(SuccBB);

            // Replace the predecessor basic block BB in PHI nodes in SuccBB with InterBB.
            for (auto II = SuccBB->begin(), IE = SuccBB->end(); II != IE; ++II) {
              PHINode *PN = dyn_cast<PHINode>(II);
              if (!PN)
                break;
              int i;
              while ((i = PN->getBasicBlockIndex(&BB)) >= 0)
                PN->setIncomingBlock(i, InterBB);
            }

            // Connect BB to InterBB
            I->setSuccessor(idx, InterBB);
          }
        }
      }
    }
  }

  // Output module which the llsplat's backend will read later.
  std::error_code error_msg;
  std::string OutputFilename = getFileName(InputFilename)+".bc";
  std::unique_ptr<llvm::tool_output_file> output(
      new llvm::tool_output_file(OutputFilename.c_str(), error_msg, llvm::sys::fs::F_None));

  //Verify generated code
  if (verifyModule(m, &llvm::errs())) {
    if (llvm::errs().has_colors()) llvm::errs().changeColor(llvm::raw_ostream::RED);
    llvm::errs() << "Error: In the SplitBasicblock pass, module verification failed.\n";
    if (llvm::errs().has_colors()) llvm::errs().resetColor();
    abort();
  }

  llvm::WriteBitcodeToFile(&m, output->os());
  output->keep();

  return true;
}

} // namespace llsplat
