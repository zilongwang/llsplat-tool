#include "pipe.h"
#include <cstdlib>
#include <unistd.h>
#include <iostream>
#include <assert.h>

using namespace std;

Pipe::Pipe() {
  sz = 1;
  pipeSize = 0;
}

void Pipe::create() {
  /* create the pipe */
  pipe(pipefd_d2c);
  pipe(pipefd_c2d);
}

Pipe::~Pipe() {
}

void Pipe::closePipe() {
  fclose(out);
  fclose(in);
}

void Pipe::setChild() {
  out = fdopen(pipefd_c2d[1], "w");
  close(pipefd_c2d[0]);
  in = fdopen(pipefd_d2c[0], "r");
  close(pipefd_d2c[1]);
  assert(out);
  assert(in);
}
void Pipe::setParent () {
  in = fdopen(pipefd_c2d[0], "r");
  close(pipefd_c2d[1]);
  out = fdopen(pipefd_d2c[1], "w");
  close(pipefd_d2c[0]);
  assert(out);
  assert(in);
}

void Pipe::write(void* buf, size_t nb) {
  int ret = 0;
  int acc = 0;
  char* ptr = (char *) buf;

  //cerr << "Pipe::writing " << nb << " bytes\n" << flush << endl;

  while ((!feof(out)) && (ret < (int)nb)) {
    ret = fwrite(ptr, sz, nb - acc, out);
    if (ret == -1) break;
    acc += ret;
    ptr = (char* )(((unsigned long)ptr) + (sz * ret));
  }
  if (acc != (int)nb) {
    cerr << "Pipe::write failed" << endl;
    exit (1);
  }
}

bool Pipe::inIsEnd() {
  return feof(in) != 0;
}

void Pipe::read (void* buf, size_t nb) {
  int ret = 0;
  int acc = 0;
  char* ptr = (char *) buf;

//  cerr << "Pipe::reading " << nb << " bytes\n" << flush << endl;
  while ((!feof(in)) && (ret < (int)nb)) {
    ret = fread(ptr, sz, nb - acc, in);
    if (ret == -1) break;
    acc += ret;
    ptr = (char* )(((unsigned long)ptr) + (sz * ret));
  }
  if (acc != (int)nb) {
    cerr << "Pipe::read failed, read " << acc << " bytes, should read " << nb << "bytes." << endl;
    exit(1);
  }
  pipeSize += nb;

  if (pipeSize % 1000000 == 0) {
    cout << "queue_size = " << pipeSize / 1000000 << "M" << endl;
  }
}
