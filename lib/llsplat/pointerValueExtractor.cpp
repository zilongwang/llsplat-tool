
/*******************************************************************************
 * This file is distributed under the MIT License. See LICENSE.TXT for details.
 *******************************************************************************/
#define DEBUG_TYPE "frontend-pointerValueExtractor"
#include "../../include/llsplat/pointerValueExtractor.h"
#include "irUtils.h"
#include "llvm/Support/Debug.h"

using namespace llvm;


SmallVector<Value*, 32> PointerValueExtractor::start() {
  SmallVector<Value*, 32> vec;
  for (auto& BB : EDom_) {
    for (auto& I : *BB) {
      if (I.getType()->isPointerTy() && I.getType()->getContainedType(0)->isSingleValueType())
        if (std::find(vec.begin(), vec.end(), &I) == vec.end())
          vec.push_back(&I);

      for (auto& Op : I.operands())
        if (Op.get()->getType()->isPointerTy() && Op.get()->getType()->getContainedType(0)->isSingleValueType())
          if (std::find(vec.begin(), vec.end(), Op.get()) == vec.end())
            vec.push_back(Op.get());

    }
  }
//  for (auto& V : vec) {
//    dbgs() << "vec: " << *V << "\n";
//  }

  return vec;
}


