/*******************************************************************************
 * This file is distributed under the MIT License. See LICENSE.TXT for details.
 *******************************************************************************/
#include "symstack.h"
#include "baseTypes.h"

SymNode::SymNode(bv_t bv, width_t nbits,  z3::expr e) :
bv_(bv),nbits_(nbits), sym_expr_(e){
}

bool SymNode::isConcreteValue() {
  return sym_expr_.is_numeral();
}
//
//bool SymNode::isTrueOrFalse() {
//  return sym_expr_.is_const();
//}

void SymNode::print() {
  cout << sym_expr_ << endl;
}

z3::expr SymNode::getSymExpr() {
  return sym_expr_;
}

bv_t SymNode::getBV() {
  return bv_;
}

width_t SymNode::getNBits() {
  return nbits_;
}

SymStack* SymStack::_instance = NULL;

SymStack::SymStack() {
}


SymStack::~SymStack() {
}

SymStack* SymStack::getSymStack() {
  if (_instance == NULL)
    _instance = new SymStack();
  return _instance;
}

size_t SymStack::size() {
  return sym_stack_.size();
}

void SymStack::push(SymNode *p) {
  sym_stack_.push(p);
}

SymNode* SymStack::toppopNode() {
  if(sym_stack_.empty()) {
    assert(0 && "SymStack is already empty when toppopNode() is called!");
  }
  SymNode* ret = sym_stack_.top();
  sym_stack_.pop();
  return ret;
}
