/*******************************************************************************
 * This file is distributed under the MIT License. See LICENSE.TXT for details.
 *******************************************************************************/
#define DEBUG_TYPE "l-instrument"
#include "instrument.h"
#include "symstack.h"
#include "state.h"
#include "llvm/Support/ErrorHandling.h"
#include "llvm/Support/Debug.h"
#include "llvm/IR/BasicBlock.h"
#include <fstream>

using llvm::dbgs;
using llvm::BasicBlock;
extern llvm::cl::opt<bool> Enable_BMC;

z3::expr createSymVar(index_t unique_id, width_t nbits) {
  StateManager* stateMgr = StateManager::getStateManager();
  return stateMgr->getContext().bv_const((StateManager::sym_prefix_ + std::to_string(unique_id)).c_str(), nbits);
}

bool isTrueOrFalse(z3::expr e) {
  StateManager* stateMgr = StateManager::getStateManager();
  if (eq(e.simplify(), StateManager::getStateManager()->getContext().bool_val(true)) ||
      eq(e.simplify(), StateManager::getStateManager()->getContext().bool_val(false))) {
    return true;
  }
  return false;
}

void printInputs(bv_t bv, width_t nbits, index_t unique_id) {
  if (nbits == 8)
    printf("sym%u -> %#llx (sign: %d) (unsigned: %u) (char: %c)\n", unique_id, bv,
        static_cast<int8_t>(bv),static_cast<uint8_t>(bv), static_cast<char>(bv));
  else if (nbits == 16)
    printf("sym%u -> %#llx (sign: %hd) (unsigned: %hu)\n", unique_id, bv,
        static_cast<int16_t>(bv),static_cast<uint16_t>(bv));
  else if (nbits == 32)
    printf("sym%u -> %#llx (sign: %d) (unsigned: %u)\n", unique_id, bv,
        static_cast<int32_t>(bv),static_cast<uint32_t>(bv));
  else if (nbits == 64)
    printf("sym%u -> %#llx (sign: %lld) (unsigned: %llu)\n", unique_id, bv,
        static_cast<int64_t>(bv),static_cast<uint64_t>(bv));
  else {
    llvm::errs() << "InitInput: Cannot width " << nbits << "\n";
    llvm::llvm_unreachable_internal("Error in llsplatInitInput");
  }
}


////////////// Instrumented function implementation //////////////////////
// Note that width is based on bytes here but in LLVM, it's based on bits.
// width_t is always uint64_t.
void llsplatInitInputInternal(address_t address, width_t nbits) {
  StateManager* stateMgr = StateManager::getStateManager();
  index_t unique_id = stateMgr->getSymCnt();
  bv_t bv = stateMgr->isInLogicInputMap(unique_id) ?
      stateMgr->getValueInLogicInputMap(unique_id) :
      stateMgr->getRamdomNumber(nbits);

//------ Start for debugging
//  if (stateMgr->getIterations() == 1) {
//    ifstream file("inputs.txt");
//    string line;
//    vector<bv_t> vec;
//    if (file.is_open())
//    {
//      while (getline(file, line)) {
//        vec.push_back(stoll(line));
//      }
//      file.close();
//    }
//    bv = vec[unique_id-1];
//  }
//------ End for debugging

  printInputs(bv, nbits, unique_id);
  stateMgr->setValue(address, bv, nbits);

  stateMgr->updateLogicInputMap(unique_id, bv);
  stateMgr->insertToSymbolicStore(address, createSymVar(unique_id, nbits));
}

void llsplatPushVar(address_t addr, bv_t bv, width_t nbits) {
  StateManager* stateMgr = StateManager::getStateManager();
  if (stateMgr->isDoingBMC())
    return;
  SymStack* stack = SymStack::getSymStack();
  SymNode* node = nullptr;

  if(stateMgr->existSymbolicExpr(addr)) {
    // Push the symbolic expression of addr the stack.
    node = new SymNode(bv, nbits, stateMgr->getSymbolicExpr(addr));
  } else {
    // Push the concrete value of addr to the stack.
    node = new SymNode(bv, nbits, stateMgr->bvToZ3Expr(bv, nbits));
  }

//  cout << "//-- PushVar: node.e = " << node->getSymExpr() << "\n";
  stack->push(node);
}

void llsplatPushC(bv_t bv, width_t nbits) {
  StateManager* stateMgr = StateManager::getStateManager();
  if (stateMgr->isDoingBMC())
    return;
  SymStack* sstack = SymStack::getSymStack();
  SymNode* node = new SymNode(bv, nbits, stateMgr->bvToZ3Expr(bv, nbits));
  sstack->push(node);
}

void llsplatPushSExt(bv_t bv, width_t nbits) {
  StateManager* stateMgr = StateManager::getStateManager();
  if (stateMgr->isDoingBMC())
    return;
  SymStack* sstack = SymStack::getSymStack();
  SymNode* node = sstack->toppopNode();
  z3::context& context = StateManager::getStateManager()->getContext();
  z3::expr node_e = node->getSymExpr();

  if (!node_e.is_bv()) {
    assert(node_e.is_bool() && "llsplatPushSExt: not reachable");
    node_e = z3::ite(node_e, context.bv_val(1,1), context.bv_val(0, 1));
  }

  z3::expr e = to_expr(context,
      Z3_mk_sign_ext(context, nbits - node->getNBits(), node_e));
  SymNode* node2 = new SymNode(bv, nbits, e);
  sstack->push(node2);

}

void llsplatPushZExt(bv_t bv, width_t nbits) {
  StateManager* stateMgr = StateManager::getStateManager();
  if (stateMgr->isDoingBMC())
    return;
  SymStack* sstack = SymStack::getSymStack();
  SymNode* node = sstack->toppopNode();
  z3::context& context = StateManager::getStateManager()->getContext();

//  cout << "//-- PushZExt: " << node->getSymExpr() << "\n";
  z3::expr node_e = node->getSymExpr();
  if (!node_e.is_bv()) {
    assert(node_e.is_bool() && "llsplatPushZExt: not reachable");
    node_e = z3::ite(node_e, context.bv_val(1,1), context.bv_val(0, 1));
  }

  z3::expr e = to_expr(context,
      Z3_mk_zero_ext(context, nbits - node->getNBits(), node_e));
//  cout << "//-- \n";
  SymNode* node2 = new SymNode(bv, nbits, e);
  sstack->push(node2);
}

void llsplatPushTrunc(bv_t bv, width_t nbits) {
  StateManager* stateMgr = StateManager::getStateManager();
  if (stateMgr->isDoingBMC())
    return;
  SymStack* sstack = SymStack::getSymStack();
  SymNode* node = sstack->toppopNode();
  z3::context& context = StateManager::getStateManager()->getContext();
  z3::expr e = to_expr(context,
      Z3_mk_extract(context, nbits-1, 0, node->getSymExpr()));
  SymNode* node2 = new SymNode(bv, nbits, e);
  sstack->push(node2);
}

// bv is the value after cast
void llsplatPushPtrToInt(bv_t bv, width_t nbits, width_t ptr_nbits) {
  StateManager* stateMgr = StateManager::getStateManager();
  if (stateMgr->isDoingBMC())
    return;
  SymStack* sstack = SymStack::getSymStack();
  SymNode* node = sstack->toppopNode();
  assert(nbits <= ptr_nbits && "llsplatPushCast: inconsistent bits for source bv");
  z3::context& context = stateMgr->getContext();
  z3::expr e = to_expr(context,
      Z3_mk_extract(context, nbits-1, 0, node->getSymExpr()));
  SymNode* node2 = new SymNode(bv, nbits, e);
  sstack->push(node2);
}


void llsplatPushSelect() {
  StateManager* stateMgr = StateManager::getStateManager();
  if (stateMgr->isDoingBMC())
    return;
  SymStack* sstack = SymStack::getSymStack();
  SymNode* rnode = sstack->toppopNode();
  SymNode* lnode = sstack->toppopNode();
  SymNode* icmp = sstack->toppopNode();

  assert(lnode->getNBits() == rnode->getNBits());
  SymNode* newnode = new SymNode(icmp->getBV() ? lnode->getBV() : rnode->getBV(),
      lnode->getNBits(), z3::ite(icmp->getSymExpr(), lnode->getSymExpr(), rnode->getSymExpr()));
  sstack->push(newnode);
}

void llsplatPushPhi(instruction_t i) {
  auto stateMgr = StateManager::getStateManager();
  if (stateMgr->isDoingBMC())
    return;
  SymStack* sstack = SymStack::getSymStack();
  sstack->push(stateMgr->getPhiMap()[i]);
}

void llsplatUpdatePhiMap(instruction_t i) {
  auto stateMgr = StateManager::getStateManager();
  if (stateMgr->isDoingBMC())
    return;
  SymStack* sstack = SymStack::getSymStack();
  SymNode* node = sstack->toppopNode();
  stateMgr->updatePhiMap(i, node);
}


void llsplatPushICmp(bv_t bv, width_t nbits, predicate_t pred) {
  auto stateMgr = StateManager::getStateManager();
  if (stateMgr->isDoingBMC())
    return;

  SymStack* sstack = SymStack::getSymStack();
  SymNode* rnode = sstack->toppopNode();
  SymNode* lnode = sstack->toppopNode();
  assert(lnode->getNBits() == rnode->getNBits());
  SymNode* node = nullptr;
  z3::context& context = StateManager::getStateManager()->getContext();
  switch(pred) {
  case ICMP_EQ:
  {
    z3::expr e = (lnode->getSymExpr() == rnode->getSymExpr());
    node = new SymNode(bv, nbits, e);
    break;
  }
  case ICMP_NE:
  {
    z3::expr e = (lnode->getSymExpr() != rnode->getSymExpr());
    node = new SymNode(bv, nbits, e);
    break;
  }

  case ICMP_UGT:
  {
    z3::expr e = to_expr(context, Z3_mk_bvugt(context, lnode->getSymExpr(), rnode->getSymExpr()));
    node = new SymNode(bv, nbits, e);
    break;
  }
  case ICMP_UGE:
  {
    z3::expr e = to_expr(context, Z3_mk_bvuge(context, lnode->getSymExpr(), rnode->getSymExpr()));
    node = new SymNode(bv, nbits, e);
    break;
  }
  case ICMP_ULT:
  {
    z3::expr e = to_expr(context, Z3_mk_bvult(context, lnode->getSymExpr(), rnode->getSymExpr()));
    node = new SymNode(bv, nbits, e);
    break;
  }
  case ICMP_ULE:
  {
    z3::expr e = to_expr(context, Z3_mk_bvule(context, lnode->getSymExpr(), rnode->getSymExpr()));
    node = new SymNode(bv, nbits, e);
    break;
  }
  case ICMP_SGT:
  {
    z3::expr e = to_expr(context, Z3_mk_bvsgt(context, lnode->getSymExpr(), rnode->getSymExpr()));
    node = new SymNode(bv, nbits, e);
    break;
  }
  case ICMP_SGE:
  {
    z3::expr e = to_expr(context, Z3_mk_bvsge(context, lnode->getSymExpr(), rnode->getSymExpr()));
    node = new SymNode(bv, nbits, e);
    break;
  }
  case ICMP_SLT:
  {
    z3::expr e = to_expr(context, Z3_mk_bvslt(context, lnode->getSymExpr(), rnode->getSymExpr()));
    node = new SymNode(bv, nbits, e);
    break;
  }
  case ICMP_SLE:
  {
    z3::expr e = to_expr(context, Z3_mk_bvsle(context, lnode->getSymExpr(), rnode->getSymExpr()));
    node = new SymNode(bv, nbits, e);
    break;
  }
  default:
  {
    stringstream errorMessage;
    errorMessage << "Unsupported predicate: " << pred;
    assert(0 && "Unsupported predicate.");
  }
  }
  sstack->push(node);
}

bv_t setValue(bv_t bv, width_t nbits, bool isSigned) {
  if (nbits == 8)
    return isSigned? static_cast<bv_t>(static_cast<int8_t>(bv)) :
        static_cast<bv_t>(static_cast<uint8_t>(bv));
  else if (nbits == 16)
    return isSigned? static_cast<bv_t>(static_cast<int16_t>(bv)) :
        static_cast<bv_t>(static_cast<uint16_t>(bv));
  else if (nbits == 32)
    return isSigned? static_cast<bv_t>(static_cast<int32_t>(bv)) :
        static_cast<bv_t>(static_cast<uint32_t>(bv));
  else if (nbits == 64)
    return isSigned? static_cast<bv_t>(static_cast<int64_t>(bv)) :
        static_cast<bv_t>(static_cast<uint64_t>(bv));
  else {
    llvm::errs() << "setValue: Cannot width " << nbits << "\n";
    llvm::llvm_unreachable_internal("Error in setValue");
  }
}

void llsplatPushBop(binop_t bop) {
  auto stateMgr = StateManager::getStateManager();
  if (stateMgr->isDoingBMC())
    return;

  SymStack* sstack = SymStack::getSymStack();
  SymNode* rnode = sstack->toppopNode();
  SymNode* lnode = sstack->toppopNode();
  assert(lnode->getNBits() == rnode->getNBits());
  z3::context& context = StateManager::getStateManager()->getContext();
  SymNode* node = nullptr;
  switch(bop) {
  case Add :
  {
    z3::expr e = (lnode->getSymExpr() + rnode->getSymExpr());
    bv_t bv = lnode->getBV() + rnode->getBV();
    node = new SymNode(bv, lnode->getNBits(), e);
    break;
  }
  case Sub :
  {

    z3::expr e = (lnode->getSymExpr() - rnode->getSymExpr());
    bv_t bv = lnode->getBV() - rnode->getBV();
    node = new SymNode(bv, lnode->getNBits(), e);
    break;
  }
  case Mul :
  {
    z3::expr e = (lnode->getSymExpr() * rnode->getSymExpr());
    bv_t bv = lnode->getBV() * rnode->getBV();
    node = new SymNode(bv, lnode->getNBits(), e);
    break;
  }
  case UDiv:
  {
    const z3::expr& le = lnode->getSymExpr();
    const z3::expr& re = rnode->getSymExpr();

    z3::expr e = to_expr(context, Z3_mk_bvudiv(context, le, re));

    const auto nbits = lnode->getNBits();

    bv_t bv;
    if (nbits == 8) {
      uint8_t lfs = static_cast<uint8_t>(lnode->getBV());
      uint8_t rfs = static_cast<uint8_t>(rnode->getBV());
      bv = static_cast<bv_t>(lfs / rfs);
    }
    else if (nbits == 16) {
      uint16_t lfs = static_cast<uint16_t>(lnode->getBV());
      uint16_t rfs = static_cast<uint16_t>(rnode->getBV());
      bv = static_cast<bv_t>(lfs / rfs);
    }
    else if (nbits == 32) {
      uint32_t lfs = static_cast<uint32_t>(lnode->getBV());
      uint32_t rfs = static_cast<uint32_t>(rnode->getBV());
      bv = static_cast<bv_t>(lfs / rfs);
    }
    else if (nbits == 64) {
      uint64_t lfs = static_cast<uint64_t>(lnode->getBV());
      uint64_t rfs = static_cast<uint64_t>(rnode->getBV());
      bv = static_cast<bv_t>(lfs / rfs);
    }
    else {
      llvm::errs() << "llsplatPushBop: Cannot width " << nbits << "\n";
      llvm::llvm_unreachable_internal("Error in llsplatPushBop");
    }

    node = new SymNode(bv, lnode->getNBits(), e);
    break;
  }
  case SDiv:
  {
    const z3::expr& le = lnode->getSymExpr();
    const z3::expr& re = rnode->getSymExpr();

    z3::expr e = to_expr(context, Z3_mk_bvsdiv(context, le, re));
    const auto nbits = lnode->getNBits();

    bv_t bv;
    if (nbits == 8) {
      int8_t lfs = static_cast<int8_t>(lnode->getBV());
      int8_t rfs = static_cast<int8_t>(rnode->getBV());
      bv = static_cast<bv_t>(lfs / rfs);
    }
    else if (nbits == 16) {
      int16_t lfs = static_cast<int16_t>(lnode->getBV());
      int16_t rfs = static_cast<int16_t>(rnode->getBV());
      bv = static_cast<bv_t>(lfs / rfs);
    }
    else if (nbits == 32) {
      int32_t lfs = static_cast<int32_t>(lnode->getBV());
      int32_t rfs = static_cast<int32_t>(rnode->getBV());
      bv = static_cast<bv_t>(lfs / rfs);
    }
    else if (nbits == 64) {
      int64_t lfs = static_cast<int64_t>(lnode->getBV());
      int64_t rfs = static_cast<int64_t>(rnode->getBV());
      bv = static_cast<bv_t>(lfs / rfs);
    }
    else {
      llvm::errs() << "llsplatPushBop: Cannot width " << nbits << "\n";
      llvm::llvm_unreachable_internal("Error in llsplatPushBop");
    };
    node = new SymNode(bv, lnode->getNBits(), e);
    break;
  }
  case URem:
  {
    const z3::expr& le = lnode->getSymExpr();
    const z3::expr& re = rnode->getSymExpr();

    z3::expr e = to_expr(context, Z3_mk_bvurem(context, le, re));
    const auto nbits = lnode->getNBits();

    bv_t bv;
    if (nbits == 8) {
      uint8_t lfs = static_cast<uint8_t>(lnode->getBV());
      uint8_t rfs = static_cast<uint8_t>(rnode->getBV());
      bv = static_cast<bv_t>(lfs % rfs);
    }
    else if (nbits == 16) {
      uint16_t lfs = static_cast<uint16_t>(lnode->getBV());
      uint16_t rfs = static_cast<uint16_t>(rnode->getBV());
      bv = static_cast<bv_t>(lfs % rfs);
    }
    else if (nbits == 32) {
      uint32_t lfs = static_cast<uint32_t>(lnode->getBV());
      uint32_t rfs = static_cast<uint32_t>(rnode->getBV());
      bv = static_cast<bv_t>(lfs % rfs);
    }
    else if (nbits == 64) {
      uint64_t lfs = static_cast<uint64_t>(lnode->getBV());
      uint64_t rfs = static_cast<uint64_t>(rnode->getBV());
      bv = static_cast<bv_t>(lfs % rfs);
    }
    else {
      llvm::errs() << "llsplatPushBop: Cannot width " << nbits << "\n";
      llvm::llvm_unreachable_internal("Error in llsplatPushBop");
    };
    node = new SymNode(bv, lnode->getNBits(), e);
    break;
  }
  case SRem:
  {
    const z3::expr& le = lnode->getSymExpr();
    const z3::expr& re = rnode->getSymExpr();

    z3::expr e = to_expr(context, Z3_mk_bvsrem(context, le, re));
    const auto nbits = lnode->getNBits();

    bv_t bv;
    if (nbits == 8) {
      int8_t lfs = static_cast<int8_t>(lnode->getBV());
      int8_t rfs = static_cast<int8_t>(rnode->getBV());
      bv = static_cast<bv_t>(lfs % rfs);
    }
    else if (nbits == 16) {
      int16_t lfs = static_cast<int16_t>(lnode->getBV());
      int16_t rfs = static_cast<int16_t>(rnode->getBV());
      bv = static_cast<bv_t>(lfs % rfs);
    }
    else if (nbits == 32) {
      int32_t lfs = static_cast<int32_t>(lnode->getBV());
      int32_t rfs = static_cast<int32_t>(rnode->getBV());
      bv = static_cast<bv_t>(lfs % rfs);
    }
    else if (nbits == 64) {
      int64_t lfs = static_cast<int64_t>(lnode->getBV());
      int64_t rfs = static_cast<int64_t>(rnode->getBV());
      bv = static_cast<bv_t>(lfs % rfs);
    }
    else {
      llvm::errs() << "llsplatPushBop: Cannot width " << nbits << "\n";
      llvm::llvm_unreachable_internal("Error in llsplatPushBop");
    };
    node = new SymNode(bv, lnode->getNBits(), e);
    break;
  }
  case Xor:
  {
    const z3::expr& le = lnode->getSymExpr();
    const z3::expr& re = rnode->getSymExpr();

    z3::expr e1 = le.is_bool() ? z3::ite(le, context.bv_val(1, 1), context.bv_val(0, 1)) : le;
    z3::expr e2 = re.is_bool() ? z3::ite(re, context.bv_val(1, 1), context.bv_val(0, 1)) : re;

    z3::expr e = e1 ^ e2;
    bv_t bv = lnode->getBV() ^ rnode->getBV();
    node = new SymNode(bv, lnode->getNBits(), e);
    break;
  }
  case And:
  {
	  const z3::expr& le = lnode->getSymExpr();
	  const z3::expr& re = rnode->getSymExpr();

	  z3::expr e1 = le.is_bool() ? z3::ite(le, context.bv_val(1, 1), context.bv_val(0, 1)) : le;
	  z3::expr e2 = re.is_bool() ? z3::ite(re, context.bv_val(1, 1), context.bv_val(0, 1)) : re;

	  z3::expr e = e1 & e2;
	  bv_t bv = lnode->getBV() & rnode->getBV();
	  node = new SymNode(bv, lnode->getNBits(), e);
	  break;
  }
  case Or:
  {
	  const z3::expr& le = lnode->getSymExpr();
	  const z3::expr& re = rnode->getSymExpr();

	  z3::expr e1 = le.is_bool() ? z3::ite(le, context.bv_val(1, 1), context.bv_val(0, 1)) : le;
	  z3::expr e2 = re.is_bool() ? z3::ite(re, context.bv_val(1, 1), context.bv_val(0, 1)) : re;

	  z3::expr e = e1 | e2;
	  bv_t bv = lnode->getBV() | rnode->getBV();
	  node = new SymNode(bv, lnode->getNBits(), e);
	  break;
  }

  default:
  {
    stringstream errorMessage;
    errorMessage << "Unsupported bop: " << bop;
    assert(0 && "Unsupported bop");
  }
  }


  sstack->push(node);
}

void llsplatPushLocalAddrRange(address_t start, address_t end) {
  auto stateMgr = StateManager::getStateManager();
  assert(!stateMgr->isDoingBMC());

  stateMgr->pushLocalAddrRange(start, end);
}

void llsplatPopLocalAddrRange() {
  auto stateMgr = StateManager::getStateManager();
  assert(!stateMgr->isDoingBMC());

  stateMgr->popLocalAddrRange();
}


void llsplatUpdateSymbolicStore(address_t addr) {
  auto stateMgr = StateManager::getStateManager();
  if (stateMgr->isDoingBMC())
    return;
  SymStack* sstack = SymStack::getSymStack();
  SymNode* node = sstack->toppopNode();
  if(node->isConcreteValue()) {
    stateMgr->eraseFromSymbolicStore(addr);
  } else {
    stateMgr->insertToSymbolicStore(addr, node->getSymExpr());
  }

}

void llsplatAddPathConstraint(bool b, const char* FnName, const char* BBName) {
  auto stateMgr = StateManager::getStateManager();
  if (stateMgr->isDoingBMC())
    return;
  SymStack* sstack = SymStack::getSymStack();
  SymNode* node = sstack->toppopNode();

  z3::expr e = node->getSymExpr();
  z3::expr one = StateManager::getStateManager()->getContext().bv_val(1, 1);
  if (!e.is_bool())
    e = (e == one);

  if (!isTrueOrFalse(e)) {
    b ? stateMgr->addPathConstraint(e) : stateMgr->addPathConstraint((!e));
    stateMgr->compareCurrentPathWithPrediction(b, FnName, BBName);
    stateMgr->reachMaxLength();
  }
}

void llsplatAddPathConstraintSwitch(index_t numOfCases, const char* FnName, const char* BBName) {
  auto stateMgr = StateManager::getStateManager();
  if (stateMgr->isDoingBMC())
    return;

  SymStack* sstack = SymStack::getSymStack();
  SymNode* cond = sstack->toppopNode();

  vector<SymNode*> cases;
  for (auto i = 0; i < numOfCases; ++i) {
    cases.push_back(sstack->toppopNode());
  }

  vector<z3::expr> vec;
  for (auto cs : cases) {
    vec.push_back((cond->getSymExpr() == cs->getSymExpr()));
  }

  z3::expr default_pc = stateMgr->getContext().bool_val(false);
  for (auto e : vec) {
    default_pc = default_pc || e;
  }
  default_pc = !default_pc;
  vec.push_back(default_pc);


  // regular cases. Index starts from 1.
  index_t current_case = 0;
  for (auto cs = cases.begin(), E = cases.end(); cs != E; ++cs, ++current_case) {
    if ((*cs)->getBV() != cond->getBV())
      continue;
    z3::expr pc = (cond->getSymExpr() == (*cs)->getSymExpr());
    if (!isTrueOrFalse(pc)) {
      stateMgr->addPathConstraint(pc);
      stateMgr->compareCurrentPathWithPrediction(vec.size(), current_case, vec, FnName, BBName);
      stateMgr->reachMaxLength();
    }
    return;
  }
  // default case. Assume that default case has index cases.size().
  if (!isTrueOrFalse(default_pc)) {
    stateMgr->addPathConstraint(default_pc);
    stateMgr->compareCurrentPathWithPrediction(vec.size(), current_case, vec, FnName, BBName);
    stateMgr->reachMaxLength();
  }

}


void llsplatSolve() {
  auto stateMgr = StateManager::getStateManager();
  if (stateMgr->isDoingBMC())
    return;
  throw stateMgr->solve();
}

void llsplatStartBMC(const char* FnName, const char* BBName, const char* DestNames) {
  if (!Enable_BMC) {
    SymStack* sstack = SymStack::getSymStack();
    assert(sstack->size() >= 1);
    sstack->toppopNode();
    return;
  }
  DEBUG(dbgs() << "llsplatStartBMC is calling in function " << FnName << "\n");
//  dbgs() << "llsplatStartBMC is calling in function " << FnName << "\n";
  auto stateMgr = StateManager::getStateManager();
  if (stateMgr->isDoingBMC()) {
    DEBUG(dbgs() << "llsplatStartBMC returns in function because it's already in BMC mode" << FnName << "\n");
    return;
  }

  istringstream iss(DestNames);
  vector<BasicBlock*> Dests;

  vector<string> tokens;
  copy(istream_iterator<string>(iss),
      istream_iterator<string>(),
      back_inserter(tokens));

  for (auto& destName : tokens)
    Dests.push_back(stateMgr->getBasicBlock(FnName, destName));
  BasicBlock* Dominator = stateMgr->getBasicBlock(FnName, BBName);
  stateMgr->startBMC(Dominator, Dests);

  DEBUG(dbgs() << "llsplatStartBMC returns in function " << FnName << "\n");
//  dbgs() << "llsplatStartBMC returns in function " << FnName << "\n";
}

void llsplatEndBMC(const char* FnName, const char* BBName, const char* DestName) {
  auto stateMgr = StateManager::getStateManager();
  if (!stateMgr->isDoingBMC()) {
    return;
  }
  DEBUG(dbgs() << "llsplatEndBMC is calling in function " << FnName << "\n";);
  BasicBlock* Dominator = stateMgr->getBasicBlock(FnName, BBName);
  if (stateMgr->getCurrDominator() != Dominator) {
    DEBUG(dbgs() <<  "stateMgr->getCurrDominator() = " << stateMgr->getCurrDominator()->getName() << "\n");
    DEBUG(dbgs() <<  "Dominator = " << Dominator->getName() << "\n");
    DEBUG(dbgs() << "llsplatEndBMC returns in function " << FnName << " because currDominator is different from the argument\n");
    return;
  }

  BasicBlock* Dest = stateMgr->getBasicBlock(FnName, DestName);
  stateMgr->coverDestination(Dest);
  stateMgr->incrementBMCNum();

  // Update coverage information
  stateMgr->updateBMCBranches(Dominator, Dest);

  // Clean up a few data structures used for a single BMC.
  stateMgr->endBMC();
  stateMgr->reachMaxLength();

  DEBUG(dbgs() << "llsplatEndBMC returns in function " << FnName << "\n");
}

z3::expr new_symbol(std::string name, width_t nbits);

void llsplatSaveVarInfo(index_t idx, address_t addr, bv_t bv, width_t nbits) {
  if (!Enable_BMC)
    return;
  auto stateMgr = StateManager::getStateManager();
  if (stateMgr->isDoingBMC())
    return;
  stateMgr->idx2addr_.insert(std::make_pair(idx, addr));
  if (stateMgr->addr2version_.find(addr) != stateMgr->addr2version_.end())
    return;
  stateMgr->addr2version_.insert(std::make_pair(addr, 0));
  stateMgr->addr2varinfo_.insert(std::make_pair(addr, std::unique_ptr<VarInfo>(new VarInfo(addr, bv, nbits))));
  stateMgr->addr2varname_.insert(std::make_pair(addr, stateMgr->getInitialSymbol()));
  z3::expr e1 = stateMgr->existSymbolicExpr(addr) ?
      stateMgr->getSymbolicExpr(addr) : stateMgr->bvToZ3Expr(bv, nbits);
  string name = stateMgr->addr2varname_[addr] + "_v0_b" + std::to_string(stateMgr->getBmcNum());
  z3::expr e2 = new_symbol(name, nbits);
  stateMgr->saveOldSymbolicExpr(addr); // Save old symbolic expressions in case BMC fails
  stateMgr->insertToSymbolicStore(addr, e2);
  stateMgr->addInitFormula(e1==e2);
}

void llsplatUpdateBranchCoverage(const char* FnName, const char* BBName, index_t numOfCases, index_t CurrCase, bool isTaken) {
  auto stateMgr = StateManager::getStateManager();
  if (stateMgr->isDoingBMC())
    return;

  DEBUG(dbgs()<< "llsplat updating branch coverage\n");

  auto BB = stateMgr->getBasicBlock(FnName, BBName);
  stateMgr->updateBranchCoverageStatEntry(BB, numOfCases, CurrCase, isTaken);
  DEBUG(dbgs()<< "llsplat updated branch coverage\n");
}

