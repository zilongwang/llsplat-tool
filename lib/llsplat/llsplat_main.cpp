/*******************************************************************************
 * This file is distributed under the MIT License. See LICENSE.TXT for details.
 *******************************************************************************/
#define DEBUG_TYPE "l-main"
#include "llvm/Support/CommandLine.h"
#include "llvm/Support/raw_ostream.h"
#include "llvm/Support/Debug.h"
#include "state.h"
#include <cstdlib>
#include <unistd.h>
#include <csignal>
#include <csetjmp>

#ifdef __linux
#include <wait.h>
#endif

static llvm::cl::opt<int>
MaxIteration("max-iter",
    llvm::cl::desc("Specify the maximal number of iterations that LLsplat explores."),
    llvm::cl::init(100),
    llvm::cl::value_desc("max iteration"));


llvm::cl::opt<bool>
Enable_BMC("bmc",
    llvm::cl::desc("Enable bounded model checking optimization"),
    llvm::cl::init(false),
    llvm::cl::value_desc("llvm-assembly"));

llvm::cl::opt<std::string>
InputFileName(
  llvm::cl::Positional,
  llvm::cl::desc("<input LLVM bitcode file>"),
  llvm::cl::Required,
  llvm::cl::value_desc("filename")
);

llvm::cl::opt<int>
MaxLength("max-len",
    llvm::cl::desc("Specify the maximal length of path constraint that LLsplat maintains."),
    llvm::cl::init(-1),
    llvm::cl::value_desc("max length"));


//llvm::cl::opt<std::string>
//InputFileArgs("args",
//  llvm::cl::desc("Arguments to the program under test"),
//  llvm::cl::init(""),
//  llvm::cl::value_desc("input file arguments")
//);


extern "C" {
//int llsplatInstrumented(int argc, char** argv);
int llsplatInstrumented();
}

jmp_buf llsplat_jmp_buffer;

/*
|-----------------------------------------------------------------
| child_sig_handler
|-----------------------------------------------------------------
|
| jump to setjump
|
 */
void child_sig_handler(int sig) {
  longjmp(llsplat_jmp_buffer, sig);
}

/*
|-----------------------------------------------------------------
| setup_signals
|-----------------------------------------------------------------
|
| sigaction has
| --sa_handler: function pointer take in int and return void, int is signal value.
| --sa_flag: flag which modifies behavior of a sognal
| --sa_mask: a set of signal should be blocked
| see http://man7.org/linux/man-pages/man2/sigaction.2.html
| and http://fanqiang.chinaunix.net/a4/b2/20010508/113528.html
| sigemptySet(sigset_t *p_sig) empty signal set for blocking
| sa_handler enveloped longjump which will jump to setjump and receive which kind
| of signal is and do operations accordingly after setting up.
| i.e setup, catch and jump
|
 */
void setup_signals() {
  struct sigaction sigSegHandler;
  sigSegHandler.sa_handler = child_sig_handler;
  sigemptyset(&sigSegHandler.sa_mask);
  sigSegHandler.sa_flags = 0;
  sigaction(SIGSEGV, &sigSegHandler, NULL);
  if(sigaction(SIGSEGV, &sigSegHandler, NULL) < 0) {
    assert(0 && "Llsplat can't setup a handler for SIGSEGV.");
  }

  struct sigaction sigAlarmHandler;
  sigAlarmHandler.sa_handler = child_sig_handler;
  sigemptyset(&sigAlarmHandler.sa_mask);
  sigSegHandler.sa_flags = 0;
  sigaction(SIGALRM, &sigSegHandler, NULL);
  if(sigaction(SIGALRM, &sigAlarmHandler, NULL) < 0) {
    assert(0 && "Llsplat can't setup a handler for SIGALRM.");
  }

  struct sigaction sigAssertHandler;
  sigAssertHandler.sa_handler = child_sig_handler;
  sigemptyset(&sigAssertHandler.sa_mask);
  sigAssertHandler.sa_flags = 0;
  sigaction(SIGABRT, &sigAssertHandler, NULL);
  if(sigaction(SIGABRT, &sigAssertHandler, NULL) < 0) {
    assert(0 && "Llsplat can't setup a handler for SIGABRT.");
  }

}

//void runLlsplat() {
//  auto stateMgr = StateManager::getStateManager();
//
//  setup_signals();
//  while(true) {
//    switch (setjmp(llsplat_jmp_buffer)) {
//    case 0: { // Normal execution
//      try {
//        auto runs = stateMgr->getIterations();
//        if (runs > MaxIteration)
//          throw MaxIterationReached;
//        llvm::outs() << "------------- run " << runs << " -------------\n";
//        llsplatInstrumented();
//        llvm::llvm_unreachable_internal("Unreachable");
//      } catch (llsplat_status_t& status) {
//        switch (status) {
//        case Continue: {
//          stateMgr->prepareForNextIteration();
//          stateMgr->endTimer();
//          stateMgr->printStatistics();
//          break;
//        }
//        case ProgramSafe: {
//          llvm::outs() << "Llsplat is over. The program is safe. :-)\n";
//          return;
//        }
//        case BugFound: {
//          llvm::outs() << "Llsplat found bugs in the program. :-(\n";
//          return;
//        }
//        case MaxIterationReached: {
//          llvm::outs() << "Llsplat reaches max iteration. The program is safe so far.\n";
//          return;
//        }
//        case Timeout: {
//          llvm::outs() << "Llsplat is timeout. The program is safe so far.\n";
//          return;
//        }
//        }
//      } catch (std::exception& ex) {
//        llvm::errs() << "Llsplat: Receive std exception: " << ex.what() << "\n";
//        return;
//      } catch (z3::exception& ex) {
//        llvm::errs() << "Llsplat: Receive z3 exception: " << ex.msg() << "\n";
//        return;
//      }
//      break;
//    }
//    case SIGSEGV: {
//      llvm::outs() << "Llsplat: in " << stateMgr->getIterations() << "th run  Instrumented code returned on seg fault.\n";
//      return;
//    }
//    case SIGALRM: {
//      llvm::outs() << "Llsplat: in " << stateMgr->getIterations() << "th run  Timeout.\n";
//      return;
//    }
//    case SIGABRT: {
//      llvm::outs() << "Llsplat: in " << stateMgr->getIterations() << "th run  Instrumented code returned on abort.\n";
////      stateMgr->prepareForNextIteration();
////      break;
//      return;
//    }
//    default: {
//      llvm::errs() << "Llsplat: in " << stateMgr->getIterations() << "th run  Unexpected signal received.\n";
//      return;
//    }
//    }
//  }
//}

/*
|-----------------------------------------------------------------
| runChild
|-----------------------------------------------------------------
|
| This function execute llsplatInstrumented which is the wrapper of code
| to be verified. Also process caught signal and handle all the exceptions
| defined in instrumentation function implementation.
|
|
 */
void runChild() {
  auto stateMgr = StateManager::getStateManager();
  stateMgr->_pipe.setChild();
  setup_signals();

  switch (setjmp(llsplat_jmp_buffer)) {
  case 0: { // Normal execution
    try {
      llvm::outs() << "------------- run " << stateMgr->getIterations() << " -------------\n";
      llsplatInstrumented();
      llvm::llvm_unreachable_internal("Unreachable");
    } catch (llsplat_status_t& status) {
      stateMgr->reportToParentProcess(status);
      return;
    } catch (std::exception& ex) {
      llvm::errs() << "Llsplat [child]: Receive std exception: " << ex.what() << "\n";
      stateMgr->reportToParentProcess(UnknownBug);
      return;
    } catch (z3::exception& ex) {
      llvm::errs() << "Llsplat [child]: Receive z3 exception: " << ex.msg() << "\n";
      stateMgr->reportToParentProcess(UnknownBug);
      return;
    }
    break;
  }
  case SIGSEGV: {
//    llvm::errs() << "Llsplat [child]: in " << stateMgr->getIterations() << "th run  Instrumented code returned on seg fault.\n";
    stateMgr->reportToParentProcess(BugFound);
    return;
  }
  case SIGALRM: {
//    llvm::outs() << "Llsplat [child]: in " << stateMgr->getIterations() << "th run  Timeout.\n";
    stateMgr->reportToParentProcess(Timeout);
    return;
  }
  case SIGABRT: {
//    llvm::outs() << "Llsplat [child]: in " << stateMgr->getIterations() << "th run  Instrumented code returned on abort.\n";
    stateMgr->reportToParentProcess(AssertFailure);
    return;
  }
  default: {
    llvm::errs() << "Llsplat [child]: in " << stateMgr->getIterations() << "th run  Unexpected signal received.\n";
    stateMgr->reportToParentProcess(UnknownBug);
    return;
  }
  }
}



/*
|-----------------------------------------------------------------
| runLlsplat
|-----------------------------------------------------------------
|
| This function is the core wrapper of Llsplat. It forks a child process
| to do code verification by visiting a specific path whose input is set
| initially(1st run empty actually) or given by result from last child
| process through a pipe to parent process.
| Based on the status returned from ended child, parent process will decide
| whether to continue perform visiting another path or finish verification.
|
 */
void runLlsplat() {
  auto stateMgr = StateManager::getStateManager();
  while(true) {
    auto runs = stateMgr->getIterations();
    if (runs > MaxIteration) {
      llvm::outs() << "LLSPLAT reaches max iteration. The program is safe so far.\n";
      return;
    }

    stateMgr->_pipe.create();
    pid_t pid;
    switch (pid = fork()) {
    case -1: {
      llvm::errs() << "fork() error\n";
      exit(EXIT_FAILURE);
    }
    case 0: { // Child
      runChild();
      exit(EXIT_SUCCESS);
    }
    default: { // Parent
      stateMgr->_pipe.setParent();
      llsplat_status_t status = stateMgr->waiting();
      stateMgr->_pipe.closePipe();

      int return_code;
      wait(&return_code);

      switch (status) {
      case Continue: {
        stateMgr->endTimer();
        stateMgr->printStatistics();
        stateMgr->incrementIterations();
        break;
      }
      case BugFound: {
        llvm::outs() << "Llsplat: in " << stateMgr->getIterations() << "th run  Instrumented code returned on seg fault.\n";
        return;
      }
      case ProgramSafe: {
        llvm::outs() << "Llsplat is over. The program is safe. :-)\n";
        return;
      }
      case Timeout: {
        llvm::outs() << "Llsplat is timeout. The program is safe so far.\n";
        return;
      }
      case AssertFailure: {
//        llvm::outs() << "Llsplat: in 2th run Instrumented code returned on abort.\n";
        llvm::outs() << "Llsplat: in " << stateMgr->getIterations() << "th run Instrumented code returned on abort.\n";
        return;
      }
      default: {
        llvm::llvm_unreachable_internal("Unknown status.\n");
      }
      }
    }
    }
  }
}



int main(int argc, char** argv) {
//  int stop = 0;
//  for (stop = 0; stop < argc; ++stop) {
//    std::string arg = argv[stop];
//    if (arg == "--")
//      break;
//  }
//  llvm::cl::ParseCommandLineOptions(stop, argv, "LLSPLAT - Bug Finder for C Programs\n");
//  llvm::cl::ParseCommandLineOptions(argc, argv, "LLSPLAT - Bug Finder for C Programs\n");
//  auto stateMgr = StateManager::getStateManager();
//  stateMgr->startTimer();
////  runLlsplat(argc-(stop+1), &argv[stop+1]);
//  runLlsplat();
//  stateMgr->endTimer();
//  stateMgr->printStatistics();
//  return EXIT_SUCCESS;


  llvm::cl::ParseCommandLineOptions(argc, argv, "LLSPLAT - Bug Finder for C Programs\n");
  auto stateMgr = StateManager::getStateManager();
  stateMgr->startTimer();
  runLlsplat();
  stateMgr->endTimer();
  stateMgr->printStatistics();
  return EXIT_SUCCESS;

}
