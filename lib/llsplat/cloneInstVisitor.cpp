
/*******************************************************************************
 * This file is distributed under the MIT License. See LICENSE.TXT for details.
 *******************************************************************************/

#define DEBUG_TYPE "frontend-varinfoBuilderHelper"
#include "../../include/llsplat/cloneInstVisitor.h"
#include "irUtils.h"
#include "llvm/Support/Debug.h"

using namespace llvm;

namespace llsplat {

//void VarInfoBuilderHelper::visitBinaryOperator(BinaryOperator& I) {
//  DEBUG(dbgs() << "VarInfoBuilderHelper: visiting bop " << I << "\n");
//
//  for(auto& op : I.operands()) {
//    visitValue(*(op.get()));
//  }
//
//  DEBUG(dbgs() << "VarInfoBuilderHelper: visited bop " << I << "\n");
//}
//
//void VarInfoBuilderHelper::visitAllocaInst(AllocaInst& I) {
//  DEBUG(dbgs() << "VarInfoBuilderHelper: visiting alloca " << I << "\n");
//
//  if (isa<StructType>(I.getAllocatedType()) || isa<ArrayType>(I.getAllocatedType())) {
//    return;
//  }
//  auto idx = builder_.getInt32(numOfAddr_++);
//  auto address = builder_.CreatePtrToInt(&I, builder_.getIntNTy(ptr_size_));
//  auto loadInst = builder_.CreateLoad(&I);
//  auto bv = isa<PointerType>(loadInst->getType()) ?
//      builder_.CreatePtrToInt(loadInst, builder_.getIntNTy(ptr_size_)) :
//      builder_.CreateZExt(loadInst, builder_.getIntNTy(ptr_size_));
//  unsigned actural_size = mod_.getDataLayout()->getTypeSizeInBits(loadInst->getType());
//  Value* nbits = builder_.getIntN(ptr_size_, actural_size);
//
//  ArrayRef<Value*> Args({idx, address, bv, nbits});
//  builder_.CreateCall(fmap_[SaveVarInfo], Args);
//
//  DEBUG(dbgs() << "VarInfoBuilderHelper: visited alloca\n");
//}
//
//void VarInfoBuilderHelper::visitLoadInst(LoadInst& I) {
//  DEBUG(dbgs() << "VarInfoBuilderHelper: visiting load " << I << "\n");
//
//  visitValue(*I.getPointerOperand());
//
//  if (!(I.getType()->isPointerTy() &&
//      I.getType()->getContainedType(0)->isSingleValueType()))
//    return;
//
//  CloneInstVisitor cloner(mod_, builder_);
//  cloner.visitValue(*I.getPointerOperand());
//  auto PtrOp = cloner.getPreviousInst();
//  auto loadInst0 = builder_.CreateLoad(PtrOp);
//
//  auto idx = builder_.getInt32(numOfAddr_++);
//  auto address = builder_.CreatePtrToInt(loadInst0, builder_.getIntNTy(ptr_size_));
//  auto loadInst = builder_.CreateLoad(loadInst0);
//  auto bv = isa<PointerType>(loadInst->getType()) ?
//      builder_.CreatePtrToInt(loadInst, builder_.getIntNTy(ptr_size_)) :
//      builder_.CreateZExt(loadInst, builder_.getIntNTy(ptr_size_));
//  unsigned actural_size = mod_.getDataLayout()->getTypeSizeInBits(loadInst->getType());
//  Value* nbits = builder_.getIntN(ptr_size_, actural_size);
//
//  ArrayRef<Value*> Args({idx, address, bv, nbits});
//  builder_.CreateCall(fmap_[SaveVarInfo], Args);
//
//  DEBUG(dbgs() << "VarInfoBuilderHelper: visited load\n");
//}
//
//void VarInfoBuilderHelper::visitStoreInst(StoreInst& I) {
//  DEBUG(dbgs() << "VarInfoBuilderHelper: visiting store " << I << "\n");
//  llvm_unreachable_internal("VarInfoBuilderHelper should not visit StoreInst.");
//  DEBUG(dbgs() << "VarInfoBuilderHelper: visited store\n");
//}
//
//void VarInfoBuilderHelper::visitGetElementPtrInst(GetElementPtrInst& I) {
//  DEBUG(dbgs() << "VarInfoBuilderHelper: visiting GEP " << I << "\n");
//
//  visitValue(*I.getPointerOperand());
//  for (auto Idx = I.idx_begin(), E = I.idx_end(); Idx != E; ++Idx) {
//    visitValue(**Idx);
//  }
//
//  if (!(I.getType()->isPointerTy() &&
//      I.getType()->getContainedType(0)->isSingleValueType()))
//    return;
//
//  CloneInstVisitor cloner(mod_, builder_);
//  cloner.visitValue(*I.getPointerOperand());
//  auto PtrOp = cloner.getPreviousInst();
//  std::vector<Value*> vec;
//  for (auto Idx = I.idx_begin(), E = I.idx_end(); Idx != E; ++Idx) {
//    cloner.visitValue(**Idx);
//    vec.push_back(cloner.getPreviousInst());
//  }
//
//  ArrayRef<Value *> IdxList(vec);
//  auto GepInst = builder_.CreateInBoundsGEP(PtrOp, IdxList);
//
//  auto idx = builder_.getInt32(numOfAddr_++);
//  auto address = builder_.CreatePtrToInt(GepInst, builder_.getIntNTy(ptr_size_));
//  auto loadInst = builder_.CreateLoad(GepInst);
//  auto bv = isa<PointerType>(loadInst->getType()) ?
//      builder_.CreatePtrToInt(loadInst, builder_.getIntNTy(ptr_size_)) :
//      builder_.CreateZExt(loadInst, builder_.getIntNTy(ptr_size_));
//  unsigned actural_size = mod_.getDataLayout()->getTypeSizeInBits(loadInst->getType());
//  Value* nbits = builder_.getIntN(ptr_size_, actural_size);
//
//  ArrayRef<Value*> Args({idx, address, bv, nbits});
//  builder_.CreateCall(fmap_[SaveVarInfo], Args);
//
//  DEBUG(dbgs() << "VarInfoBuilderHelper: visited GEP\n");
//}
//
//void VarInfoBuilderHelper::visitPHINode(PHINode& I) {
//  DEBUG(dbgs() << "VarInfoBuilderHelper: visiting PHI node " << I << "\n");
//  llvm::llvm_unreachable_internal("VarInfoBuilderHelper: assume no phi node for now");
//  DEBUG(dbgs() << "VarInfoBuilderHelper: visited PHI node\n");
//}
//
//void VarInfoBuilderHelper::visitSelectInst(llvm::SelectInst& I) {
//  DEBUG(dbgs() << "VarInfoBuilderHelper: visiting SelectInst " << I << "\n");
//
//  visitValue(*I.getCondition());
//  visitValue(*I.getTrueValue());
//  visitValue(*I.getFalseValue());
//
//  DEBUG(dbgs() << "VarInfoBuilderHelper: visited SelectInst\n");
//}
//
//
//void VarInfoBuilderHelper::visitICmpInst(ICmpInst& I) {
//  DEBUG(dbgs() << "VarInfoBuilderHelper: visiting icmp " << I << "\n");
//  visitValue(*I.getOperand(0));
//  visitValue(*I.getOperand(1));
//  DEBUG(dbgs() << "VarInfoBuilderHelper: visited icmp\n");
//}
//
//void VarInfoBuilderHelper::visitConstant(Constant& I) {
//  DEBUG(dbgs() << "VarInfoBuilderHelper: visiting Constant " << I << "\n");
//
//  if (!(isa<GlobalVariable>(I) && I.getType()->getContainedType(0)->isSingleValueType())) {
//    DEBUG(dbgs() << "VarInfoBuilderHelper: visited Constant " << I << "\n");
//    return;
//  }
//
//
//
//  auto idx = builder_.getInt32(numOfAddr_++);
//  auto address = builder_.CreatePtrToInt(&I, builder_.getIntNTy(ptr_size_));
//  auto loadInst = builder_.CreateLoad(&I);
//  // Note that I must be of pointer type because I is a global variable.
//  auto bv = builder_.CreateZExt(loadInst, builder_.getIntNTy(ptr_size_));
//  unsigned actural_size = mod_.getDataLayout()->getTypeSizeInBits(loadInst->getType());
//  Value* nbits = builder_.getIntN(ptr_size_, actural_size);
//
//  ArrayRef<Value*> Args({idx, address, bv, nbits});
//  builder_.CreateCall(fmap_[SaveVarInfo], Args);
//  DEBUG(dbgs() << "VarInfoBuilderHelper: visited Constant " << I << "\n");
//}
//
//void VarInfoBuilderHelper::visitBitCastInst(llvm::BitCastInst &I) {
//  DEBUG(dbgs() << "VarInfoBuilderHelper: visiting BitCastInst " << I << "\n");
//
//  DEBUG(dbgs() << "VarInfoBuilderHelper: visited BitCastInst\n");
//}
//
//void VarInfoBuilderHelper::visitPtrToIntInst(PtrToIntInst& I) {
//  DEBUG(dbgs() << "VarInfoBuilderHelper: visiting PtrToIntInst " << I << "\n");
////  visitValue(*I.getOperand(0));
//  DEBUG(dbgs() << "VarInfoBuilderHelper: visited PtrToIntInst\n");
//}
//
//
//void VarInfoBuilderHelper::visitTruncInst(TruncInst& I) {
//  DEBUG(dbgs() << "VarInfoBuilderHelper: visiting TruncInst " << I << "\n");
//  visitValue(*I.getOperand(0));
//  DEBUG(dbgs() << "VarInfoBuilderHelper: visited TruncInst\n");
//}
//
//void VarInfoBuilderHelper::visitZExtInst(ZExtInst& I) {
//  DEBUG(dbgs() << "VarInfoBuilderHelper: visiting ZExtInst " << I << "\n");
//  visitValue(*I.getOperand(0));
//  DEBUG(dbgs() << "VarInfoBuilderHelper: visited ZExtInst\n");
//}
//
//void VarInfoBuilderHelper::visitSExtInst(SExtInst& I) {
//  DEBUG(dbgs() << "VarInfoBuilderHelper: visiting SExtInst " << I << "\n");
//  visitValue(*I.getOperand(0));
//  DEBUG(dbgs() << "VarInfoBuilderHelper: visited SExtInst\n");
//}
//
//void VarInfoBuilderHelper::visitArgument(Argument& I) {
//  DEBUG(dbgs() << "VarInfoBuilderHelper: visiting argument " << I << "\n");
//  DEBUG(dbgs() << "VarInfoBuilderHelper: visited argument\n");
//}
//
//void VarInfoBuilderHelper::visitValue(Value& V) {
//  if (isa<Instruction>(V)) {
//    visit(cast<Instruction>(V));
//  } else if (isa<Argument>(V)) {
//    visitArgument(cast<Argument>(V));
//  } else if (isa<Constant>(V)) {
//    visitConstant(cast<Constant>(V));
//  } else {
//    DEBUG(dbgs() << "V = " << V << "\n");
//    llvm::llvm_unreachable_internal("VarInfoBuilderHelper: Unknown type of value operand!");
//  }
//}
//
//void VarInfoBuilderHelper::visitInstruction(Instruction& I) {
//  DEBUG(dbgs() << "VarInfoBuilderHelper: Instruction not handled: " << I << "\n");
//  llvm_unreachable("VarInfoBuilderHelper: Instruction not handled");
//}





/////////////////////// CloneInstVisitor ///////////////////////////////////


void CloneInstVisitor::visitBinaryOperator(BinaryOperator& I) {
  DEBUG(dbgs() << "CloneInstVisitor: visiting bop " << I << "\n");

  visitValue(*I.getOperand(0));
  auto LHS = last_cloned_inst_;
  visitValue(*I.getOperand(1));
  auto RHS = last_cloned_inst_;

  last_cloned_inst_ = builder_.CreateBinOp(I.getOpcode(), LHS, RHS);

  DEBUG(dbgs() << "CloneInstVisitor: visited bop " << I << "\n");
}
void CloneInstVisitor::visitAllocaInst(AllocaInst& I) {
  DEBUG(dbgs() << "CloneInstVisitor: visiting alloca " << I << "\n");
  last_cloned_inst_ = &I;
  DEBUG(dbgs() << "CloneInstVisitor: visited alloca\n");
}

void CloneInstVisitor::visitLoadInst(LoadInst& I) {
  DEBUG(dbgs() << "CloneInstVisitor: visiting load " << I << "\n");
  assert(I.getNumOperands() == 1 && "InstruemntVisitor: Load should have only one operand!");

  visitValue(*I.getPointerOperand());
  last_cloned_inst_ = builder_.CreateLoad(last_cloned_inst_);

  DEBUG(dbgs() << "CloneInstVisitor: visited load " << I << "\n");
}

void CloneInstVisitor::visitGetElementPtrInst(GetElementPtrInst& I) {
  DEBUG(dbgs() << "CloneInstVisitor: visiting GEP " << I << "\n");

  visitValue(*I.getPointerOperand());
  auto PtrOp = last_cloned_inst_;
  std::vector<Value*> vec;
  for (auto Idx = I.idx_begin(), E = I.idx_end(); Idx != E; ++Idx) {
    visitValue(**Idx);
    vec.push_back(last_cloned_inst_);
  }
  ArrayRef<Value *> IdxList(vec);
  last_cloned_inst_ = builder_.CreateInBoundsGEP(PtrOp, IdxList);

  DEBUG(dbgs() << "CloneInstVisitor: visited GEP\n");
}


void CloneInstVisitor::visitPHINode(PHINode& I) {
  DEBUG(dbgs() << "CloneInstVisitor: visiting PHI node " << I << "\n");
  llvm::llvm_unreachable_internal("CloneInstVisitor: assume no phi node for now");
  DEBUG(dbgs() << "CloneInstVisitor: visited PHI node\n");
}

void CloneInstVisitor::visitSelectInst(llvm::SelectInst& I) {
  DEBUG(dbgs() << "CloneInstVisitor: visiting SelectInst " << I << "\n");

  visitValue(*I.getCondition());
  auto Cond = last_cloned_inst_;
  visitValue(*I.getTrueValue());
  auto TrueValue = last_cloned_inst_;
  visitValue(*I.getFalseValue());
  auto FalseValue = last_cloned_inst_;

  last_cloned_inst_ = builder_.CreateSelect(Cond, TrueValue, FalseValue);

  DEBUG(dbgs() << "CloneInstVisitor: visited SelectInst\n");
}


void CloneInstVisitor::visitICmpInst(ICmpInst& I) {
  DEBUG(dbgs() << "CloneInstVisitor: visiting icmp " << I << "\n");
  llvm::llvm_unreachable_internal("CloneInstVisitor: assume no ICmpInst for now");
  DEBUG(dbgs() << "CloneInstVisitor: visited icmp\n");
}

void CloneInstVisitor::visitCastInst(CastInst& I) {
  DEBUG(dbgs() << "CloneInstVisitor: visiting cast " << I << "\n");
  llvm::llvm_unreachable_internal("CloneInstVisitor: assume no CastInst for now");
  DEBUG(dbgs() << "CloneInstVisitor: visited cast\n");
}

void CloneInstVisitor::visitCallInst(CallInst& I) {
  DEBUG(dbgs() << "CloneInstVisitor: visiting CallInst " << I << "\n");
  llvm::llvm_unreachable_internal("CloneInstVisitor: assume no CallInst for now");
  DEBUG(dbgs() << "CloneInstVisitor: visited CallInst\n");
}

void CloneInstVisitor::visitConstant(Constant& I) {
  DEBUG(dbgs() << "CloneInstVisitor: visiting constant " << I << "\n");
  last_cloned_inst_ = &I;
  DEBUG(dbgs() << "CloneInstVisitor: visited constant\n");
}

void CloneInstVisitor::visitTruncInst(TruncInst& I) {
  DEBUG(dbgs() << "CloneInstVisitor: visiting TruncInst " << I << "\n");
  visitValue(*I.getOperand(0));
  last_cloned_inst_ = builder_.CreateTrunc(last_cloned_inst_, I.getDestTy());
  DEBUG(dbgs() << "CloneInstVisitor: visited TruncInst\n");
}

void CloneInstVisitor::visitZExtInst(ZExtInst& I) {
  DEBUG(dbgs() << "CloneInstVisitor: visiting ZExtInst " << I << "\n");
  visitValue(*I.getOperand(0));
  last_cloned_inst_ = builder_.CreateZExt(last_cloned_inst_, I.getDestTy());
  DEBUG(dbgs() << "CloneInstVisitor: visited ZExtInst\n");
}

void CloneInstVisitor::visitSExtInst(SExtInst& I) {
  DEBUG(dbgs() << "CloneInstVisitor: visiting SExtInst " << I << "\n");
  visitValue(*I.getOperand(0));
  last_cloned_inst_ = builder_.CreateSExt(last_cloned_inst_, I.getDestTy());
  DEBUG(dbgs() << "CloneInstVisitor: visited SExtInst\n");
}

void CloneInstVisitor::visitArgument(Argument& I) {
  DEBUG(dbgs() << "CloneInstVisitor: visiting argument " << I << "\n");
  last_cloned_inst_ = &I;
  DEBUG(dbgs() << "CloneInstVisitor: visited argument\n");
}

void CloneInstVisitor::visitValue(Value& V) {
  if (isa<Instruction>(V)) {
    visit(cast<Instruction>(V));
  } else if (isa<Argument>(V)) {
    visitArgument(cast<Argument>(V));
  } else if (isa<Constant>(V)) {
    visitConstant(cast<Constant>(V));
  } else {
    DEBUG(dbgs() << "V = " << V << "\n");
    llvm::llvm_unreachable_internal("Unknown type of value operand!");
  }
}

void CloneInstVisitor::visitInstruction(Instruction& I) {
  DEBUG(dbgs() << "CloneInstVisitor: Instruction not handled: " << I << "\n");
  llvm_unreachable("CloneInstVisitor: Instruction not handled");
}
}  // namespace llsplat



