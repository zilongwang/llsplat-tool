
#define DEBUG_TYPE "backend-bmcbuilderhelper"
#include "bmcBuilderHelper.h"
#include "arrayIndexChecker.h"
#include "state.h"
#include "llvm/Support/Debug.h"
#include "llvm/Support/raw_ostream.h"
#include "llvm/IR/CFG.h"
#include "z3Utils.h"
#include <exception>
#include <algorithm>

using llvm::BasicBlock;
using llvm::Constant;
using llvm::Value;
using llvm::Constant;
using llvm::ConstantInt;
using llvm::GlobalVariable;
using llvm::Instruction;
using llvm::Argument;
using llvm::dyn_cast;
using llvm::cast;
using llvm::isa;
using llvm::dbgs;

using std::string;
using std::to_string;

z3::expr new_symbol(std::string name, width_t nbits) {
  StateManager* stateMgr = StateManager::getStateManager();
  return stateMgr->getContext().bv_const((StateManager::sym_prefix_+"_"+name).c_str(), nbits);
}



void BmcBuilderHelper::visitArgument(Argument& I) {
  DEBUG(dbgs() << "BmcBuilderHelper: visiting argument " << I << "\n");
  DEBUG(dbgs() << "BmcBuilderHelper: visited argument\n");
}

void BmcBuilderHelper::visitConstant(Constant& I) {
  DEBUG(dbgs() << "BmcBuilderHelper: visiting constant " << I << "\n");
  StateManager* stateMgr = StateManager::getStateManager();

  if (ConstantInt* CI = dyn_cast<ConstantInt>(&I)) {
    bv_t bv = CI->getValue().getZExtValue();
    width_t nbits = CI->getBitWidth();
    result_ = stateMgr->bvToZ3Expr(bv, nbits);
  } else if (GlobalVariable* GV = dyn_cast<GlobalVariable>(&I)) {
    if (GV->getType()->isSingleValueType()) {
      auto pos = std::find(ptr_value_vec_.begin(), ptr_value_vec_.end(), GV);
      assert(pos != ptr_value_vec_.end());
      index_t varIdx = pos - ptr_value_vec_.begin();
      address_t address = idx2addr_[varIdx];
      result_ = stateMgr->getSymbolicExpr(address);

      std::string name;
      llvm::raw_string_ostream buffer(name);
      buffer << I;
      idx2name_[varIdx] = buffer.str();
    }
  } else {
    throw std::runtime_error("BmcBuilderHelper: visiting constant: I assume there are only two types of constants for now.");
  }

  DEBUG(dbgs() << "BmcBuilderHelper: visited constant\n");
}

void BmcBuilderHelper::visitValue(Value& V) {
  if (isa<Instruction>(V)) {
    visit(cast<Instruction>(V));
  } else if (isa<Argument>(V)) {
    visitArgument(cast<Argument>(V));
  } else if (isa<Constant>(V)) {
    visitConstant(cast<Constant>(V));
  } else {
    DEBUG(dbgs() << "V = " << V << "\n");
    llvm::llvm_unreachable_internal("Unknown type of value operand!");
  }
}

void BmcBuilderHelper::visitAllocaInst(llvm::AllocaInst& I) {
  DEBUG(dbgs() << "BmcBuilderHelper: visiting alloca " << I << "\n");

  if (isa<llvm::StructType>(I.getAllocatedType()) || isa<llvm::ArrayType>(I.getAllocatedType())) {
    return;
  }

  auto pos = std::find(ptr_value_vec_.begin(), ptr_value_vec_.end(), &I);
  assert(pos != ptr_value_vec_.end());
  index_t varIdx = pos - ptr_value_vec_.begin();

  idx2name_[varIdx] = I.getName().data();

  DEBUG(dbgs() << "BmcBuilderHelper: visited alloca " << I << "\n");
}


void BmcBuilderHelper::visitGetElementPtrInst(llvm::GetElementPtrInst& I) {
  DEBUG(dbgs() << "BmcBuilderHelper: visiting GEP " << I << "\n");

  visitValue(*I.getPointerOperand());
  ArrayIndexChecker checker(idx2addr_, addr2version_, ptr_value_vec_);
  for (auto Idx = I.idx_begin(), E = I.idx_end(); Idx != E; ++Idx) {
    checker.visitValue(**Idx);
    visitValue(**Idx);
  }

  auto pos = std::find(ptr_value_vec_.begin(), ptr_value_vec_.end(), &I);
  assert(pos != ptr_value_vec_.end());
  index_t varIdx = pos - ptr_value_vec_.begin();

  idx2name_[varIdx] = I.getName().data();

  DEBUG(dbgs() << "BmcBuilderHelper: visited GEP\n");
}



void BmcBuilderHelper::visitSelectInst(llvm::SelectInst& I) {
  DEBUG(dbgs() << "BmcBuilderHelper: visiting SelectInst " << I << "\n");

  visitValue(*I.getCondition());
  visitValue(*I.getTrueValue());
  visitValue(*I.getFalseValue());

  DEBUG(dbgs() << "BmcBuilderHelper: visited SelectInst\n");
}

void BmcBuilderHelper::visitBinaryOperator(llvm::BinaryOperator& I) {
  DEBUG(dbgs() << "BmcBuilderHelper: visiting bop " << I << "\n");
  visitValue(*I.getOperand(0));
  z3::expr e1 = result_;
  visitValue(*I.getOperand(1));
  z3::expr e2 = result_;

  switch(I.getOpcode()) {
  case Add : result_ = e1 + e2; break;
  case Sub : result_ = e1 - e2; break;
  case Mul : result_ = e1 * e2; break;
  case SDiv: result_ = to_expr(context_, Z3_mk_bvsdiv(context_, e1, e2)); break;
  case UDiv: result_ = to_expr(context_, Z3_mk_bvudiv(context_, e1, e2)); break;
  case SRem: result_ = to_expr(context_, Z3_mk_bvsrem(context_, e1, e2)); break;
  case URem: result_ = to_expr(context_, Z3_mk_bvurem(context_, e1, e2)); break;
  default: {
    std::cerr << "BmcBuilderHelper: Unsupported bop: " << I.getOpcode();
    throw std::runtime_error("Unsupported bop");
  }
  }

  DEBUG(dbgs() << "BmcBuilderHelper: visited bop " << I << "\n");
}

void BmcBuilderHelper::visitICmpInst(llvm::ICmpInst& I) {
  DEBUG(dbgs() << "BmcBuilderHelper: visiting icmp " << I << "\n");
  visitValue(*I.getOperand(0));
  z3::expr e1 = result_;
  visitValue(*I.getOperand(1));
  z3::expr e2 = result_;

  auto stateMgr = StateManager::getStateManager();
  z3::context& context = stateMgr->getContext();
  switch(I.getPredicate()) {
    case ICMP_EQ: result_ = (e1 == e2); break;
    case ICMP_NE: result_ = (e1 != e2); break;
    case ICMP_UGT:
      {
        result_ = to_expr(context, Z3_mk_bvugt(context, e1, e2)); break;
      }
    case ICMP_UGE:
      {
        result_ = to_expr(context, Z3_mk_bvuge(context, e1, e2)); break;
      }
    case ICMP_ULT:
      {
        result_ = to_expr(context, Z3_mk_bvult(context, e1, e2)); break;
      }
    case ICMP_ULE:
      {
        result_ = to_expr(context, Z3_mk_bvule(context, e1, e2)); break;
      }
    case ICMP_SGT:
      {
        result_ = to_expr(context, Z3_mk_bvsgt(context, e1, e2)); break;
      }
    case ICMP_SGE:
      {
        result_ = to_expr(context, Z3_mk_bvsge(context, e1, e2)); break;
      }
    case ICMP_SLT:
      {
        result_ = to_expr(context, Z3_mk_bvslt(context, e1, e2)); break;
      }
    case ICMP_SLE:
      {
        result_ = to_expr(context, Z3_mk_bvsle(context, e1, e2)); break;
      }
    default:
      {
        std::cerr << "Unsupported predicate: " << I.getPredicate();
        throw std::runtime_error("Unsupported predicate.");
      }
  }

  DEBUG(dbgs() << "BmcBuilderHelper: visited icmp\n");
}

void BmcBuilderHelper::visitLoadInst(llvm::LoadInst& I) {
  DEBUG(dbgs() << "BmcBuilderHelper: visiting LoadInst " << I << "\n");
  auto stateMgr = StateManager::getStateManager();
  visitValue(*I.getPointerOperand());

  auto pos = std::find(ptr_value_vec_.begin(), ptr_value_vec_.end(), I.getPointerOperand());
  assert(pos != ptr_value_vec_.end());
  index_t varIdx = pos - ptr_value_vec_.begin();

  address_t address = idx2addr_[varIdx];
  result_ = stateMgr->getSymbolicExpr(address);
  DEBUG(dbgs() << "BmcBuilderHelper: visited LoadInst " << I << "\n");
}

void BmcBuilderHelper::visitTruncInst(llvm::TruncInst& I) {
  DEBUG(dbgs() << "BmcBuilderHelper: visiting TruncInst " << I << "\n");
  visitValue(*I.getOperand(0));

  auto stateMgr = StateManager::getStateManager();
  unsigned to_nbits = stateMgr->getModule().getDataLayout()->getTypeSizeInBits(I.getType());

  result_ =  z3::to_expr(context_, Z3_mk_extract(context_, to_nbits-1, 0, result_));
  DEBUG(dbgs() << "BmcBuilderHelper: visited TruncInst " << I << "\n");
}

void BmcBuilderHelper::visitZExtInst(llvm::ZExtInst& I) {
  DEBUG(dbgs() << "BmcBuilderHelper: visiting ZExtInst " << I << "\n");

  visitValue(*I.getOperand(0));

  auto stateMgr = StateManager::getStateManager();
  unsigned from_nbits = stateMgr->getModule().getDataLayout()->getTypeSizeInBits(I.getOperand(0)->getType());
  unsigned to_nbits = stateMgr->getModule().getDataLayout()->getTypeSizeInBits(I.getType());

  result_ = z3::to_expr(context_, Z3_mk_zero_ext(context_, to_nbits - from_nbits, result_));
  DEBUG(dbgs() << "BmcBuilderHelper: visited ZExtInst\n");
}

void BmcBuilderHelper::visitSExtInst(llvm::SExtInst& I) {
  DEBUG(dbgs() << "BmcBuilderHelper: visiting SExtInst " << I << "\n");

  visitValue(*I.getOperand(0));

  auto stateMgr = StateManager::getStateManager();
  unsigned from_nbits = stateMgr->getModule().getDataLayout()->getTypeSizeInBits(I.getOperand(0)->getType());
  unsigned to_nbits = stateMgr->getModule().getDataLayout()->getTypeSizeInBits(I.getType());

  result_ = z3::to_expr(context_, Z3_mk_sign_ext(context_, to_nbits - from_nbits, result_));
  DEBUG(dbgs() << "BmcBuilderHelper: visited SExtInst\n");
}

void BmcBuilderHelper::visitBranchInst(llvm::BranchInst& I) {
  DEBUG(dbgs() << "BmcBuilderHelper: visiting BranchInst" << I << "\n");

  BasicBlock* CurrBB = I.getParent();
  BasicBlock* BB = *succ_begin(CurrBB);
  if (I.isUnconditional()) {
    if (std::find(SortedEDom_.begin(), SortedEDom_.end(), BB) != SortedEDom_.end()) {
      ctx_expr_map_edom_[BB].push_back(ctx_map_edom_.at(CurrBB));
    } else
      ctx_expr_map_dest_[BB].push_back(ctx_map_edom_.at(CurrBB));
    DEBUG(dbgs() << "BmcBuilderHelper: visited BranchInst" << I << "\n");
    return;
  }

  z3::expr e_icmp = context_.bool_val(true);
  if (I.getParent() == Dominator_) {
    e_icmp = ctx_;
  } else {
    visitValue(*I.getCondition());
    e_icmp = getAndResetResult();
  }

  if (!e_icmp.is_bool()) {
    assert(e_icmp.is_bv() && e_icmp.get_sort().bv_size() == 1);
    e_icmp = e_icmp == 1;
  }

  BasicBlock* BB1 = *succ_begin(CurrBB);
  BasicBlock* BB2 = *(++succ_begin(CurrBB));

//  dbgs() << "///--- currbb = " << CurrBB->getName() << "\n";
//  dbgs() << "///--- BB1 = " << BB1->getName() << "\n";
//  dbgs() << "///--- BB2 = " << BB2->getName() << "\n";
  if (std::find(SortedEDom_.begin(), SortedEDom_.end(), BB1) != SortedEDom_.end()) {
    if (CurrBB == Dominator_)
      ctx_expr_map_edom_[BB1].push_back(e_icmp);
    else
      ctx_expr_map_edom_[BB1].push_back(ctx_map_edom_.at(CurrBB) && e_icmp);
  } else
    ctx_expr_map_dest_[BB1].push_back(ctx_map_edom_.at(CurrBB) && e_icmp);

  if (std::find(SortedEDom_.begin(), SortedEDom_.end(), BB2) != SortedEDom_.end()) {
    if (CurrBB == Dominator_)
      ctx_expr_map_edom_[BB2].push_back(!e_icmp);
    else
      ctx_expr_map_edom_[BB2].push_back(ctx_map_edom_.at(CurrBB) && !e_icmp);
  } else
    ctx_expr_map_dest_[BB2].push_back(ctx_map_edom_.at(CurrBB) && !e_icmp);

  DEBUG(dbgs() << "BmcBuilderHelper: visited BranchInst\n");
}


void BmcBuilderHelper::visitSwitchInst(llvm::SwitchInst& I) {
  DEBUG(dbgs() << "BmcBuilderHelper: visiting SwitchInst" << I << "\n");

  z3::expr e_icmp = context_.bool_val(true);

  if (I.getParent() == Dominator_) {
    e_icmp = ctx_;
  } else {
    visitValue(*I.getCondition());
    e_icmp = getAndResetResult();
  }

//  if (!e_icmp.is_bool()) {
//    assert(e_icmp.is_bv() && e_icmp.get_sort().bv_size() == 1);
//    e_icmp = e_icmp == 1;
//  }

  BasicBlock* CurrBB = I.getParent();
  std::vector<z3::expr> not_default;
  for (auto& CS : I.cases()) {
    visitValue(*CS.getCaseValue());
    z3::expr current_case = getAndResetResult();
    auto BB = CS.getCaseSuccessor();
    if (std::find(SortedEDom_.begin(), SortedEDom_.end(), BB) != SortedEDom_.end()) {
      if (CurrBB == Dominator_)
        ctx_expr_map_edom_[BB].push_back(e_icmp == current_case);
      else
        ctx_expr_map_edom_[BB].push_back(ctx_map_edom_.at(CurrBB) && (e_icmp == current_case));
    } else
        ctx_expr_map_dest_[BB].push_back(ctx_map_edom_.at(CurrBB) && (e_icmp == current_case));

    not_default.push_back(e_icmp == current_case);
  }

  auto DefaultBB = I.getDefaultDest();
  if (std::find(SortedEDom_.begin(), SortedEDom_.end(), DefaultBB) != SortedEDom_.end()) {
    if (CurrBB == Dominator_)
      ctx_expr_map_edom_[DefaultBB].push_back(!getBigOr(not_default, context_));
    else
      ctx_expr_map_edom_[DefaultBB].push_back(ctx_map_edom_.at(CurrBB) && !getBigOr(not_default, context_));
  } else
      ctx_expr_map_edom_[DefaultBB].push_back(ctx_map_edom_.at(CurrBB) && !getBigOr(not_default, context_));

  DEBUG(dbgs() << "BmcBuilderHelper: visited SwitchInst\n");
}


void BmcBuilderHelper::visitStoreInst(llvm::StoreInst& I) {
  DEBUG(dbgs() << "BmcBuilderHelper: visiting store" << I << "\n");
  auto stateMgr = StateManager::getStateManager();

  auto pos = std::find(ptr_value_vec_.begin(), ptr_value_vec_.end(), I.getPointerOperand());
  assert(pos != ptr_value_vec_.end());
  index_t varIdx = pos - ptr_value_vec_.begin();
  assert(addr2varinfo_.find(idx2addr_[varIdx]) != addr2varinfo_.end());
  address_t address = addr2varinfo_[idx2addr_[varIdx]]->addr;
  width_t nbits = addr2varinfo_[idx2addr_[varIdx]]->nbits;

  Value* val = I.getValueOperand();
  visitValue(*val);
  z3::expr symVal = getAndResetResult();
  z3::expr symAddr = stateMgr->getSymbolicExpr(address);

  assert(addr2varname_.find(idx2addr_[varIdx]) != addr2varname_.end() &&
      "BmcBuilderHelper: Can't find the address. This should not happen.");

  std::string name = addr2varname_[idx2addr_[varIdx]] + "_v" + to_string(++addr2version_[address]) + "_b" + to_string(bmc_no_);
  z3::expr symNewAddr = new_symbol(name, nbits);
  stateMgr->insertToSymbolicStore(address, symNewAddr);
  formula_map_edom_[I.getParent()].push_back(symNewAddr == z3::ite(ctx_map_edom_.at(I.getParent()), symVal, symAddr));
  DEBUG(dbgs() << "BmcBuilderHelper: visited store" << I << "\n");
}

