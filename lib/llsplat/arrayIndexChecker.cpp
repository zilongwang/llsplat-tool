


/*******************************************************************************
 * This file is distributed under the MIT License. See LICENSE.TXT for details.
 *******************************************************************************/

#define DEBUG_TYPE "backend-ArrayIndexChecker"
#include "arrayIndexChecker.h"
#include "llvm/Support/Debug.h"
#include "llvm/IR/Instructions.h"
#include <vector>

using namespace llvm;


void ArrayIndexChecker::visitBinaryOperator(BinaryOperator& I) {
  DEBUG(dbgs() << "ArrayIndexChecker: visiting bop " << I << "\n");

  for(auto& op : I.operands()) {
    visitValue(*(op.get()));
  }

  DEBUG(dbgs() << "ArrayIndexChecker: visited bop " << I << "\n");
}

void ArrayIndexChecker::visitAllocaInst(AllocaInst& I) {
  DEBUG(dbgs() << "ArrayIndexChecker: visiting alloca " << I << "\n");

  if (isa<StructType>(I.getAllocatedType()) || isa<ArrayType>(I.getAllocatedType())) {
    return;
  }

  auto pos = std::find(ptr_value_vec_.begin(), ptr_value_vec_.end(), &I);
  assert(pos != ptr_value_vec_.end());
  index_t varIdx = pos - ptr_value_vec_.begin();

  assert(idx2addr_.find(varIdx) != idx2addr_.end());
  if (addr2version_[idx2addr_[varIdx]] != 0)
    throw ArrayIndexIsNotConstant;

  DEBUG(dbgs() << "ArrayIndexChecker: visited alloca\n");
}

void ArrayIndexChecker::visitLoadInst(LoadInst& I) {
  DEBUG(dbgs() << "ArrayIndexChecker: visiting load " << I << "\n");

  visitValue(*I.getPointerOperand());

  if (I.getType()->isPointerTy()) {
    auto pos = std::find(ptr_value_vec_.begin(), ptr_value_vec_.end(), &I);
    assert(pos != ptr_value_vec_.end());
    index_t varIdx = pos - ptr_value_vec_.begin();

    assert(idx2addr_.find(varIdx) != idx2addr_.end());
    if (addr2version_[idx2addr_[varIdx]] != 0)
      throw ArrayIndexIsNotConstant;
  }
  DEBUG(dbgs() << "ArrayIndexChecker: visited load\n");
}

void ArrayIndexChecker::visitStoreInst(StoreInst& I) {
  DEBUG(dbgs() << "ArrayIndexChecker: visiting store " << I << "\n");
  llvm_unreachable_internal("ArrayIndexChecker should not visit StoreInst.");
  DEBUG(dbgs() << "ArrayIndexChecker: visited store\n");
}

void ArrayIndexChecker::visitGetElementPtrInst(GetElementPtrInst& I) {
  DEBUG(dbgs() << "ArrayIndexChecker: visiting GEP " << I << "\n");

  visitValue(*I.getPointerOperand());
  for (auto Idx = I.idx_begin(), E = I.idx_end(); Idx != E; ++Idx) {
    visitValue(**Idx);
  }

  auto pos = std::find(ptr_value_vec_.begin(), ptr_value_vec_.end(), &I);
  assert(pos != ptr_value_vec_.end());
  index_t varIdx = pos - ptr_value_vec_.begin();

  assert(idx2addr_.find(varIdx) != idx2addr_.end());
  if (addr2version_[idx2addr_[varIdx]] != 0)
    throw ArrayIndexIsNotConstant;;

  DEBUG(dbgs() << "ArrayIndexChecker: visited GEP\n");
}

void ArrayIndexChecker::visitPHINode(PHINode& I) {
  DEBUG(dbgs() << "ArrayIndexChecker: visiting PHI node " << I << "\n");
  llvm::llvm_unreachable_internal("ArrayIndexChecker: assume no phi node for now");
  DEBUG(dbgs() << "ArrayIndexChecker: visited PHI node\n");
}

void ArrayIndexChecker::visitSelectInst(llvm::SelectInst& I) {
  DEBUG(dbgs() << "ArrayIndexChecker: visiting SelectInst " << I << "\n");

  visitValue(*I.getCondition());
  visitValue(*I.getTrueValue());
  visitValue(*I.getFalseValue());

  DEBUG(dbgs() << "ArrayIndexChecker: visited SelectInst\n");
}


void ArrayIndexChecker::visitICmpInst(ICmpInst& I) {
  DEBUG(dbgs() << "ArrayIndexChecker: visiting icmp " << I << "\n");
  visitValue(*I.getOperand(0));
  visitValue(*I.getOperand(1));
  DEBUG(dbgs() << "ArrayIndexChecker: visited icmp\n");
}

void ArrayIndexChecker::visitCastInst(CastInst& I) {
  DEBUG(dbgs() << "ArrayIndexChecker: visiting cast " << I << "\n");
  llvm::llvm_unreachable_internal("ArrayIndexChecker: assume no CastInst for now");
  DEBUG(dbgs() << "ArrayIndexChecker: visited cast\n");
}


void ArrayIndexChecker::visitTruncInst(TruncInst& I) {
  DEBUG(dbgs() << "ArrayIndexChecker: visiting TruncInst " << I << "\n");
  visitValue(*I.getOperand(0));
  DEBUG(dbgs() << "ArrayIndexChecker: visited TruncInst\n");
}

void ArrayIndexChecker::visitZExtInst(ZExtInst& I) {
  DEBUG(dbgs() << "ArrayIndexChecker: visiting ZExtInst " << I << "\n");
  visitValue(*I.getOperand(0));
  DEBUG(dbgs() << "ArrayIndexChecker: visited ZExtInst\n");
}

void ArrayIndexChecker::visitSExtInst(SExtInst& I) {
  DEBUG(dbgs() << "ArrayIndexChecker: visiting SExtInst " << I << "\n");
  visitValue(*I.getOperand(0));
  DEBUG(dbgs() << "ArrayIndexChecker: visited SExtInst\n");
}

void ArrayIndexChecker::visitArgument(Argument& I) {
  DEBUG(dbgs() << "ArrayIndexChecker: visiting argument " << I << "\n");
  DEBUG(dbgs() << "ArrayIndexChecker: visited argument\n");
}

void ArrayIndexChecker::visitValue(Value& V) {
  if (isa<Instruction>(V)) {
    visit(cast<Instruction>(V));
  } else if (isa<Argument>(V)) {
    visitArgument(cast<Argument>(V));
  } else if (isa<Constant>(V)) {
    visitConstant(cast<Constant>(V));
  } else {
    DEBUG(dbgs() << "V = " << V << "\n");
    llvm::llvm_unreachable_internal("Unknown type of value operand!");
  }
}

void ArrayIndexChecker::visitInstruction(Instruction& I) {
  DEBUG(dbgs() << "ArrayIndexChecker: Instruction not handled: " << I << "\n");
  llvm_unreachable("ArrayIndexChecker: Instruction not handled");
}


void ArrayIndexChecker::visitConstant(Constant& I) {
  DEBUG(dbgs() << "ArrayIndexChecker: visiting Constant " << I << "\n");
  DEBUG(dbgs() << "ArrayIndexChecker: visited Constant\n");
}






