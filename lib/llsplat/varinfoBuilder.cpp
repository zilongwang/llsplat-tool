
/*******************************************************************************
 * This file is distributed under the MIT License. See LICENSE.TXT for details.
 *******************************************************************************/

#define DEBUG_TYPE "frontend-varinfoBuilder"
#include "varinfoBuilder.h"
#include "irUtils.h"
#include "llvm/Support/Debug.h"

using namespace llvm;

namespace llsplat {



void VarInfoBuilder::visitBranchInst(llvm::BranchInst& I) {
  DEBUG(dbgs() << "VarInfoBuilder: visiting BranchInst" << I << "\n");

  BasicBlock* BB = *succ_begin(I.getParent());
  if (I.isUnconditional()) {
    visitBasicBlock(BB);
    DEBUG(dbgs() << "VarInfoBuilder: visited BranchInst" << I << "\n");
    return;
  }

  helper_.visitValue(*I.getCondition());

  BasicBlock* BB1 = *succ_begin(I.getParent());
  BasicBlock* BB2 = *(++succ_begin(I.getParent()));
  visitBasicBlock(BB1);
  visitBasicBlock(BB2);

  DEBUG(dbgs() << "VarInfoBuilder: visited BranchInst\n");
}


void VarInfoBuilder::visitSwitchInst(llvm::SwitchInst& I) {
  DEBUG(dbgs() << "VarInfoBuilder: visiting SwitchInst" << I << "\n");

  helper_.visitValue(*I.getCondition());


  for (auto& CS : I.cases()) {
    helper_.visitValue(*CS.getCaseValue());
    visitBasicBlock(CS.getCaseSuccessor());
  }

  helper_.visitBasicBlock(*I.getDefaultDest());


  DEBUG(dbgs() << "VarInfoBuilder: visited SwitchInst\n");
}

void VarInfoBuilder::visitStoreInst(StoreInst& I) {
  DEBUG(dbgs() << "VarInfoBuilder: visiting store " << I << "\n");

  helper_.visitValue(*I.getPointerOperand());
  helper_.visitValue(*I.getValueOperand());

  DEBUG(dbgs() << "VarInfoBuilder: visited store\n");
}

void VarInfoBuilder::visitBasicBlock(llvm::BasicBlock* BB) {
  DEBUG(dbgs() << "VarInfoBuilder: visiting BasicBlock " << BB->getName() << "\n");
  if (std::find(EDom_.begin(), EDom_.end(), BB) == EDom_.end()) {
    DEBUG(dbgs() << "VarInfoBuilder: visited BasicBlock " << BB->getName() << "\n");
    return;
  }

  int n = getNumPredecessors(BB);
  if (++num_of_visited_[BB] != n) {
    DEBUG(dbgs() << "VarInfoBuilder: visited BasicBlock " << BB->getName() << "\n");
    return;
  }

  for (auto& I : *BB)
    visit(I);
  DEBUG(dbgs() << "VarInfoBuilder: visited BasicBlock " << BB->getName() << "\n");
}

}  // namespace llsplat



