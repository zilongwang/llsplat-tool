/*******************************************************************************
 * This file is distributed under the MIT License. See LICENSE.TXT for details.
 *******************************************************************************/

#include "z3Utils.h"

z3::expr getBigOr(std::vector<z3::expr>& v, z3::context& ctx) {
  auto res = ctx.bool_val(false);
  if (v.empty())
    return res;

  res = v[0];
  for (auto i = 1; i < v.size(); ++i)
    res = res || v[i];
  return res;
}

z3::expr getBigAnd(std::vector<z3::expr>& v, z3::context& ctx) {
  auto res = ctx.bool_val(true);
  if (v.empty())
    return res;
  res = v[0];
  for (auto i = 1; i < v.size(); ++i)
    res = res && v[i];
  return res;
}
