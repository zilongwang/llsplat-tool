/*******************************************************************************
 * This file is distributed under the MIT License. See LICENSE.TXT for details.
 *******************************************************************************/

#define DEBUG_TYPE "irutils"
#include "irUtils.h"
#include "algorithm"
#include <queue>
#include <set>



bool isSuccessor(BasicBlock* FromBB, BasicBlock* ToBB) {
  for (auto PI = succ_begin(FromBB), E = succ_end(FromBB); PI != E; ++PI) {
    if (*PI == ToBB)
      return true;
  }
  return false;
}

//bool isAcyclic(Function& fn, SmallVector<BasicBlock*, 16>& v) {
//  SmallVector< std::pair< const BasicBlock *, const BasicBlock * >, 16 > result;
//  FindFunctionBackedges(fn, result);
//
//  DEBUG(for (auto& kvp : result)
//    errs() << kvp.first->getName() << " " << kvp.second->getName() << "\n";
//  for (auto& BB : v)
//    errs() << BB->getName() << "\n";);
//
//  for (auto& BB : v) {
//    for (auto SI = succ_begin(BB), E = succ_end(BB); SI != E; ++SI)
//      for (auto& kvp : result)
//        if (kvp.first == BB && kvp.second == *SI && std::find(v.begin(), v.end(), kvp.second) != v.end())
//          return false;
//  }
//
//  return true;
//}

bool isSourceOrTargetOfBackEdge(Function& fn, BasicBlock& BB) {
  SmallVector< std::pair< const BasicBlock *, const BasicBlock * >, 16 > result;
  FindFunctionBackedges(fn, result);

  for (auto PI = pred_begin(&BB), E = pred_end(&BB); PI != E; ++PI)
    for (auto& kvp : result)
      if (kvp.first == *PI && kvp.second == &BB)
        return true;
  for (auto SI = succ_begin(&BB), E = succ_end(&BB); SI != E; ++SI)
    for (auto& kvp : result)
      if (kvp.first == &BB && kvp.second == *SI)
        return true;

  return false;
}

bool hasFnCall(BasicBlock* BB) {
  for (auto& I : *BB) {
    if (CallInst* CI = dyn_cast<CallInst>(&I)) {
      if (CI->getCalledFunction() == NULL ||
          CI->getCalledFunction()->getName() != "printf")
        return true;
    }
  }
  return false;
}

bool hasPhiNode(BasicBlock* BB) {
  for (auto& I : *BB)
    if (isa<llvm::PHINode>(I))
      return true;
  return false;

}


int getNumPredecessors(BasicBlock* BB) {
  int cnt = 0;
  for (auto PI = pred_begin(BB), E = pred_end(BB); PI != E; ++PI)
    cnt++;
  return cnt;
}

int getNumSuccessors(BasicBlock* BB) {
  return BB->getTerminator()->getNumSuccessors();
}

int hasNoSuccessor(BasicBlock* BB) {
  return succ_begin(BB) == succ_end(BB);
}

SmallVector<BasicBlock *, 16>& getEffecitveDominance(SmallVector<BasicBlock *, 16>&DominatedBBs) {
  std::set<BasicBlock *> Marked;
  for (auto& BB : DominatedBBs)
    if (hasFnCall(BB) || hasNoSuccessor(BB) || isSourceOrTargetOfBackEdge(*BB->getParent(), *BB))
      Marked.insert(BB);

  //    SmallVector<BasicBlock *, 16> EDominatedBBs;
  std::deque<BasicBlock *> WL(Marked.begin(), Marked.end());

  while (!WL.empty()) {
    BasicBlock* BB = WL.front();
    WL.pop_front();
    for (auto PI = succ_begin(BB), E = succ_end(BB); PI != E; ++PI) {
      if (Marked.find(*PI) == Marked.end() &&
          std::find(DominatedBBs.begin(), DominatedBBs.end(), *PI) != DominatedBBs.end()) {
        Marked.insert(*PI);
        WL.push_back(*PI);
      }
    }
  }

  // Compute Effective DominatedBBs.
  DominatedBBs.erase(std::remove_if(DominatedBBs.begin(),
      DominatedBBs.end(),
      [&, Marked](BasicBlock* BB) { return Marked.find(BB) != Marked.end();}),
      DominatedBBs.end());
  return DominatedBBs;
}

//Instruction* getFirstNotOfPHiNode(BasicBlock* BB) {
//  for (auto& I : *BB)
//    if (!isa<llvm::PHINode>(I))
//      return &I;
//  llvm::llvm_unreachable_internal("getFirstNotOfPHiNode: should not reach here");
//}

bool hasPointerAssignment(SmallVector<BasicBlock *, 16>& BBs) {
  for (auto& BB : BBs) {
    if (std::any_of(BB->begin(), BB->end(), [] (Instruction& I) {
      if (llvm::StoreInst* SI = llvm::dyn_cast<llvm::StoreInst>(&I)) {
        return SI->getPointerOperand()->getType()->getContainedType(0)->isPointerTy();
      } else {
        return false;
      }
    })) {
      return true;
    }
  }
  return false;

}

Constant* createConstString(llvm::StringRef Name, llvm::Module* mod) {
  llvm::LLVMContext& context = mod->getContext();
  ArrayType* ArrayTy_Name_addr = ArrayType::get(IntegerType::get(context, 8), Name.size()+1);
  GlobalVariable* gvar_array__str_Name_addr = new GlobalVariable(/*Module=*/*mod,
      /*Type=*/ArrayTy_Name_addr,
      /*isConstant=*/true,
      /*Linkage=*/GlobalValue::PrivateLinkage,
      /*Initializer=*/0, // has initializer, specified below
      /*Name=*/".str_"+Name);
  gvar_array__str_Name_addr->setAlignment(1);

  // Constant Definitions
  ConstantInt* const_int32_NULL = ConstantInt::get(context, APInt(64, StringRef("0"), 10));
  ArrayRef<Constant*> const_ptr_indices {const_int32_NULL, const_int32_NULL};
  Constant* const_array_Name_addr = ConstantDataArray::getString(context, Name, true);
  Constant* const_ptr_Name_addr = ConstantExpr::getGetElementPtr(gvar_array__str_Name_addr, const_ptr_indices);
  // Global Variable Definitions
  gvar_array__str_Name_addr->setInitializer(const_array_Name_addr);

  return const_ptr_Name_addr;
}


llvm::SmallVector<llvm::BasicBlock*, 16> TopologicalSort::sort() {
  for (auto& BB : StartBBs_)
    visitBasicBlock(BB);
  return result_;
}

void TopologicalSort::visitBasicBlock(llvm::BasicBlock* BB) {
  DEBUG(dbgs() << "TopologicalSort: visiting BasicBlock " << BB->getName() << "\n");
  if (std::find(EDom_.begin(), EDom_.end(), BB) == EDom_.end()) {
    DEBUG(dbgs() << "TopologicalSort: visited BasicBlock " << BB->getName() << "\n");
    return;
  }

  int n = getNumPredecessors(BB);
  if (++num_of_visited_[BB] != n) {
    DEBUG(dbgs() << "TopologicalSort: visited BasicBlock " << BB->getName() << "\n");
    return;
  }

  result_.push_back(BB);
  for (auto PI = succ_begin(BB), E = succ_end(BB); PI != E; ++PI)
    visitBasicBlock(*PI);
  DEBUG(dbgs() << "TopologicalSort: visited BasicBlock " << BB->getName() << "\n");
}

