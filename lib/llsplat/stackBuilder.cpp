/*******************************************************************************
 * This file is distributed under the MIT License. See LICENSE.TXT for details.
 *******************************************************************************/

#define DEBUG_TYPE "frontend-stackBuilder"
#include "stackBuilder.h"
#include "llvm/Support/Debug.h"

using namespace llvm;

namespace llsplat {

void StackBuilder::visitBinaryOperator(BinaryOperator& I) {
  DEBUG(dbgs() << "StackBuilder: visiting bop\n");

  for(auto& op : I.operands()) {
    visitValue(*(op.get()));
  }

  unsigned size = mod_.getDataLayout()->getPointerSizeInBits();
  Value* inst = builder_.getIntN(size, I.getOpcode());
  ArrayRef<Value*> Args({inst});
  builder_.CreateCall(fmap_[PushBop], Args);

  DEBUG(dbgs() << "StackBuilder: visited bop\n");
}

void StackBuilder::visitAllocaInst(AllocaInst& I) {
  DEBUG(dbgs() << "StackBuilder: visiting alloca " << I << "\n");
  unsigned size = mod_.getDataLayout()->getPointerSizeInBits();
  unsigned actural_size = mod_.getDataLayout()->getTypeSizeInBits(I.getType());

  assert(size >= actural_size && "StackBuilder: visitAllocaInst: bad bit truncation");
  Value* bv = builder_.CreatePtrToInt(&I, builder_.getIntNTy(size));
  Value* nbits = builder_.getIntN(size, actural_size);

  ArrayRef<Value*> Args({bv, nbits});
  builder_.CreateCall(fmap_[PushC], Args);
  DEBUG(dbgs() << "StackBuilder: visited alloca\n");
}

void StackBuilder::visitLoadInst(LoadInst& I) {
  DEBUG(dbgs() << "StackBuilder: visiting load " << I << "\n");
  assert(I.getNumOperands() == 1 && "InstruemntVisitor: Load should have only one operand!");

  unsigned size = mod_.getDataLayout()->getPointerSizeInBits();
  Value* address = builder_.CreatePtrToInt(I.getPointerOperand(), builder_.getIntNTy(size));

  unsigned actural_size = mod_.getDataLayout()->getTypeSizeInBits(I.getType());
  assert(size >= actural_size && "StackBuilder: visitLoadInst: bad bit truncation");

  Value* bv = isa<PointerType>(I.getType()) ?
      builder_.CreatePtrToInt(&I, builder_.getIntNTy(size)) :
      builder_.CreateZExt(&I, builder_.getIntNTy(size));

  Value* nbits = builder_.getIntN(size, actural_size);
  ArrayRef<Value*> Args({address, bv, nbits});

  builder_.CreateCall(fmap_[PushVar], Args);
  DEBUG(dbgs() << "StackBuilder: visited load\n");
}

void StackBuilder::visitGetElementPtrInst(GetElementPtrInst& I) {
  DEBUG(dbgs() << "StackBuilder: visiting GEP " << I << "\n");
  unsigned size = mod_.getDataLayout()->getPointerSizeInBits();
  Value* inst = builder_.CreatePtrToInt(&I, builder_.getIntNTy(size));

  unsigned actural_size = mod_.getDataLayout()->getTypeSizeInBits(I.getType());
  Value* inst2 = builder_.getIntN(size, actural_size);
  ArrayRef<Value*> Args({inst, inst2});
  builder_.CreateCall(fmap_[PushC], Args);
  DEBUG(dbgs() << "StackBuilder: visited GEP\n");
}


void StackBuilder::visitPHINode(PHINode& I) {
  DEBUG(dbgs() << "StackBuilder: visiting PHI node " << I << "\n");

  Value* inst = builder_.getInt64((uint64_t)&I);
  ArrayRef<Value*> Args({inst});
  builder_.CreateCall(fmap_[PushPhi], Args);

  int cnt = 0;
  for (auto BB = I.block_begin(), E = I.block_end(); BB != E; ++BB, ++cnt) {
    auto Op = I.getOperand(cnt);

    Instruction* OldInsertPoint = builder_.GetInsertPoint();

    DEBUG(
        errs() << "BB = " << (*BB)->getName() << "\n";
        errs() << "Op = " << Op->getName() << "\n";
        errs() << "OldInsertPoint = " << *OldInsertPoint << "\n";);

    builder_.SetInsertPoint((*BB)->getTerminator());
    visitValue(*Op);

    builder_.CreateCall(fmap_[UpdatePhiMap], Args);
    builder_.SetInsertPoint(OldInsertPoint);
  }

  DEBUG(dbgs() << "StackBuilder: visited PHI node\n");
}

void StackBuilder::visitSelectInst(llvm::SelectInst& I) {
  DEBUG(dbgs() << "StackBuilder: visiting SelectInst " << I << "\n");

  for (auto& op : I.operands()) {
    visitValue(*op.get());
  }

  builder_.CreateCall(fmap_[PushSelect]);

  DEBUG(dbgs() << "StackBuilder: visited SelectInst\n");
}


void StackBuilder::visitICmpInst(ICmpInst& I) {
  DEBUG(dbgs() << "StackBuilder: visiting icmp " << I << "\n");

  for (auto& op : I.operands()) {
    visitValue(*op.get());
  }

  unsigned size = mod_.getDataLayout()->getPointerSizeInBits();
  unsigned actural_size = mod_.getDataLayout()->getTypeSizeInBits(I.getType());

  assert(size >= actural_size && "StackBuilder: visitICmpInst: bad bit truncation");
  Value* bv = builder_.CreateZExt(&I, builder_.getIntNTy(size));
  Value* nbits = builder_.getIntN(size, actural_size);

  Value* opcode = builder_.getIntN(size, I.getPredicate());
  ArrayRef<Value*> Args({bv, nbits, opcode});
  builder_.CreateCall(fmap_[PushICmp], Args);

  DEBUG(dbgs() << "StackBuilder: visited icmp\n");
}

//void StackBuilder::visitCastInst(CastInst& I) {
//  DEBUG(dbgs() << "StackBuilder: visiting cast " << I << "\n");
//
//  DEBUG(dbgs() << "StackBuilder: visited cast\n");
//}




void StackBuilder::visitCallInst(CallInst& I) {
  DEBUG(dbgs() << "StackBuilder: visiting CallInst " << I << "\n");

  Function* Fn = I.getCalledFunction();
  if (Fn && Fn->getName().startswith("llvm.expect")) {
    visitValue(*I.getOperand(0));
    return;
  }

  // When Fn is not defined, we push the returned value into the symbolic stack.
  // Otherwise, we do nothing because the called function will push its returned symbolic
  // expression into the symbolic stack.
  if (Fn == NULL || Fn->isDeclaration()) {
    unsigned size = mod_.getDataLayout()->getPointerSizeInBits();
    unsigned actural_size = mod_.getDataLayout()->getTypeSizeInBits(I.getType());
    assert(size >= actural_size && "StackBuilder: visitCallInst: bad bit truncation");
    Value* bv = nullptr;
    if (isa<PointerType>(I.getType()))
    {
      bv = builder_.CreatePtrToInt(&I, builder_.getIntNTy(size));
    } else {
      bv = builder_.CreateZExt(&I, builder_.getIntNTy(size));
    }
    Value* nbits = builder_.getIntN(size, actural_size);

    ArrayRef<Value*> Args({bv, nbits});
    builder_.CreateCall(fmap_[PushC], Args);
  }


  DEBUG(dbgs() << "StackBuilder: visited CallInst\n");
}

void StackBuilder::visitConstant(Constant& I) {
  DEBUG(dbgs() << "StackBuilder: visiting constant " << I << "\n");

  unsigned size = mod_.getDataLayout()->getPointerSizeInBits();
  unsigned actural_size = mod_.getDataLayout()->getTypeSizeInBits(I.getType());
  assert(size >= actural_size && "StackBuilder: visitConstant: bad bit truncation");
  Value* bv = nullptr;
  if (isa<PointerType>(I.getType()))
  {
    bv = builder_.CreatePtrToInt(&I, builder_.getIntNTy(size));
  } else {
    bv = builder_.CreateZExt(&I, builder_.getIntNTy(size));
  }
  Value* nbits = builder_.getIntN(size, actural_size);

  ArrayRef<Value*> Args({bv, nbits});
  builder_.CreateCall(fmap_[PushC], Args);



  DEBUG(dbgs() << "StackBuilder: visited constant\n");
}

void StackBuilder::visitTruncInst(TruncInst& I) {
  DEBUG(dbgs() << "StackBuilder: visiting TruncInst " << I << "\n");
  for (auto& op : I.operands()) {
    visitValue(*op.get());
  }

  unsigned size = mod_.getDataLayout()->getPointerSizeInBits();
  unsigned actural_size = mod_.getDataLayout()->getTypeSizeInBits(I.getType());

  assert(size >= actural_size && "StackBuilder: visitTruncInst: bad bit truncation");
  Value* bv = builder_.CreateZExt(&I, builder_.getIntNTy(size));
  Value* nbits = builder_.getIntN(size, actural_size);

  ArrayRef<Value*> Args({bv, nbits});
  builder_.CreateCall(fmap_[PushTrunc], Args);
  DEBUG(dbgs() << "StackBuilder: visited TruncInst\n");
}

void StackBuilder::visitZExtInst(ZExtInst& I) {
  DEBUG(dbgs() << "StackBuilder: visiting ZExtInst " << I << "\n");
  for (auto& op : I.operands()) {
    visitValue(*op.get());
  }

  unsigned size = mod_.getDataLayout()->getPointerSizeInBits();
  unsigned actural_size = mod_.getDataLayout()->getTypeSizeInBits(I.getType());

  assert(size >= actural_size && "StackBuilder: visitZExtInst: bad bit truncation");
  Value* bv = builder_.CreateZExt(&I, builder_.getIntNTy(size));
  Value* nbits = builder_.getIntN(size, actural_size);

  ArrayRef<Value*> Args({bv, nbits});
  builder_.CreateCall(fmap_[PushZExt], Args);
  DEBUG(dbgs() << "StackBuilder: visited ZExtInst\n");
}

void StackBuilder::visitSExtInst(SExtInst& I) {
  DEBUG(dbgs() << "StackBuilder: visiting SExtInst " << I << "\n");

  for (auto& op : I.operands()) {
    visitValue(*op.get());
  }

  unsigned size = mod_.getDataLayout()->getPointerSizeInBits();
  unsigned actural_size = mod_.getDataLayout()->getTypeSizeInBits(I.getType());

  assert(size >= actural_size && "StackBuilder: visitSExtInst: bad bit truncation");
  Value* bv = builder_.CreateZExt(&I, builder_.getIntNTy(size));
  Value* nbits = builder_.getIntN(size, actural_size);

  ArrayRef<Value*> Args({bv, nbits});
  builder_.CreateCall(fmap_[PushSExt], Args);
  DEBUG(dbgs() << "StackBuilder: visited SExtInst\n");
}

void StackBuilder::visitPtrToIntInst(PtrToIntInst& I) {
  DEBUG(dbgs() << "StackBuilder: visiting PtrToIntInst " << I << "\n");

  for (auto& op : I.operands())
    visitValue(*op.get());

  unsigned size = mod_.getDataLayout()->getPointerSizeInBits();
  unsigned actural_size = mod_.getDataLayout()->getTypeSizeInBits(I.getType());

  assert(size >= actural_size && "StackBuilder: visitSExtInst: bad bit truncation");
  Value* bv = builder_.CreateZExt(&I, builder_.getIntNTy(size));
  Value* nbits = builder_.getIntN(size, actural_size);
  Value* ptr_nbits = builder_.getIntN(size, size);

  ArrayRef<Value*> Args({bv, nbits, ptr_nbits});
  builder_.CreateCall(fmap_[PushPtrToInt], Args);
  DEBUG(dbgs() << "StackBuilder: visited PtrToIntInst\n");
}

// <result> = inttoptr <ty> <value> to <ty2>             ; yields ty2
// The 'inttoptr' instruction converts value to type ty2 by applying
// either a zero extension or a truncation depending on the size of the integer value.
// If value is larger than the size of a pointer then a truncation is done.
// If value is smaller than the size of a pointer then a zero extension is done.
// If they are the same size, nothing is done (no-op cast).
void StackBuilder::visitIntToPtrInst(IntToPtrInst& I) {
  DEBUG(dbgs() << "StackBuilder: visiting IntToPtrInst " << I << "\n");

  for (auto& op : I.operands())
    visitValue(*op.get());

  auto int_size = mod_.getDataLayout()->getTypeSizeInBits(I.getSrcTy());
  auto ptr_size = mod_.getDataLayout()->getTypeSizeInBits(I.getDestTy());

  if (int_size > ptr_size) {
    Value* bv = builder_.CreateZExt(&I, builder_.getIntNTy(ptr_size));
    Value* nbits = builder_.getIntN(ptr_size, int_size);

    ArrayRef<Value*> Args({bv, nbits});
    builder_.CreateCall(fmap_[PushTrunc], Args);;
  } else if (int_size < ptr_size) {
    Value* bv = builder_.CreateZExt(&I, builder_.getIntNTy(ptr_size));
    Value* nbits = builder_.getIntN(ptr_size, int_size);

    ArrayRef<Value*> Args({bv, nbits});
    builder_.CreateCall(fmap_[PushZExt], Args);
  } else {

  }

  DEBUG(dbgs() << "StackBuilder: visiting IntToPtrInst " << I << "\n");
}

void StackBuilder::visitBitCastInst(llvm::BitCastInst &I) {
  DEBUG(dbgs() << "StackBuilder: visiting BitCastInst " << I << "\n");

//  for (auto& op : I.operands())
//    visitValue(*op.get());
//
//  unsigned size = mod_.getDataLayout()->getPointerSizeInBits();
//  unsigned actural_size = mod_.getDataLayout()->getTypeSizeInBits(I.getType());
//
//  Value* bv = builder_.CreateZExt(&I, builder_.getIntNTy(size));
//  Value* nbits = builder_.getIntN(size, actural_size);
//  Value* ptr_nbits = builder_.getIntN(size, size);
//
//  ArrayRef<Value*> Args({bv, nbits, ptr_nbits});
//  builder_.CreateCall(fmap_[PushPtrToInt], Args);

  DEBUG(dbgs() << "StackBuilder: visited BitCastInst\n");
}


void StackBuilder::visitArgument(Argument& I) {
  DEBUG(dbgs() << "StackBuilder: visiting argument " << I << "\n");
  DEBUG(dbgs() << "StackBuilder: visited argument\n");
}

void StackBuilder::visitValue(Value& V) {
  if (isa<Instruction>(V)) {
    visit(cast<Instruction>(V));
  } else if (isa<Argument>(V)) {
    visitArgument(cast<Argument>(V));
  } else if (isa<Constant>(V)) {
    visitConstant(cast<Constant>(V));
  } else {
    DEBUG(dbgs() << "V = " << V << "\n");
    llvm_unreachable_internal("Unknown type of value operand!");
  }
}

void StackBuilder::visitInstruction(Instruction& I) {
  dbgs() << "StackBuilder: Instruction not handled: " << I << "\n";
  llvm_unreachable("StackBuilder: Instruction not handled");
}



}  // namespace llsplat


