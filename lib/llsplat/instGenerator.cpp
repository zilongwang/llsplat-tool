/*******************************************************************************
 * This file is distributed under the MIT License. See LICENSE.TXT for details.
 *******************************************************************************/

#define DEBUG_TYPE "frontend-instGenerator"
#include "instGenerator.h"
#include "stackBuilder.h"
#include "pointerValueExtractor.h"
#include "../../include/llsplat/cloneInstVisitor.h"
#include "irUtils.h"
#include "llvm/Support/Debug.h"
#include "llvm/IR/Dominators.h"
#include "llvm/IR/Verifier.h"
#include "llvm/Analysis/CFG.h"
#include "llvm/ADT/SmallString.h"
#include "llvm/Support/FileSystem.h"
#include <queue>
#include <string>
#include <algorithm>
#include <libgen.h>
using namespace llvm;

extern cl::opt<bool>Enable_BMC;

namespace llsplat {

char InstGenerator::ID = 0;
RegisterPass<InstGenerator> IG("InstGenerator", "InstGenerator", true, false);

void InstGenerator::init_(Module& m) {
  mod_ = &m;
  context_= &m.getContext();
  ptr_size_ = m.getDataLayout()->getPointerSizeInBits();
  total_br_ = 0;
  makePushVar_();
  makePushC_();
  makePushBop_();
  makePushICmp_();
  makePushZExt_();
  makePushSExt_();
  makePushTrunc_();
  makePushPtrToInt_();
  makePushSelect_();
  makePushPhi_();
  makeUpdatePhiMap_();
  makeUpdateSymbolicStore_();
  makeAddPathConstraint_();
  makeAddPathConstraintSwitch_();
  makeSolve_();
  makeBMC_();
  makeUpdateBranchCoverage_();
  makeLocalAddrRange_();
}

bool InstGenerator::runOnModule(llvm::Module& m)  {
  init_(m);



  for (auto& Fn: m)
    visitFunction(Fn);

  for (auto& Fn : m) {
    if (Fn.isDeclaration() || Fn.empty() || Fn.getEntryBlock().empty())
      continue;
    auto& EntryBB = Fn.getEntryBlock();

    // Find the last alloca in the entry block. I assume that all allcoas in a function
    // are in the entry block, and they SHOULD be.

    if (!hasAllocaInst_(EntryBB))
      continue;

    assert(isa<AllocaInst>(EntryBB.begin()));
    IRBuilder<> builder(EntryBB.begin());
    AllocaInst* first_alloca = builder.CreateAlloca(builder.getInt32Ty(), nullptr, "llsplat_dummy_start");
    first_alloca->setAlignment(4);


    Instruction* first_non_alloca = nullptr;
    for (BasicBlock::iterator I = EntryBB.begin(), E = EntryBB.end(); I != E; ++I)
      if (!isa<AllocaInst>(*I)) {
        first_non_alloca = &*I;
        break;
      }
    assert(first_non_alloca != nullptr);
    builder.SetInsertPoint(first_non_alloca);
    AllocaInst* last_alloca = builder.CreateAlloca(builder.getInt32Ty(), nullptr, "llsplat_dummy_end");
    last_alloca->setAlignment(4);

    Value* address_begin = builder.CreatePtrToInt(first_alloca, builder.getIntNTy(ptr_size_));
    Value* address_end = builder.CreatePtrToInt(last_alloca, builder.getIntNTy(ptr_size_));

    ArrayRef<Value*> Args({address_begin, address_end});
    builder.CreateCall(fmap_[PushLocalAddrRange], Args);

    // Instrument PushStackFrame at the very beginning of the entry block
    for (auto& BB : Fn) {
      auto TermI = BB.getTerminator();
      if (isa<ReturnInst>(TermI)) {
        IRBuilder<> builder(TermI);
        builder.CreateCall(fmap_[PopLocalAddrRange]);
      }
    }
  }



  // Initialize scalar global variables.
//  for (auto& G : m.globals()) {
//    if (!G.isConstant() && G.hasInitializer()) {
//      if (G.getType()->getContainedType(0)->isSingleValueType()) {
//        auto EntryFn = m.getFunction("llsplatInstrumented");
//        assert(EntryFn != NULL && "Frontend cannot find the entry function llsplatInstrumented.");
//        IRBuilder<> builder(EntryFn->getEntryBlock().begin());
//
//        Value* address = builder.CreatePtrToInt(&G, builder.getIntNTy(ptr_size_));
//
//        unsigned actural_size = m.getDataLayout()->getTypeSizeInBits(G.getType()->getContainedType(0));
//        assert(ptr_size_ >= actural_size && "instGenerator: SaveGlovalVar: bad bit truncation");
//
//        Value* bv = isa<PointerType>(G.getType()->getContainedType(0)) ?
//            builder.CreatePtrToInt(G.getInitializer(), builder.getIntNTy(ptr_size_)) :
//            builder.CreateZExt(G.getInitializer(), builder.getIntNTy(ptr_size_));
//
//        Value* nbits = builder.getIntN(ptr_size_, actural_size);
//        ArrayRef<Value*> Args({address, bv, nbits});
//        builder.CreateCall(fmap_[SaveGlovalVar], Args);
//      } else {
////        if (!isa<ConstantAggregateZero>(G.getInitializer())) {
//        if (!G.getInitializer()->isZeroValue()) {
//          dbgs() << "G = " << G.getName() << "\n";
//          dbgs() << "v = " << *G.getInitializer() << "\n";
//          llvm_unreachable("For the moment, frontend does not support re-initialize non-scalar global variables");
//        }
//      }
//    }
//  }

  if (verifyModule(m, &llvm::errs())) {
    if (llvm::errs().has_colors()) llvm::errs().changeColor(llvm::raw_ostream::RED);
    llvm::errs() << "Error: In the InstGenerator pass, module verification failed.\n";
    if (llvm::errs().has_colors()) llvm::errs().resetColor();
    abort();
  }

  // Initialize scalar global variables.
//  for (auto& G : m.globals()) {
//    if (!G.isConstant() && G.hasInitializer()) {
//      if (G.getType()->getContainedType(0)->isSingleValueType()) {
//        auto EntryFn = m.getFunction("llsplatInstrumented");
//        assert(EntryFn != NULL && "Frontend cannot find the entry function llsplatInstrumented.");
//        IRBuilder<> builder(EntryFn->getEntryBlock().begin());
//
//        Value* address = builder.CreatePtrToInt(&G, builder.getIntNTy(ptr_size_));
//
//        unsigned actural_size = m.getDataLayout()->getTypeSizeInBits(G.getType()->getContainedType(0));
//        assert(ptr_size_ >= actural_size && "instGenerator: SaveGlovalVar: bad bit truncation");
//
//        Value* bv = isa<PointerType>(G.getType()->getContainedType(0)) ?
//            builder.CreatePtrToInt(G.getInitializer(), builder.getIntNTy(ptr_size_)) :
//            builder.CreateZExt(G.getInitializer(), builder.getIntNTy(ptr_size_));
//
//        Value* nbits = builder.getIntN(ptr_size_, actural_size);
//        ArrayRef<Value*> Args({address, bv, nbits});
//        builder.CreateCall(fmap_[SaveGlovalVar], Args);
//      } else {
//        dbgs() << "G = " << G.getName() << "\n";
//        dbgs() << "v = " << *G.getInitializer() << "\n";
//        llvm_unreachable("For the moment, frontend does not support re-initialize non-scalar global variables");
//      }
//    }
//  }

  // Count the total number of branches.
  for (auto& Fn: m)
    for (auto& BB: Fn)
      for (auto& I: BB)
        if (llvm::CallInst* CI = dyn_cast<llvm::CallInst>(&I))
          if (CI->getCalledFunction() != NULL && CI->getCalledFunction()->getName() == "llsplatUpdateBranchCoverage")
            total_br_ = total_br_ + 2;

  std::string base_name = m.getName().drop_back(strlen(".ll"));
  base_name = basename(const_cast<char*>(base_name.c_str()));
  std::error_code EC;
  StringRef covfilename{"coverage_"+base_name+".txt"};
  llvm::raw_fd_ostream output(covfilename, EC, llvm::sys::fs::F_None);
  output << total_br_;
  output.close();

  return false;
}

void InstGenerator::visitInstruction(Instruction& I) {
  DEBUG(dbgs() << "instGenerator: Instruction not handled: " << I << "\n");
}


/******************************************************************************/
/*                 TERMINATOR                  INSTRUCTIONS                   */
/******************************************************************************/

void InstGenerator::visitReturnInst(ReturnInst& I) {
  DEBUG(dbgs() << "instGenerator: visiting return" << I << "\n");

  IRBuilder<> builder(&I);


  // Process return statement in llsplatInstrumented()
  if (I.getParent()->getParent()->getName() == "llsplatInstrumented") {
    builder.CreateCall(fmap_[Solve]);
    return;
  }

  // Do not process functions that return void
  Value* ret = I.getReturnValue();
  if (!ret)
    return;

  // Process the rest functions.
  StackBuilder instrV(*mod_, *context_, builder, fmap_);
  instrV.visitValue(*ret);

  DEBUG(dbgs() << "instGenerator: visited return\n");
}

void InstGenerator::visitBranchInst(BranchInst& I) {
  DEBUG(dbgs() << "instGenerator: visiting branch" << I << "\n");

  if (I.isUnconditional())
    return;

  if (isa<ConstantInt>(I.getCondition()))
    return;

  // BMC instrumentation
  tryBMC_(I.getParent());


  // Instrumentation for pure concolic

  IRBuilder<> builder(&I);

  Value* Cond = I.getCondition();

  // Instrument the coverage function.
  coverageBranchInst(I);

  if (isa<CallInst>(Cond))
    return;

  if (!(isa<ICmpInst>(Cond) || isa<TruncInst>(Cond) || isa<PHINode>(Cond))) {
    llvm::errs() << "Cond = " << *Cond << "\n";
    assert(0 && "instGenerator: cmp is not a ICmpInst");
  }
  StackBuilder instrV(*mod_, *context_, builder, fmap_);
  instrV.visitValue(*Cond);

  // create FnName
  auto const_ptr_FnName_addr = createConstString(I.getParent()->getParent()->getName(), mod_);
  // create BBName
  auto const_ptr_BBName_addr = createConstString(I.getParent()->getName(), mod_);

  ArrayRef<Value*> Args({Cond, const_ptr_FnName_addr, const_ptr_BBName_addr});

  builder.CreateCall(fmap_[AddPathConstraint], Args);

  DEBUG(dbgs() << "instGenerator: visited branch\n");
}

void InstGenerator::visitSwitchInst(SwitchInst& I) {
  DEBUG(dbgs() << "instGenerator: visiting SwitchInst " << I << "\n");
  // BMC instrumentation
  tryBMC_(I.getParent());

  // Instrumentation for pure concolic
  IRBuilder<> builder(&I);

  int numOfCases = 0;
  for (auto Case : I.cases()) {
    DEBUG(errs() << "------ Case = " << *Case.getCaseValue() << "\n";);
    StackBuilder instrV(*mod_, *context_, builder, fmap_);
    instrV.visitValue(*Case.getCaseValue());
    ++numOfCases;
  }

  auto cond = I.getCondition();
  StackBuilder instrV(*mod_, *context_, builder, fmap_);
  instrV.visitValue(*cond);

  // create FnName
  auto const_ptr_FnName_addr = createConstString(I.getParent()->getParent()->getName(), mod_);
  // create BBName
  auto const_ptr_BBName_addr = createConstString(I.getParent()->getName(), mod_);

  ArrayRef<Value*> Args({builder.getInt64(numOfCases), const_ptr_FnName_addr, const_ptr_BBName_addr});
  builder.CreateCall(fmap_[AddPathConstraintSwitch], Args);

  // Instrument the coverage function.
  coverageSwitchInst(I);
  DEBUG(dbgs() << "instGenerator: visited SwitchInst\n");
}

void InstGenerator::visitPHINode(llvm::PHINode& I) {
  DEBUG(dbgs() << "instGenerator: visiting PHINode " << I << "\n");

  if (!I.getType()->isIntegerTy(1))
    return;

  int cnt = 0;
  for (auto BB = I.block_begin(), E = I.block_end(); BB != E; ++BB, ++cnt) {
    auto Op = I.getOperand(cnt);
    if (isa<Constant>(Op))
      continue;
    if (ICmpInst* II = dyn_cast<ICmpInst>(Op)) {
      auto TermI = II->getParent()->getTerminator();
      if (BranchInst* BI = dyn_cast<BranchInst>(TermI)) {
        if (!BI->isUnconditional())
          continue;

        IRBuilder<> builder(BI);
        StackBuilder instrV(*mod_, *context_, builder, fmap_);
        instrV.visitValue(*II);

        // create FnName
        auto const_ptr_FnName_addr = createConstString(I.getParent()->getParent()->getName(), mod_);
        // create BBName
        auto const_ptr_BBName_addr = createConstString(I.getParent()->getName(), mod_);
        ArrayRef<Value*> Args({II, const_ptr_FnName_addr, const_ptr_BBName_addr});
        builder.CreateCall(fmap_[AddPathConstraint], Args);


      } else
        continue;
    }
  }
  // Instrument the coverage function.
  coveragePHINode(I);
  DEBUG(dbgs() << "instGenerator: visited PHINode\n");
}

void InstGenerator::visitSelectInst(llvm::SelectInst& I) {
  DEBUG(dbgs() << "instGenerator: visiting SelectInst " << I << "\n");

  // Instrument the coverage function.
  coverageSelectInst(I);
  DEBUG(dbgs() << "instGenerator: visited SelectInst " << I << "\n");
}



/******************************************************************************/
/*     MEMORY       ACCESS        AND       ADDRESSING       OPERATIONS       */
/******************************************************************************/

void InstGenerator::visitStoreInst(StoreInst& I) {
  DEBUG(dbgs() << "instGenerator: visiting store " << I << "\n");
  IRBuilder<> builder(&I);
  auto V = I.getValueOperand();

  // If V is an external function, then we don't handle it.
//  if (CallInst* CI = dyn_cast<CallInst>(V)) {
//    if (CI->getCalledFunction() == NULL ||
//        CI->getCalledFunction()->isDeclaration())
//    return;
//  }

  // Instrument function calls to build symbolic stack.
  StackBuilder instrV(*mod_, *context_, builder, fmap_);
  instrV.visitValue(*V);

  auto address = I.getPointerOperand();
  auto inst = builder.CreatePtrToInt(address, builder.getIntNTy(ptr_size_));
  ArrayRef<Value*> Args({inst});

  builder.CreateCall(fmap_[UpdateSymbolicStore], Args);
  DEBUG(dbgs() << "instGenerator: visited store\n");
}

/******************************************************************************/
/*                 CONVERSION                    OPERATIONS                   */
/******************************************************************************/

/******************************************************************************/
/*                   OTHER                     OPERATIONS                     */
/******************************************************************************/

void InstGenerator::visitCmpInst(CmpInst& I) {

}

void InstGenerator::visitCallInst(CallInst& I) {
  DEBUG(dbgs() << "instGenerator: visiting call" << I << "\n");

  // Do not handle indirect function calls via function pointers.
  if (I.getCalledFunction() == NULL)
    return;

  // Process return statement in llsplatInstrumented()
  if (I.getCalledFunction()->getName() == "exit") {
    IRBuilder<> builder(&I);
    builder.CreateCall(fmap_[Solve]);
    return;
  }

  // Ignore all functions that do not have a definition in the module.
  if (I.getCalledFunction()->isDeclaration())
    return;

  // Normal part.
  IRBuilder<> builder(&I);
  StackBuilder instrV(*mod_, *context_, builder, fmap_);

  for (auto arg = I.arg_operands().end(), E = I.arg_operands().begin(); arg != E; ) {
    --arg;
    instrV.visitValue(*arg->get());
  }

  DEBUG(dbgs() << "instGenerator: visited call\n");
}



/******************************************************************************/
/*                  Visit                        FUNCTIONS                    */
/******************************************************************************/
void InstGenerator::visitFunction(Function& Fn) {
  if (Fn.isDeclaration() || Fn.empty() || Fn.getEntryBlock().empty())
    return;

  DEBUG(dbgs() << "=================================================\n");
  DEBUG(dbgs() << "Analyzing function: " << Fn.getName() << "\n");

  visit(Fn.begin(), Fn.end());
}

/**
 * Private functions.
 */

void InstGenerator::makeUpdateSymbolicStore_() {
  Type* address_t = Type::getIntNTy(*context_, ptr_size_);
  ArrayRef<Type*> Args_t({address_t});
  FunctionType *FT = FunctionType::get(Type::getVoidTy(*context_), Args_t, false);
  fmap_[UpdateSymbolicStore] = Function::Create(FT, Function::ExternalLinkage, "llsplatUpdateSymbolicStore", mod_);
}

void InstGenerator::makePushVar_() {
  Type* address_t = Type::getIntNTy(*context_, ptr_size_);
  ArrayRef<Type*> Args_t({address_t, address_t, address_t});
  FunctionType *FT = FunctionType::get(Type::getVoidTy(*context_), Args_t, false);
  fmap_[PushVar] = Function::Create(FT, Function::ExternalLinkage, "llsplatPushVar", mod_);
}

void InstGenerator::makePushC_() {
  Type* address_t = Type::getIntNTy(*context_, ptr_size_);
  ArrayRef<Type*> Args_t({address_t, address_t});
  FunctionType *FT = FunctionType::get(Type::getVoidTy(*context_), Args_t, false);
  fmap_[PushC] = Function::Create(FT, Function::ExternalLinkage, "llsplatPushC", mod_);
}

void InstGenerator::makePushBop_() {
  Type* address_t = Type::getIntNTy(*context_, ptr_size_);
  ArrayRef<Type*> Args_t({address_t});
  FunctionType *FT = FunctionType::get(Type::getVoidTy(*context_), Args_t, false);
  fmap_[PushBop] = Function::Create(FT, Function::ExternalLinkage, "llsplatPushBop", mod_);
}

void InstGenerator::makePushSelect_() {
  FunctionType *FT = FunctionType::get(Type::getVoidTy(*context_), false);
  fmap_[PushSelect] = Function::Create(FT, Function::ExternalLinkage, "llsplatPushSelect", mod_);
}

void InstGenerator::makePushPhi_() {
  Type* address_t = Type::getIntNTy(*context_, ptr_size_);
  ArrayRef<Type*> Args_t({address_t});
  FunctionType *FT = FunctionType::get(Type::getVoidTy(*context_), Args_t, false);
  fmap_[PushPhi] = Function::Create(FT, Function::ExternalLinkage, "llsplatPushPhi", mod_);
}

void InstGenerator::makeUpdatePhiMap_() {
  Type* address_t = Type::getIntNTy(*context_, ptr_size_);
  ArrayRef<Type*> Args_t({address_t});
  FunctionType *FT = FunctionType::get(Type::getVoidTy(*context_), Args_t, false);
  fmap_[UpdatePhiMap] = Function::Create(FT, Function::ExternalLinkage, "llsplatUpdatePhiMap", mod_);
}

void InstGenerator::makeAddPathConstraint_() {
  Type* address_t = Type::getInt1Ty(*context_);
  PointerType* PointerTy = PointerType::get(IntegerType::get(*context_, 8), 0);
  ArrayRef<Type*> Args_t({address_t, PointerTy, PointerTy});
  FunctionType *FT = FunctionType::get(Type::getVoidTy(*context_), Args_t, false);
  fmap_[AddPathConstraint] = Function::Create(FT, Function::ExternalLinkage, "llsplatAddPathConstraint", mod_);
}

void InstGenerator::makeAddPathConstraintSwitch_() {
  Type* address_t = Type::getInt64Ty(*context_);
  PointerType* PointerTy = PointerType::get(IntegerType::get(*context_, 8), 0);
  ArrayRef<Type*> Args_t({address_t, PointerTy, PointerTy});
  FunctionType *FT = FunctionType::get(Type::getVoidTy(*context_), Args_t, false);
  fmap_[AddPathConstraintSwitch] =
      Function::Create(FT, Function::ExternalLinkage, "llsplatAddPathConstraintSwitch", mod_);
}


void InstGenerator::makePushICmp_() {
  Type* address_t = Type::getIntNTy(*context_, ptr_size_);
  ArrayRef<Type*> Args_t({address_t, address_t, address_t});
  FunctionType *FT = FunctionType::get(Type::getVoidTy(*context_), Args_t, false);
  fmap_[PushICmp] = Function::Create(FT, Function::ExternalLinkage, "llsplatPushICmp", mod_);
}

void InstGenerator::makePushSExt_() {
  Type* address_t = Type::getIntNTy(*context_, ptr_size_);
  ArrayRef<Type*> Args_t({address_t, address_t});
  FunctionType *FT = FunctionType::get(Type::getVoidTy(*context_), Args_t, false);
  fmap_[PushSExt] = Function::Create(FT, Function::ExternalLinkage, "llsplatPushSExt", mod_);
}


void InstGenerator::makePushZExt_() {
  Type* address_t = Type::getIntNTy(*context_, ptr_size_);
  ArrayRef<Type*> Args_t({address_t, address_t});
  FunctionType *FT = FunctionType::get(Type::getVoidTy(*context_), Args_t, false);
  fmap_[PushZExt] = Function::Create(FT, Function::ExternalLinkage, "llsplatPushZExt", mod_);
}

void InstGenerator::makePushTrunc_() {
  Type* address_t = Type::getIntNTy(*context_, ptr_size_);
  ArrayRef<Type*> Args_t({address_t, address_t});
  FunctionType *FT = FunctionType::get(Type::getVoidTy(*context_), Args_t, false);
  fmap_[PushTrunc] = Function::Create(FT, Function::ExternalLinkage, "llsplatPushTrunc", mod_);
}

void InstGenerator::makePushPtrToInt_() {
  Type* address_t = Type::getIntNTy(*context_, ptr_size_);
  ArrayRef<Type*> Args_t({address_t, address_t, address_t});
  FunctionType *FT = FunctionType::get(Type::getVoidTy(*context_), Args_t, false);
  fmap_[PushPtrToInt] = Function::Create(FT, Function::ExternalLinkage, "llsplatPushPtrToInt", mod_);
}

void InstGenerator::makeSolve_() {
  FunctionType *FT = FunctionType::get(Type::getVoidTy(*context_), false);
  fmap_[Solve] = Function::Create(FT, Function::ExternalLinkage, "llsplatSolve", mod_);
}

void InstGenerator::makeBMC_() {
  PointerType* PointerTy = PointerType::get(IntegerType::get(*context_, 8), 0);
  ArrayRef<Type*> Args_t({PointerTy, PointerTy, PointerTy});
  FunctionType *FT = FunctionType::get(Type::getVoidTy(*context_), Args_t, false);
  fmap_[StartBMC] = Function::Create(FT, Function::ExternalLinkage, "llsplatStartBMC", mod_);
  fmap_[EndBMC] = Function::Create(FT, Function::ExternalLinkage, "llsplatEndBMC", mod_);

  Type* index_t = Type::getInt32Ty(*context_);
  Type* address_t = Type::getIntNTy(*context_, ptr_size_);
  ArrayRef<Type*> Args2_t({index_t, address_t, address_t, address_t});
  FunctionType *FT2 = FunctionType::get(Type::getVoidTy(*context_), Args2_t, false);
  fmap_[SaveVarInfo] = Function::Create(FT2, Function::ExternalLinkage, "llsplatSaveVarInfo", mod_);

}

void InstGenerator::makeUpdateBranchCoverage_() {
  PointerType* PointerTy = PointerType::get(IntegerType::get(*context_, 8), 0);
  Type* numOfcases = Type::getInt64Ty(*context_);
  Type* curr_case = Type::getInt64Ty(*context_);
  Type* is_covered_curr_case = Type::getInt1Ty(*context_);
  ArrayRef<Type*> Args_t({PointerTy, PointerTy, numOfcases, curr_case, is_covered_curr_case});
  FunctionType *FT = FunctionType::get(Type::getVoidTy(*context_), Args_t, false);
  fmap_[UpdateBranchCoverage] = Function::Create(FT, Function::ExternalLinkage, "llsplatUpdateBranchCoverage", mod_);
}

void InstGenerator::makeLocalAddrRange_() {
    Type* address_t = Type::getIntNTy(*context_, ptr_size_);
    ArrayRef<Type*> Args_t({address_t, address_t});
    FunctionType *FT = FunctionType::get(Type::getVoidTy(*context_), Args_t, false);
    fmap_[PushLocalAddrRange] = Function::Create(FT, Function::ExternalLinkage, "llsplatPushLocalAddrRange", mod_);

    FunctionType *FT2 = FunctionType::get(Type::getVoidTy(*context_), false);
    fmap_[PopLocalAddrRange] = Function::Create(FT2, Function::ExternalLinkage, "llsplatPopLocalAddrRange", mod_);
}


void InstGenerator::instrumentBMC_(Function* fn, BasicBlock* Dominator,
  SmallVector<llvm::BasicBlock *, 16>& EDom, std::set<BasicBlock*>& Destinations) {

  Value* cond = nullptr;
  if (BranchInst* BI = dyn_cast<BranchInst>(Dominator->getTerminator())) {
    cond = BI->getCondition();
  } else if (SwitchInst* SI = dyn_cast<SwitchInst>(Dominator->getTerminator())) {
    cond = SI->getCondition();
  } else {
    llvm::llvm_unreachable_internal("Dominator's terminateInst must be either branchInst or switchInst.");
  }

  IRBuilder<> builder(Dominator->getTerminator());

  // Instrument llsplatSaveVarInfo().

  TopologicalSort TS(Dominator, EDom);
  SmallVector<llvm::BasicBlock *, 16> SortedEDom = TS.sort();
//  dbgs() << "dom = " << Dominator->getName() <<"\n";
//  for (auto& e : SortedEDom)
//    dbgs() << "e = " << e->getName() << "\n";
  PointerValueExtractor PVEtor(SortedEDom);
  auto valueVec = PVEtor.start();


  for (auto i = 0; i < valueVec.size(); ++i) {
    CloneInstVisitor cloner(*mod_, builder);
    cloner.visitValue(*valueVec[i]);
    auto PtrOp = cloner.getLastClonedInst();

    auto idx = builder.getInt32(i);
    auto address = builder.CreatePtrToInt(PtrOp, builder.getIntNTy(ptr_size_));
    auto loadInst = builder.CreateLoad(PtrOp);
    auto bv = isa<PointerType>(loadInst->getType()) ?
        builder.CreatePtrToInt(loadInst, builder.getIntNTy(ptr_size_)) :
        builder.CreateZExt(loadInst, builder.getIntNTy(ptr_size_));
    auto actural_size = mod_->getDataLayout()->getTypeSizeInBits(loadInst->getType());
    auto nbits = builder.getIntN(ptr_size_, actural_size);

    ArrayRef<Value*> Args({idx, address, bv, nbits});
    builder.CreateCall(fmap_[SaveVarInfo], Args);

  }

  // Instrument llsplatStartBMC().
  StackBuilder instrV(*mod_, *context_, builder, fmap_);
  instrV.visitValue(*cond);

  StringRef currFn {fn->getName()};
  StringRef currBB {Dominator->getName() };
  std::string Dest_names = "";
  for (auto& BB : Destinations) {
    Dest_names = Dest_names + BB->getName().data() + " ";
  }
  Dest_names = Dest_names.substr(0, Dest_names.size()-1);

  // create fn.
  auto const_ptr_FnName_addr = createConstString(currFn, mod_);
  // create BB
  auto const_ptr_BBName_addr = createConstString(currBB, mod_);
  // create Destinations
  auto const_ptr_DestsName_addr = createConstString(Dest_names, mod_);

  ArrayRef<Value*> Args({const_ptr_FnName_addr, const_ptr_BBName_addr, const_ptr_DestsName_addr});
  builder.CreateCall(fmap_[StartBMC], Args);

  // Instrument llsplatEndBMC().
  for (auto& Dest : Destinations) {
    // create Destination
    StringRef dest_name = Dest->getName();
    auto const_ptr_DestName_addr = createConstString(dest_name, mod_);

    ArrayRef<Value*> Args({const_ptr_FnName_addr, const_ptr_BBName_addr, const_ptr_DestName_addr});

    // Compute the insertion points.
    IRBuilder<> builder(&*Dest->begin());
    builder.CreateCall(fmap_[EndBMC], Args);
  }
}

void InstGenerator::tryBMC_(BasicBlock* BB) {
  // Compute dominatees and dominators.
  auto Fn = BB->getParent();

  LoopInfo &li = getAnalysis<LoopInfo>(*Fn);
  //if node have no branches go find the next available node from successors
  if (li.isLoopHeader(BB))
    return;


  if (std::any_of(succ_begin(BB), succ_end(BB), hasFnCall))
    return;

  if (std::any_of(succ_begin(BB), succ_end(BB), hasNoSuccessor))
    return;

  // Compute effective dominance set.
  DominatorTreeAnalysis DTA;
  DominatorTree dt = std::move(DTA.run(*BB->getParent()));

//  SmallVector<BasicBlock *, 16> DominatedBBs;
  SmallVector<BasicBlock*, 16> EDominatedBBs;
  for (succ_iterator SI = succ_begin(BB), E = succ_end(BB); SI != E; ++SI) {
    SmallVector<BasicBlock *, 16> acc;
    dt.getDescendants(*SI, acc);

    SmallVector<BasicBlock*, 16>& edomSuccBB = getEffecitveDominance(acc);
    if (edomSuccBB.empty())
      return;

    EDominatedBBs.append(edomSuccBB.begin(), edomSuccBB.end());
  }

//  SmallVector<BasicBlock*, 16>& EDominatedBBs = getEffecitveDominance(DominatedBBs);

//  DEBUG(
//      dbgs().changeColor(raw_ostream::RED);
//  dbgs() << "In function " << BB->getParent()->getName() << ", " << BB->getName() << " dominates: \n";
//  for (auto& BB : EDominatedBBs)
//    dbgs() << BB->getName() << " ";
//  dbgs() << "hasPointerAssignment(EDominatedBBs) : " << hasPointerAssignment(EDominatedBBs) << "\n";
//  dbgs().resetColor() << "\n";);

  if (hasPointerAssignment(EDominatedBBs)) {
    outs().changeColor(raw_ostream::BLUE);
    outs() << "In function " << Fn->getName() << ", "
        << "Basic block " << BB->getName() << "'s EDominatedBBs has non-constant pointer.";
    outs().resetColor() << "\n";
  }

//  if (!isAcyclic(*Fn, EDominatedBBs) || hasPointerAssignment(EDominatedBBs))
  if (hasPointerAssignment(EDominatedBBs))
    return;

  // Compute destinations.
  std::set<BasicBlock*> Destinations;
  for (auto& BB : EDominatedBBs) {
    for (succ_iterator SI = succ_begin(BB), E = succ_end(BB); SI != E; ++SI)
      if (std::find(EDominatedBBs.begin(), EDominatedBBs.end(), *SI) == EDominatedBBs.end())
        Destinations.insert(*SI);
  }

  if (std::any_of(EDominatedBBs.begin(), EDominatedBBs.end(), hasPhiNode))
    return;

  if (std::any_of(Destinations.begin(), Destinations.end(), hasPhiNode))
    return;


//  outs().changeColor(raw_ostream::RED);
//  outs() << "In function " << BB->getParent()->getName() << ", ";
//  outs() << BB->getName() << " effectively and strictly dominates: \n";
//  for (auto& BB : EDominatedBBs)
//    outs() << BB->getName() << " ";
//  outs() << "\n";
//  outs().changeColor(raw_ostream::GREEN) << "Destinations are :\n";
//  for (auto& BB : Destinations)
//    outs() << BB->getName() << " ";
//  outs().resetColor() << "\n";

  // Instrument a BMC-related function at BB.
  // Instrument a BMC-related function at each destination.
  instrumentBMC_(Fn, BB, EDominatedBBs, Destinations);

}

bool InstGenerator::hasReturnUsageWithinBlock_(Instruction& I, BasicBlock& BB) {
  for (auto i = I.user_begin(); i != I.user_end(); ++i) {
    Instruction* inst = cast<Instruction>(*i);
    if (inst->getParent() == &BB && isa<ReturnInst>(*inst)) {
      return true;
    } else if (hasReturnUsageWithinBlock_(*inst, BB))
      return true;
  }
  return false;
}

bool InstGenerator::hasNoCondBranchUsageWithinBlock_(Instruction& I, BasicBlock& BB) {
  for (auto i = I.user_begin(); i != I.user_end(); ++i) {
    Instruction* inst = cast<Instruction>(*i);
    if (inst->getParent() == &BB && isa<BranchInst>(*i) && cast<BranchInst>(*i)->isConditional())
      return false;
    else if (!hasNoCondBranchUsageWithinBlock_(*inst, BB))
      return false;
  }
  return true;
}

void InstGenerator::coverageBranchInst(BranchInst& I) {
  IRBuilder<> builder(&I);
  StringRef currFn{I.getParent()->getParent()->getName()};
  StringRef currBB{I.getParent()->getName()};
  auto FnName = createConstString(currFn, mod_);
  auto BBName = createConstString(currBB, mod_);
  Value* NumOfCases = builder.getInt64(2);
  Value* Cond = I.getCondition();
  Value* CurrCase = builder.CreateZExt(Cond, builder.getInt64Ty());
  Value* IsTaken = builder.getInt1(true);
  ArrayRef<Value*> Args({FnName, BBName, NumOfCases, CurrCase, IsTaken});
  builder.CreateCall(fmap_[UpdateBranchCoverage], Args);
}

void InstGenerator::coverageSwitchInst(SwitchInst& I) {
  IRBuilder<> builder(&I);
  StringRef currFn{I.getParent()->getParent()->getName()};
  StringRef currBB{I.getParent()->getName()};
  auto FnName = createConstString(currFn, mod_);
  auto BBName = createConstString(currBB, mod_);
  auto NumOfCases = builder.getInt64(I.getNumCases()+1);
  // Note that the default case has index 0.
  auto cnt = 1;
  for (auto Case : I.cases()) {
     auto switch_case_compare = builder.CreateICmpEQ(I.getCondition(), Case.getCaseValue());
     auto CurrCase = builder.getInt64(++cnt);
     ArrayRef<Value*> Args({FnName, BBName, NumOfCases, CurrCase, switch_case_compare});
     builder.CreateCall(fmap_[UpdateBranchCoverage], Args);
  }
}

void InstGenerator::coverageSelectInst(SelectInst& I) {
  IRBuilder<> builder(&I);
  StringRef currFn{I.getParent()->getParent()->getName()};
  StringRef currBB{I.getParent()->getName()};
  auto FnName = createConstString(currFn, mod_);
  auto BBName = createConstString(currBB, mod_);
  Value* NumOfCases = builder.getInt64(2);
  Value* Cond = I.getCondition();
  Value* CurrCase = builder.CreateZExt(Cond, builder.getInt64Ty());
  Value* IsTaken = builder.getInt1(true);
  ArrayRef<Value*> Args({FnName, BBName, NumOfCases, CurrCase, IsTaken});
  builder.CreateCall(fmap_[UpdateBranchCoverage], Args);
}

void InstGenerator::coveragePHINode(PHINode& I) {
  if (!I.getType()->isIntegerTy(1))
    return;

  int cnt = 0;
  for (auto BB = I.block_begin(), E = I.block_end(); BB != E; ++BB, ++cnt) {
    auto Op = I.getOperand(cnt);
    if (isa<Constant>(Op))
      continue;

    if (ICmpInst* II = dyn_cast<ICmpInst>(Op)) {
      auto TermI = II->getParent()->getTerminator();
      if (BranchInst* BI = dyn_cast<BranchInst>(TermI)) {
        if (!BI->isUnconditional())
          continue;
        if (hasReturnUsageWithinBlock_(I, *I.getParent()) ||
            hasNoCondBranchUsageWithinBlock_(I, *I.getParent())) {
          IRBuilder<> builder(BI);
          StringRef currFn{I.getParent()->getParent()->getName()};
          StringRef currBB{I.getParent()->getName()};
          auto FnName = createConstString(currFn, mod_);
          auto BBName = createConstString(currBB, mod_);
          auto NumOfCases = builder.getInt64(2);
          auto CurrCase = builder.CreateZExt(II, builder.getInt64Ty());
          Value* IsTaken = builder.getInt1(true);
          ArrayRef<Value*> Args({FnName, BBName, NumOfCases, CurrCase, IsTaken});
          builder.CreateCall(fmap_[UpdateBranchCoverage], Args);
          break;
        }
      }
    }
  }
}

bool InstGenerator::hasAllocaInst_(llvm::BasicBlock& BB) {
  for (auto& I : BB)
    if (isa<AllocaInst>(I))
      return true;

  return false;
}


}  // namespace llsplat

