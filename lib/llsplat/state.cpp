/*******************************************************************************
 * This file is distributed under the MIT License. See LICENSE.TXT for details.
 *******************************************************************************/
#define DEBUG_TYPE "l-state"
#include "llvm/Support/raw_ostream.h"
#include "llvm/Support/Debug.h"
#include "llvm/IR/Dominators.h"
#include "llvm/IR/Instructions.h"
#include "state.h"
#include "pointerValueExtractor.h"
#include "bmcBuilderHelper.h"
#include "irUtils.h"
#include "z3Utils.h"
#include <unistd.h>
#include <random>
#include <cmath>
#include <sstream>
#include <deque>

using std::cout;
using std::endl;
using std::string;
using std::stringstream;
using llvm::BasicBlock;
using llvm::StoreInst;
using llvm::BranchInst;
using llvm::SwitchInst;
using llvm::Function;
using llvm::SmallVector;
using llvm::isa;
using llvm::cast;
using llvm::dyn_cast;
using llvm::CallInst;
using llvm::errs;
using llvm::outs;
using namespace z3;

StateManager* StateManager::_instance = nullptr;
const string StateManager::sym_prefix_ = "sym";

void StateManager::compareCurrentPathWithPrediction(bool current_branch, string FnName, string BBName) {
  auto i = path_constraint_.size() - 1;
  if (i < branch_history_.size()) {
    if (branch_history_[i]->branch != current_branch) {
//      cout << "pc[" << i << "]: " << (path_constraint_[i].first && path_constraint_[i].second).simplify() << "\n";
//      cout << "old_pc[" << i << "]: " << (old_path_constraint_[i].first && old_path_constraint_[i].second).simplify() << "\n";
//      cout << "old_pc's size: " << old_path_constraint_.size() << "\n";
//      cout << "br_hist's size: " << branch_history_.size() << "\n";
      stringstream err_msg("Path predication fails.\n");
      err_msg << "Predict to cover branch " << branch_history_[i]->branch
          << ", but actually visit " << current_branch << "\n";
      throw std::runtime_error(err_msg.str());
    }
    else if (i == branch_history_.size() - 1) {

      branch_history_[i]->done = true;
    }
  } else {
    auto BB = getBasicBlock(FnName, BBName);
    branch_history_.push_back(std::unique_ptr<BranchInfo>(new BranchInfo(current_branch, BB)));
  }
}

void StateManager::compareCurrentPathWithPrediction(index_t numOfCases, index_t current_case,
    vector<z3::expr>& vec, string FnName, string BBName) {
  auto i = path_constraint_.size() - 1;
  if (i < branch_history_.size()) {
    if (branch_history_[i]->to_cover_case != current_case) {
      stringstream err_msg("Path predication fails.\n");
      err_msg << "Predict to cover case" << branch_history_[i]->to_cover_case
          << ", but actually visit case" << current_case << "\n";
      throw std::runtime_error(err_msg.str());
    }
    if (i == branch_history_.size() - 1 && coveredAllOtherCases_(current_case)) {
      branch_history_[i]->done = true;
    }
    branch_history_[i]->switchmap[current_case].first = true;
    for (auto j = 0; j < numOfCases; ++j) {
      branch_history_[i]->switchmap[j].second = vec[j];
    }
  } else {
    auto BB = getBasicBlock(FnName, BBName);
    branch_history_.push_back(std::unique_ptr<BranchInfo>(new BranchInfo(current_case, vec, BB)));
  }
}

//void StateManager::compareCurrentPathWithPrediction(BasicBlock* Dominator, std::vector<BasicBlock*>& Dests) {
//  auto i = path_constraint_.size();
//  if (i < branch_history_.size()) {
//    DEBUG(errs().changeColor(llvm::raw_ostream::RED) << "path_constraints are: \n";
//    cout << "------ branch_history's size = " << branch_history_.size() << "\n";
//    cout << "------ path_constraint's size = " << path_constraint_.size() << "\n";
//    errs().resetColor() << "\n";);
//    if (branch_history_[i]->dominator != Dominator) {
//    	cerr << "i = " << i << "\n";
//    	for (auto& kvp : path_constraint_) {
//    		cerr << "first = " << kvp.first << "\n";
//    		cerr << "second = " << kvp.second << "\n";
//    	}
//    	cerr << "Should cover " << branch_history_[i]->dominator->getName().data() << "\n";
//    	cerr << "but actually cover " << Dominator->getName().data() << "\n";
//    	cerr << "Should cover " << branch_history_[i]->dominator->getParent()->getName().data() << "\n";
//    	cerr << "but actually cover " << Dominator->getParent()->getName().data() << "\n";
//    	throw std::runtime_error("Path predication fails at Dominator");
//    }
//  }
//  currBranchInfo_ = std::unique_ptr<BranchInfo>(new BranchInfo(Dominator, Dests));
//}


bv_t StateManager::getRamdomNumber(width_t nbits) {
  std::random_device rd;
  std::mt19937_64 e2(rd());
  std::uniform_int_distribution<bv_t> dist(std::llround(std::pow(2,61)), std::llround(std::pow(2,62)));

  switch(nbits) {
  case 8: return dist(e2) & 0xFF;
  case 16: return dist(e2) & 0xFFFF;
  case 32: return dist(e2) & static_cast<bv_t>(0xFFFFFFFF);
  case 64: return dist(e2) & 0xFF;
  default: {
    std::cerr << "getRamdomNumber: width = " << nbits << " is not handled";
    assert(0 && "getRamdomNumber: width");
  }
  }
}

llsplat_status_t StateManager::solve() {
  old_path_constraint_.clear();
  old_path_constraint_ = path_constraint_;
  auto j = (int) path_constraint_.size() - 1;
//  int cnt = 0;
//  for(auto kvp = path_constraint_.begin(); kvp != path_constraint_.begin()+j+1; ++kvp) {
//  	cout << "In function " << branch_history_[cnt]->fn_name_ << "'s basic block " << branch_history_[cnt]->bb_name_ <<":\n";
//  	cout << "pc[" << cnt << "]: " << (eq(kvp->second, context_.bool_val(true)) ? kvp->first : (kvp->first && kvp->second)) << "\n";
//  	cnt++;
//  }
  while (j >= 0) {
    if (!branch_history_[j]->done) {
      if (branch_history_[j]->is_bmc) {
        // branch_history_[j] is in the BMC case.
        for (auto& kvp: branch_history_[j]->Dests) {
          auto Dest = kvp.first;
          if (branch_history_[j]->Dests[Dest])
            continue;

          for (auto& ctx : branch_history_[j]->ctxmap[Dest]) {
            // enabling unsat core tracking

            solver s(context_);

            DEBUG(
            for(auto kvp = path_constraint_.begin(); kvp != path_constraint_.begin()+j; ++kvp) {
              cout << (eq(kvp->second, context_.bool_val(true)) ? kvp->first : (kvp->first && kvp->second)) << "\n";
            }
            cout << (path_constraint_[j].first && ctx) << "\n";
            );
            for(auto kvp = path_constraint_.begin(); kvp != path_constraint_.begin()+j; ++kvp) {
              s.add(eq(kvp->second, context_.bool_val(true)) ? kvp->first : (kvp->first && kvp->second));
            }

//            DEBUG(cout << "ctx = " << ctx << "\n");
//            cout << "j = " << j << "\n";
//            dbgs() << "Dest =  " << Dest->getName() << "\n";
//            cout << "path_constraint_[j].first = " << path_constraint_[j].first << "\n";
//            cout << "ctx = " << ctx << "\n";

            s.add(path_constraint_[j].first);
            s.add(ctx);

            switch (s.check()) {
            case sat:
            {
//              int cnt = 0;
//              for(auto kvp = path_constraint_.begin(); kvp != path_constraint_.begin()+j+1; ++kvp) {
//                cout << "In function " << branch_history_[cnt]->fn_name_ << "'s basic block " << branch_history_[cnt]->bb_name_ <<":\n";
//                cout << "pc[" << cnt << "]: " << (eq(kvp->second, context_.bool_val(true)) ? kvp->first : (kvp->first && kvp->second)) << "\n";
//                cnt++;
//              }
              model m = s.get_model();
              DEBUG(cout << "bmc sat\n" << Z3_model_to_string(context_, m) << "\n";
              cout << "j = " << j << "\n");
              branch_history_.erase(branch_history_.begin()+j+1, branch_history_.end());
              branch_history_[j]->to_cover = Dest;
              // I = I'
              for(size_t i = 0; i < m.size(); ++i) {
                func_decl v = m[i];
                assert(v.arity() == 0);
                bv_t ret;
                assert(sizeof(bv_t) == 8);
                if(Z3_get_numeral_uint64(context_, m.get_const_interp(v), &ret) == Z3_TRUE)
                  logical_input_map_[exprToLogicalAddress(v.name().str())] = ret;
                else
                  assert(m.get_const_interp(v).is_bool() && "Type conversion error during the model extraction.");
              }
              return Continue;
            }
            case unsat:
            {
              DEBUG(cout << "bmc unsat\n");
//              cout << "Try to cover dest " << Dest->getName().data() << "\n";
//              cout << "bmc unsat\n";
//              expr_vector core = s.unsat_core();
//              std::cout << core << "\n";
//              std::cout << "size: " << core.size() << "\n";
//              for (unsigned i = 0; i < core.size(); i++) {
//                  std::cout << core[i] << "\n";
//              }
              continue;
            }
            default: {
              DEBUG(cout << "bmc unknown\n");
              continue;
            }
            }
          }
        }
        j--;
      } else if (branch_history_[j]->is_switch) {
        // branch_history_[j] is in the Switch case.
        index_t cnt = 0;
        for (auto kvp = branch_history_[j]->switchmap.begin(), E = branch_history_[j]->switchmap.end();
            kvp != E; ++kvp, ++cnt) {
          if (kvp->first)
            continue;

          solver s(context_);
//          DEBUG(
//          for(auto kvp = path_constraint_.begin(); kvp != path_constraint_.begin()+j; ++kvp) {
//            cout << (eq(kvp->second, context_.bool_val(true)) ? kvp->first : (kvp->first && kvp->second)) << "\n";
//          }
//          cout << kvp->second << "\n";
//          );
          for(auto kvp = path_constraint_.begin(); kvp != path_constraint_.begin()+j; ++kvp) {
            s.add(eq(kvp->second, context_.bool_val(true)) ? kvp->first : (kvp->first && kvp->second));
          }

          s.add(kvp->second);

          switch (s.check()) {
          case sat:
          {
            model m = s.get_model();
            DEBUG(cout << "switch sat\n" << Z3_model_to_string(context_, m) << "\n";
            cout << "j = " << j << "\n");
            branch_history_.erase(branch_history_.begin()+j+1, branch_history_.end());
            branch_history_[j]->to_cover_case = cnt;
            // I = I'
            for(size_t i = 0; i < m.size(); ++i) {
              func_decl v = m[i];
              assert(v.arity() == 0);
              bv_t ret;
              assert(sizeof(bv_t) == 8);
              if(Z3_get_numeral_uint64(context_, m.get_const_interp(v), &ret) == Z3_TRUE)
                logical_input_map_[exprToLogicalAddress(v.name().str())] = ret;
              else
                assert(m.get_const_interp(v).is_bool() && "Type conversion error during the model extraction.");
            }
//            DEBUG(
//                cout << "Inputs for the next run are:\n";
//            for(auto& kvp : logical_input_map_)
//              cout << "sym" << kvp.first << " -> " << kvp.second << endl;
//            );

            return Continue;
          }
          case unsat:
          {
            DEBUG(cout << "switch unsat\n");
            continue;
          }
          default: {
            DEBUG(cout << "switch unknown\n");
            continue;
          }
          }
        }
        j--;
      } else {
        // branch_history_[j] is in the normal case.
//        DEBUG(
//            cout << "path constraints before negated are\n";
//        for(auto kvp = path_constraint_.begin(); kvp != path_constraint_.begin()+j+1; ++kvp) {
//          cout << (eq(kvp->second, context_.bool_val(true)) ? kvp->first : (kvp->first && kvp->second)) << "\n";
//        }
//        );

        branch_history_[j]->branch = !branch_history_[j]->branch;
        negateLast(j);

//        DEBUG(cout << "path constraints after negated are\n");
        solver s(context_);
        for(auto kvp = path_constraint_.begin(); kvp != path_constraint_.begin()+j+1; ++kvp) {
          s.add(eq(kvp->second, context_.bool_val(true)) ? kvp->first : kvp->first && kvp->second);
        }
        switch (s.check()) {
        case sat:
        {
          model m = s.get_model();
          DEBUG(cout << "normal sat\n" << Z3_model_to_string(context_, m) << "\n";
              cout << "j = " << j << "\n");
//          int cnt = 0;
//          for(auto kvp = path_constraint_.begin(); kvp != path_constraint_.begin()+j+1; ++kvp) {
//            cout << "pc[" << cnt << "]: " << (eq(kvp->second, context_.bool_val(true)) ? kvp->first : (kvp->first && kvp->second)) << "\n";
//            cnt++;
//          }
//          cout << "normal sat\n" << "j = " << j << "\n";
//          cout << "model is " << m << "\n";
          branch_history_.erase(branch_history_.begin()+j+1, branch_history_.end());

          // I = I'
          for(size_t i = 0; i < m.size(); ++i) {
            func_decl v = m[i];
            assert(v.arity() == 0);
            bv_t ret;
            assert(sizeof(bv_t) == 8);
            if (Z3_get_numeral_uint64(context_, m.get_const_interp(v), &ret) == Z3_TRUE)
              logical_input_map_[exprToLogicalAddress(v.name().str())] = ret;
            else
              assert(m.get_const_interp(v).is_bool() && "Type conversion error during the model extraction.");
          }
          return Continue;
        }
        case unsat:
        {
          DEBUG(cout << "normal unsat\n");
          --j;
          break;
        }
        default: {
          DEBUG(cout << "normal unknown\n");
          --j;
        }
        }
      }
    } else
      --j;

  }
  return ProgramSafe;
}

index_t StateManager::exprToLogicalAddress(string e) {
  if (e.find(sym_prefix_) == 0) {
    index_t ret;
    stringstream(e.substr(sym_prefix_.size())) >> ret;
    return ret;
  }
  assert(0 && "StateManager: cannot convert it to a logical address");
}

void StateManager::prepareForNextIteration() {
  path_constraint_.clear();
  symbolic_store_.clear();
  local_address_stack_.clear();
  phi_map_.clear();
  sym_cnt_ = 1;
  ++runs_;
  bmc_no_ = 1;
}

//void StateManager::restart() {
//  logical_input_map_.clear();
//  path_constraint_.clear();
//  symbolic_store_.clear();
//  branch_history_.clear();
//  runs_ = 1;
//  sym_cnt_ = 1;
//  fresh_no_ = 1;
//}

void StateManager::printStatistics() {
  outs() << "------------------------ Statistics ------------------------\n";
  outs() << "runs = " << runs_ << "\n";
  outs() << "time = "
      << std::chrono::duration_cast<std::chrono::seconds>(end_ - start_).count() << "s ("
      << std::chrono::duration_cast<std::chrono::milliseconds>(end_ - start_).count() << "ms).\n";
  outs() <<"covered "<<getBranchCovered()<<" branches out of " <<total_br_<<" branches."<<"\n";
//  printBranchCoverage_();
  outs() << "-------------------------------------------------------------\n";
}

bool StateManager::coveredAllOtherDestinations_(llvm::BasicBlock* Dest) {
  auto i = path_constraint_.size();
//  DEBUG(
//  cout << "pc size  = " << path_constraint_.size() << "\n";
//  cout << "i = " << i << "\n";
//  cout << "size = " << branch_history_.size() << "\n";
//  cout << "siz2e = " << branch_history_[i]->Dests.size() << "\n";
//  for (auto& kvp : branch_history_[i]->Dests) {
//  	llvm::errs() << "first = " << kvp.first->getName() << "\n";
//  }
//  );
  for (auto& kvp : branch_history_[i]->Dests)
    if (kvp.first != Dest && !kvp.second)
      return false;
  return true;
}

bool StateManager::coveredAllOtherCases_(index_t current_case) {
  auto i = path_constraint_.size() - 1;

  index_t idx = 0;
  for (auto kvp = branch_history_[i]->switchmap.begin(), E = branch_history_[i]->switchmap.end();
      kvp != E; ++kvp, ++idx) {
    if (idx != current_case && !kvp->first)
      return false;
  }
  return true;
}

void StateManager::coverDestination(llvm::BasicBlock* Dest) {
  auto i = path_constraint_.size();

  auto init_part = getBigAnd(init_part_formula_, context_);
  vector<z3::expr> block_formula;
  assert(ctx_map_edom_.size() == ctx_expr_map_edom_.size());
  for (auto& kvp : ctx_map_edom_) {
    block_formula.push_back(ctx_map_edom_.at(kvp.first) == getBigOr(ctx_expr_map_edom_[kvp.first], context_));
  }
  for (auto& kvp : formula_map_edom_) {
    block_formula.push_back(getBigAnd(kvp.second, context_));
  }
  auto formula = init_part && getBigAnd(block_formula, context_);
  assert(ctx_expr_map_dest_.find(Dest) != ctx_expr_map_dest_.end());
  auto dest_formula = getBigOr(ctx_expr_map_dest_[Dest], context_);

  if (i < branch_history_.size()) {
    if (branch_history_[i]->to_cover != Dest) {
      stringstream err_msg("coverDestination: Path predication fails.\n");
      err_msg << "Predict to cover " << branch_history_[i]->to_cover->getName().data()
          << ", but actually visit " << Dest->getName().data() << "\n";
      throw std::runtime_error(err_msg.str());
    }
    if (i == branch_history_.size() - 1 && coveredAllOtherDestinations_(Dest))
      branch_history_[i]->done = true;

    branch_history_[i]->Dests[Dest] = true;
    branch_history_[i]->ctxmap = currBranchInfo_->ctxmap;

  } else {
  	currBranchInfo_->Dests[Dest] = true;
  	currBranchInfo_->to_cover = Dest;


//    addPathConstraint(formula, dest_formula);
  	branch_history_.push_back(std::move(currBranchInfo_));

  	DEBUG(
    errs().changeColor(llvm::raw_ostream::RED);
errs() << "coverDestination: Dest = " << Dest->getName() << "\n";
errs() << "path_constraints's size = " << path_constraint_.size() << "\n";
errs() << "branch_history's size = " << branch_history_.size() << "\n";
errs().resetColor() << "\n";);
  }
  addPathConstraint(formula, dest_formula);

}

void StateManager::doBMC(llvm::BasicBlock* Dominator) {
  if (dominatorVisited_.find(Dominator) == dominatorVisited_.end()) {
    auto Fn = Dominator->getParent();
    // Compute dominatees and dominators.
    llvm::DominatorTreeAnalysis DTA;
    llvm::DominatorTree dt = std::move(DTA.run(*Fn));

//    SmallVector<BasicBlock *, 16> DominatedBBs;
//    for (auto SI = succ_begin(Dominator), E = succ_end(Dominator); SI != E; ++SI) {
//      SmallVector<BasicBlock *, 16> acc;
//      dt.getDescendants(*SI, acc);
//      DominatedBBs.append(acc.begin(), acc.end());
//    }
//    auto EDom = getEffecitveDominance(DominatedBBs);

    SmallVector<BasicBlock*, 16> EDom;
    for (auto SI = succ_begin(Dominator), E = succ_end(Dominator); SI != E; ++SI) {
      SmallVector<BasicBlock *, 16> acc;
      dt.getDescendants(*SI, acc);

      SmallVector<BasicBlock*, 16>& edomSuccBB = getEffecitveDominance(acc);
      assert (!edomSuccBB.empty());

      EDom.append(edomSuccBB.begin(), edomSuccBB.end());
    }

    TopologicalSort TS(Dominator, EDom);
    SmallVector<llvm::BasicBlock *, 16> SortedEDom = TS.sort();

    dominatorVisited_.insert(std::make_pair(Dominator, SortedEDom));
  }

  SmallVector<BasicBlock *, 16>& SortedEDom = dominatorVisited_[Dominator];
  PointerValueExtractor PVEtor(SortedEDom);
  llvm::SmallVector<llvm::Value*, 32> ptr_value_vec = PVEtor.start();

  DEBUG(errs().changeColor(llvm::raw_ostream::RED);
  errs() << "In function " << Dominator->getParent()->getName() << ", ";
  errs() << Dominator->getName() << " effectively and strictly dominates: \n";
  for (auto& BB : SortedEDom)
    errs() << BB->getName() << " ";
  errs() << "\n";
  errs().resetColor() << "\n";);

  SymStack* sstack = SymStack::getSymStack();
  auto node = sstack->toppopNode();
  z3::expr ctx = node->getSymExpr();

  BmcBuilderHelper helper(context_, bmc_no_, idx2addr_, addr2varname_, addr2varinfo_, addr2version_, ptr_value_vec,
      ctx_map_edom_, ctx_expr_map_edom_, ctx_expr_map_dest_, formula_map_edom_, SortedEDom, Dominator, ctx);

  helper.visitValue(*Dominator->getTerminator());
  for (auto& BB : SortedEDom) {
    string new_symb = BB->getName().data();
    new_symb = "BB_" + new_symb + "_b" + to_string(bmc_no_);
    ctx_map_edom_.insert(make_pair(BB, context_.bool_const(new_symb.c_str())));
    for (auto& I : *BB)
      helper.visitValue(I);
  }

  currBranchInfo_->ctxmap = ctx_expr_map_dest_;
}

void StateManager::print_branch_history(size_t i) {
  branch_history_[i]->print();
}

void StateManager::print_z3_expr(z3::expr e) {
	cout << "e = " << e << "\n";
}

void StateManager::updateBranchCoverageStatEntry(llvm::BasicBlock* BB, index_t numOfCases, index_t currCase, bool isTaken) {
  if (branch_coverage_map_.find(BB) == branch_coverage_map_.end()) {
    std::map<index_t, bool> case_map;
    for (auto i = 0; i < numOfCases; ++i)
      case_map.insert(std::make_pair(i, false));
    branch_coverage_map_.insert(std::make_pair(BB, case_map));
  }

  if (isTaken)
    branch_coverage_map_[BB][currCase] = 1;
}


int StateManager::getBranchCovered() {
  int count = 0;
  for (auto& kvp : branch_coverage_map_)
    for (auto& kvp2 : kvp.second)
      if (kvp2.second)
        count++;
  return count;
}

void StateManager::printBranchCoverage_() {
  for (auto& kvp : branch_coverage_map_) {
    cout << (kvp.first)->getName().data() << "\n";
    for (auto& kvp2 : kvp.second)
      cout << kvp2.first << " " << (kvp2.second ? "true" : "false") << "\n";
  }
}

void StateManager::updateBMCBranches(llvm::BasicBlock* Dominator, llvm::BasicBlock* Dest) {

  std::vector<llvm::BasicBlock*> visit_queue;
  visit_queue.push_back(Dominator);
  assert(dominatorVisited_.find(Dominator) != dominatorVisited_.end());
  const llvm::SmallVector<llvm::BasicBlock *, 16>& EDom = dominatorVisited_[Dominator];
  inBMCPath_(Dominator, Dest, visit_queue, EDom);
}

void StateManager::inBMCPath_(llvm::BasicBlock* current_BB,
                              llvm::BasicBlock* Dest,
                              std::vector<llvm::BasicBlock*>& visit_queue,
                              const llvm::SmallVector<llvm::BasicBlock *, 16>& EDom) {

  for (auto SI = succ_begin(current_BB), E = succ_end(current_BB); SI != E; ++SI) {
    llvm::BasicBlock* new_current_BB = *SI;
    if (std::find(EDom.begin(), EDom.end(), new_current_BB) == EDom.end() && new_current_BB != Dest)
      continue;

    visit_queue.push_back(new_current_BB);
    if (new_current_BB == Dest) {
      //trace back and update branch table
      updateBMCBranchCoverageTable_(visit_queue);
    } else {
      inBMCPath_(new_current_BB, Dest, visit_queue, EDom);
    }
    visit_queue.pop_back();
  }
}

void StateManager::updateBMCBranchCoverageTable_(std::vector<llvm::BasicBlock*>& visit_queue){
  for (auto iter = visit_queue.begin(); iter != visit_queue.end() - 1; ++iter) {
    BasicBlock* BB = *iter;
    BasicBlock* BB_post = *(iter+1);
    if (BranchInst* br_inst = dyn_cast<BranchInst>(BB->getTerminator())) {
      if (br_inst->isUnconditional())
        continue;
//      auto FnName = BB->getParent()->getName();
//      auto BBName = BB->getName();
//      auto idx = (FnName + BBName).str();
      auto which_branch_taken = br_inst->getSuccessor(0) == BB_post;
      updateBranchCoverageStatEntry(BB, 2, which_branch_taken, true);
    } else if (SwitchInst* SI = dyn_cast<SwitchInst>(BB->getTerminator())) {
//      auto FnName = BB->getParent()->getName();
//      auto BBName = BB->getName();
      auto numOfCases = SI->getNumCases() + 1;
//      auto idx = (FnName + BBName).str();

      for (auto& Case : SI->cases()) {
        if (Case.getCaseSuccessor() == BB_post) {
          auto currCase = Case.getCaseIndex()+1;
          updateBranchCoverageStatEntry(BB, numOfCases, currCase, true);
        }
      }

      if (SI->getDefaultDest() == BB_post) {
          updateBranchCoverageStatEntry(BB, numOfCases, 0, true);
      }
    }
  }
}

void StateManager::setValue(address_t address, bv_t bv, width_t nbits) {
  if (nbits == 8)
    *((uint8_t*) address) = static_cast<uint8_t>(bv);
  else if (nbits == 16)
    *((uint16_t*) address) = static_cast<uint16_t>(bv);
  else if (nbits == 32)
    *((uint32_t*) address) = static_cast<uint32_t>(bv);
  else if (nbits == 64)
    *((uint64_t*) address) = static_cast<uint64_t>(bv);
  else {
    llvm::errs() << "setValue: Cannot width " << nbits << "\n";
    llvm::llvm_unreachable_internal("Error in setValue");
  }
}

void StateManager::reportToParentProcess(llsplat_status_t status) {
  _pipe.write(&status, sizeof(llsplat_status_t));

  // In all status, I need to pass the branch coverage info to the parent process.
  size_t number_of_bytes_to_read = 0;
  for (auto& it : branch_coverage_map_) {
    number_of_bytes_to_read += sizeof(llvm::BasicBlock*) + sizeof(size_t);
    for (auto & kvp : it.second) {
      number_of_bytes_to_read += sizeof(index_t) + sizeof(bool);
    }
  }
  _pipe.write(&number_of_bytes_to_read, sizeof(size_t));

  // Write branch_coverage_map_

  size_t size_br_cov_map = branch_coverage_map_.size();
  _pipe.write(&size_br_cov_map, sizeof(size_br_cov_map));

  for (auto& it : branch_coverage_map_) {
    auto BB = it.first;
    _pipe.write(&BB, sizeof(BB));

    size_t size = it.second.size();
    _pipe.write(&size, sizeof(size));
    for (auto& kvp : it.second) {
      auto idx = kvp.first;
      auto taken = kvp.second;

      _pipe.write(&idx, sizeof(idx));
      _pipe.write(&taken, sizeof(taken));
    }
  }


  // When status is Continue, I need to pass the coverage history and the logical input map to the parent process.
  if (status == Continue) {
    // Write the number of inputs
    size_t number_of_inputs = logical_input_map_.size();
    _pipe.write(&number_of_inputs, sizeof(size_t));
    // Write the number of bytes that is taken for inputs and their sizes
    number_of_bytes_to_read = number_of_inputs * sizeof(size_t);
    for(auto &it : logical_input_map_) {
      number_of_bytes_to_read += sizeof(index_t) + sizeof(bv_t);
    }
    _pipe.write(&number_of_bytes_to_read, sizeof(size_t));
    DEBUG(
        cout << "Write: number_of_inputs = " << number_of_inputs << "\n";
    cout << "Write: number_of_bytes_to_read = " << number_of_bytes_to_read << "\n";
    );
    // Write inputs
    for(auto &it : logical_input_map_) {
      index_t idx = it.first;
      _pipe.write(&idx, sizeof(idx));
      bv_t bv = it.second;
      size_t size = sizeof(bv);
      _pipe.write(&size, sizeof(size_t));
      _pipe.write(&bv, size);
    }

    // Write the length of branch history


//    number_of_bytes_to_read += sizeof(size_t); // Record the length the branch history
    size_t length_of_history = branch_history_.size();
    _pipe.write(&length_of_history, sizeof(size_t));
    number_of_bytes_to_read = 0;
    for(const auto &it : branch_history_) {
      number_of_bytes_to_read += sizeof(uint8_t); // Record the type of the node in the branch history
      if (it->is_bmc) { // BmcNode
        for (const auto& d : it->Dests) {
          number_of_bytes_to_read += sizeof(llvm::BasicBlock*) + sizeof(bool);
        }
        number_of_bytes_to_read += sizeof((size_t) it->Dests.size()) + sizeof(it->done) +
            sizeof(it->dominator) + sizeof(it->to_cover);
      } else if (it->is_switch) { // SwitchNode
        for (const auto& kvp : it->switchmap) {
          number_of_bytes_to_read += sizeof(kvp.first);
        }
        number_of_bytes_to_read += sizeof((size_t) it->switchmap.size()) + sizeof(it->done) +
            sizeof(it->BB) + sizeof(it->to_cover_case) + sizeof(it->current_case);
      } else { // BranchNode
        number_of_bytes_to_read += sizeof(it->branch) + sizeof(it->done) + sizeof(it->BB);
      }
    }
    _pipe.write(&number_of_bytes_to_read, sizeof(size_t));

//    cout << "Write branch history: length_of_history = " << length_of_history << "\n";
//    cout << "Write branch history: number_of_bytes_to_read = " << number_of_bytes_to_read << "\n";

    // Write branch history

    for(const auto &it : branch_history_) {
      if (it->is_bmc) { // BmcNode
        uint8_t type = 1;
        _pipe.write(&type, sizeof(uint8_t));
        _pipe.write(&it->done, sizeof(it->done));
        _pipe.write(&it->dominator, sizeof(it->dominator));
        _pipe.write(&it->to_cover, sizeof(it->to_cover));
        auto num_of_dests = (size_t) it->Dests.size();
        _pipe.write(&num_of_dests, sizeof(num_of_dests));
        for (auto& kvp : it->Dests) {
          auto BB = kvp.first;
          auto is_covered = kvp.second;
          _pipe.write(&BB, sizeof(llvm::BasicBlock*));
          _pipe.write(&is_covered, sizeof(bool));
        }
      } else if (it->is_switch) { // SwitchNode
        uint8_t type = 2;
        _pipe.write(&type, sizeof(uint8_t));
        _pipe.write(&it->done, sizeof(it->done));
        _pipe.write(&it->BB, sizeof(it->BB));
        _pipe.write(&it->to_cover_case, sizeof(it->to_cover_case));
        _pipe.write(&it->current_case, sizeof(it->current_case));
        auto num_of_cases = (size_t) it->switchmap.size();
        _pipe.write(&num_of_cases, sizeof(num_of_cases));
        for (auto& kvp : it->switchmap) {
          _pipe.write(&kvp.first, sizeof(bool));
        }
      } else { // BranchNode
        uint8_t type = 0;
        _pipe.write(&type, sizeof(uint8_t));
        _pipe.write(&it->branch, sizeof(it->branch));
        _pipe.write(&it->done, sizeof(it->done));
        _pipe.write(&it->BB, sizeof(it->BB));
      }
    }
  }
  _pipe.closePipe();
}

void StateManager::readBranchCoverageFromChildProcess(void* buf, size_t length_of_branch_coverage_map, size_t number_of_bytes_to_read) {
  _pipe.read(buf, number_of_bytes_to_read);
  void* current = buf;
  size_t bytesAlreadyRead = 0;

  for (auto i = 0; i < length_of_branch_coverage_map; ++i) {
    auto BB = *(llvm::BasicBlock* *) current; // size is counted by bytes.
    bytesAlreadyRead += sizeof(llvm::BasicBlock*);
    current = (llvm::BasicBlock* *) current + 1;

    auto size = *(size_t*) current;
    bytesAlreadyRead += sizeof(size_t);
    current = (size_t *) current + 1;

    map<index_t, bool> mmap;
    for (auto j = 0; j < size; ++j) {
      auto idx = *(index_t*) current;
      bytesAlreadyRead += sizeof(index_t);
      current = (index_t *) current + 1;

      auto taken = *(bool*) current;
      bytesAlreadyRead += sizeof(bool);
      current = (bool *) current + 1;

      mmap.insert(make_pair(idx, taken));
    }

    branch_coverage_map_.insert(make_pair(BB, mmap));
  }

  free (buf);
  if (bytesAlreadyRead != number_of_bytes_to_read) {
    cout << "bytesAlreadyRead = " << bytesAlreadyRead << "\n";
    cout << "number_of_bytes_to_read = " << number_of_bytes_to_read << "\n";
    assert(0 && "Pipe error: Llsplat cannot read all bytes as specified");
  }
}


void StateManager::readInputsFromChildProcess(void* buf, size_t number_of_inputs, size_t number_of_bytes_to_read) {
  _pipe.read(buf, number_of_bytes_to_read);
  void* current = buf;
  size_t bytesAlreadyRead = 0;
  DEBUG(cout << "Read inputs: number_of_inputs = " << number_of_inputs << "\n";
    cout << "Read inputs: number_of_bytes_to_read = " << number_of_bytes_to_read << "\n";);
  for (size_t i = 0; i < number_of_inputs; ++i) {
    index_t idx = *(index_t *) current; // size is counted by bytes.
    bytesAlreadyRead += sizeof(index_t);
    current = (index_t *) current + 1;

    size_t input_size = *(size_t *) current; // size is counted by bytes.
    bytesAlreadyRead += sizeof(size_t);
    current = (size_t *) current + 1;
    assert(input_size == sizeof(bv_t));
    logical_input_map_[idx] = * (bv_t *) (current); // TODO: We need to distinguish if input_value is a logical address or a primitive value.
    current = (bv_t *) current + 1;
    bytesAlreadyRead += sizeof(bv_t);
  }
  free (buf);
  if (bytesAlreadyRead != number_of_bytes_to_read) {
    cout << "bytesAlreadyRead = " << bytesAlreadyRead << "\n";
    cout << "number_of_bytes_to_read = " << number_of_bytes_to_read << "\n";
    assert(0 && "Pipe error: Llsplat cannot read all bytes as specified");;
  }
}

void StateManager::readBranchHistoryFromChildProcess(void* buf, size_t length_of_history, size_t number_of_bytes_to_read) {
  DEBUG(cout << "Read inputs: length_of_history = " << length_of_history << "\n";
    cout << "Read inputs: number_of_bytes_to_read = " << number_of_bytes_to_read << "\n";);
  _pipe.read(buf, number_of_bytes_to_read);
  void* current = buf;
  size_t bytesAlreadyRead = 0;
  for (size_t i = 0; i < length_of_history; ++i) {
    uint8_t type = *(uint8_t *) current;
    bytesAlreadyRead += sizeof(uint8_t);
    current = (uint8_t*) current + 1;
    if (type == 0) { // BranchNode
      bool branch = *(bool *) current; // size is counted by bytes.
      bytesAlreadyRead += sizeof(bool);
      current = (bool*) current + 1;
      bool done = *(bool *) current;
      bytesAlreadyRead += sizeof(bool);
      current = (bool*) current + 1;
      llvm::BasicBlock* BB = * (llvm::BasicBlock* *) current;
      bytesAlreadyRead += sizeof(llvm::BasicBlock*);
      current = (llvm::BasicBlock**) current + 1;

      auto ptr = std::unique_ptr<BranchInfo>(new BranchInfo(branch, BB));
      ptr->done = done;
      branch_history_.push_back(std::move(ptr));
    } else if (type == 1) { // BmcNode

      bool done = *(bool *) current; // size is counted by bytes.
      bytesAlreadyRead += sizeof(bool);
      current = (bool*) current + 1;
      llvm::BasicBlock* dominator = * (llvm::BasicBlock**) current;
      bytesAlreadyRead += sizeof(llvm::BasicBlock*);
      current = (llvm::BasicBlock**) current + 1;
      llvm::BasicBlock* to_cover = * (llvm::BasicBlock**) current;
      bytesAlreadyRead += sizeof(llvm::BasicBlock*);
      current = (llvm::BasicBlock**) current + 1;
      size_t num_of_dests = *(size_t *) current; // size is counted by bytes.
      bytesAlreadyRead += sizeof(size_t);
      current = (size_t*) current + 1;

      std::vector<llvm::BasicBlock*> Dests_vec;
      std::map<llvm::BasicBlock*, bool> is_covered_map;

      for (auto i = 0; i < num_of_dests; ++i) {
        llvm::BasicBlock* dest = * (llvm::BasicBlock* *) current;
        bytesAlreadyRead += sizeof(llvm::BasicBlock*);
        current = (llvm::BasicBlock**) current + 1;
        bool is_covered = *(bool *) current; // size is counted by bytes.
        bytesAlreadyRead += sizeof(bool);
        current = (bool*) current + 1;

        Dests_vec.push_back(dest);
        is_covered_map.insert(make_pair(dest, is_covered));
      }


      auto ptr = std::unique_ptr<BranchInfo>(new BranchInfo(dominator, Dests_vec));
      ptr->done = done;
      ptr->to_cover = to_cover;

      for (auto& kvp : is_covered_map) {
        ptr->Dests.at(kvp.first) = kvp.second;
      }

      branch_history_.push_back(std::move(ptr));

    } else { // SwitchNode
      bool done = *(bool *) current; // size is counted by bytes.
      bytesAlreadyRead += sizeof(bool);
      current = (bool*) current + 1;
      llvm::BasicBlock* BB = * (llvm::BasicBlock* *) current;
      bytesAlreadyRead += sizeof(llvm::BasicBlock*);
      current = (llvm::BasicBlock**) current + 1;
      index_t to_cover_case = * (index_t *) current;
      bytesAlreadyRead += sizeof(index_t);
      current = (index_t*) current + 1;
      index_t current_case = * (index_t *) current;
      bytesAlreadyRead += sizeof(index_t);
      current = (index_t*) current + 1;
      size_t num_of_cases = *(size_t *) current; // size is counted by bytes.
      bytesAlreadyRead += sizeof(size_t);
      current = (size_t*) current + 1;

      std::vector<z3::expr> vec;
      for (auto i = 0; i < num_of_cases; ++i) {
        bool is_covered = *(bool *) current; // size is counted by bytes.
        bytesAlreadyRead += sizeof(bool);
        current = (bool*) current + 1;
        vec.push_back(context_.bool_val(is_covered)); // This is a placeholder for the i-th case.
      }

      auto ptr = std::unique_ptr<BranchInfo>(new BranchInfo(current_case, vec, BB));
      ptr->done = done;
      ptr->to_cover_case = to_cover_case;

      branch_history_.push_back(std::move(ptr));
    }

  }
  free (buf);
  if (bytesAlreadyRead != number_of_bytes_to_read) {
    cout << "bytesAlreadyRead = " << bytesAlreadyRead << "\n";
    cout << "number_of_bytes_to_read = " << number_of_bytes_to_read << "\n";
    assert(0 && "Pipe error: Llsplat cannot read all bytes as specified");
//    throw LlsplatException("Pipe error: Llsplat cannot read all bytes as specified");
  }

}

llsplat_status_t StateManager::waiting() {
  llsplat_status_t status;
  _pipe.read(&status, sizeof(llsplat_status_t));
  resetParent();
  // Read branch coverage map

  size_t number_of_items= 0;
  size_t number_of_bytes_to_read= 0;
  _pipe.read(&number_of_bytes_to_read, sizeof(size_t));
  _pipe.read(&number_of_items, sizeof(size_t));
  DEBUG(cout << "### Read: number_of_items = " << number_of_items << "\n";
  cout << "### Read: number_of_bytes_to_read = " << number_of_bytes_to_read << "\n";);
  uint8_t* buf = (uint8_t *) malloc(number_of_bytes_to_read);
  readBranchCoverageFromChildProcess(buf, number_of_items, number_of_bytes_to_read);


  if(status == Continue) {
//    prepareForNextIteration();
    // Read inputs for the next iteration.
    size_t number_of_inputs= 0;
    size_t number_of_bytes_to_read= 0;
    _pipe.read(&number_of_inputs, sizeof(size_t));
    _pipe.read(&number_of_bytes_to_read, sizeof(size_t));
    DEBUG(cout << "### Read: number_of_inputs = " << number_of_inputs << "\n";
    cout << "### Read: number_of_bytes_to_read = " << number_of_bytes_to_read << "\n";);
    uint8_t* buf = (uint8_t *) malloc(number_of_bytes_to_read);
    readInputsFromChildProcess(buf, number_of_inputs, number_of_bytes_to_read);

    // Read the predicted branch history for the next iteration.
    size_t length_of_history= 0;
    number_of_bytes_to_read= 0;
    _pipe.read(&length_of_history, sizeof(size_t));
    _pipe.read(&number_of_bytes_to_read, sizeof(size_t));
//    cout << "### Read: length_of_history = " << length_of_history << "\n";
//    cout << "### Read: number_of_bytes_to_read = " << number_of_bytes_to_read << "\n";
    buf = (uint8_t *) malloc(number_of_bytes_to_read);
    readBranchHistoryFromChildProcess(buf, length_of_history, number_of_bytes_to_read);
  }
  return status;
}

