#!/bin/bash
#*******************************************************************************
# This file is distributed under the MIT License. See LICENSE.TXT for details.
#*******************************************************************************

################################################################################
#
# Builds and installs LLSPLAT in BASE_DIR (see shell var below in settings).
#
# Requirements (see "Install required packages" below):
# - git
# - python
# - clang
# - clang++
#
################################################################################

# Exit on error
set -e

################################################################################

# Settings

# Set these flags to control various installation options
INSTALL_Z3=1
INSTALL_LLVM=1
INSTALL_LLSPLAT=1
BUILD_RELEASE_LLVM=1
BUILD_RELEASE_LLSPLAT=0

# Change this to the desired path (default uses working-dir/llsplat-project)
BASE_DIR=`pwd`/llsplat-project

# Other dirs
Z3_DIR="${BASE_DIR}/z3"
LLVM_DIR="${BASE_DIR}/llvm-3.6"
LLSPLAT_DIR="${BASE_DIR}/llsplat"

# Setting colors
textcolor='\033[0;35m'
nocolor='\033[0m'

################################################################################

# Set up base directory for everything
mkdir -p ${BASE_DIR}
cd ${BASE_DIR}

################################################################################

# Z3

if [ ${INSTALL_Z3} -eq 1 ]; then

echo -e "${textcolor}*** LLSPLAT BUILD: Installing Z3 ***${nocolor}"

mkdir -p ${Z3_DIR}/src
mkdir -p ${Z3_DIR}/install

# Get Z3
cd ${Z3_DIR}/src/
curl -O "http://download-codeplex.sec.s-msft.com/Download/SourceControlFileDownload.ashx?ProjectName=z3&changeSetId=cee7dd39444c9060186df79c2a2c7f8845de415b"
unzip -o SourceControlFileDownload*
rm -f SourceControlFileDownload*

# Configure Z3 and build
cd ${Z3_DIR}/src/
python scripts/mk_make.py --prefix=${Z3_DIR}/install
cd build
make
make install

cd ${BASE_DIR}

echo -e "${textcolor}*** LLSPLAT BUILD: Installed Z3 ***${nocolor}"

fi

################################################################################

# LLVM

if [ ${INSTALL_LLVM} -eq 1 ]; then

echo -e "${textcolor}*** LLSPLAT BUILD: Installing LLVM ***${nocolor}"

mkdir -p ${LLVM_DIR}/src
mkdir -p ${LLVM_DIR}/install

# Get llvm and extract
curl -O http://llvm.org/releases/3.6.0/llvm-3.6.0.src.tar.xz
curl -O http://llvm.org/releases/3.6.0/cfe-3.6.0.src.tar.xz
curl -O http://llvm.org/releases/3.6.0/libcxx-3.6.0.src.tar.xz
curl -O http://llvm.org/releases/3.6.0/compiler-rt-3.6.0.src.tar.xz

tar -C ${LLVM_DIR}/src -xvf llvm-3.6.0.src.tar.xz --strip 1
mkdir -p ${LLVM_DIR}/src/tools/clang
tar -C ${LLVM_DIR}/src/tools/clang -xvf cfe-3.6.0.src.tar.xz --strip 1
# mkdir -p ${LLVM_DIR}/src/projects/libcxx
# tar -C ${LLVM_DIR}/src/projects/libcxx -xvf libcxx-3.6.0.src.tar.xz --strip 1
mkdir -p ${LLVM_DIR}/src/projects/compiler-rt
tar -C ${LLVM_DIR}/src/projects/compiler-rt -xvf compiler-rt-3.6.0.src.tar.xz --strip 1

# Configure llvm and build
if [ ${BUILD_RELEASE_LLVM} -eq 1 ]; then
    echo -e "${textcolor}*** LLSPLAT BUILD: Building Release Version ***${nocolor}"
    mkdir -p ${LLVM_DIR}/build
    cd ${LLVM_DIR}/build/
    cmake -DCMAKE_BUILD_TYPE=Release -DLLVM_ENABLE_ASSERTIONS=ON -DCMAKE_INSTALL_PREFIX="${LLVM_DIR}/install" ${LLVM_DIR}/src
else
    echo -e "${textcolor}*** LLSPLAT BUILD: Building Debug Version ***${nocolor}"
    mkdir -p ${LLVM_DIR}/build_debug
    cd ${LLVM_DIR}/build_debug/
    cmake -DCMAKE_BUILD_TYPE=Debug -DLLVM_ENABLE_ASSERTIONS=ON -DCMAKE_INSTALL_PREFIX="${LLVM_DIR}/install" ${LLVM_DIR}/src
fi

make
make install
cd ${BASE_DIR}

echo -e "${textcolor}*** LLSPLAT BUILD: Installed LLVM ***${nocolor}"

fi

################################################################################

# LLSPLAT

if [ ${INSTALL_LLSPLAT} -eq 1 ]; then

echo -e "${textcolor}*** LLSPLAT BUILD: Installing LLSPLAT ***${nocolor}"

mkdir -p ${LLSPLAT_DIR}/src
mkdir -p ${LLSPLAT_DIR}/install

# Get LLSPLAT
git clone https://zilongwang@bitbucket.org/zilongwang/llsplat-src.git ${LLSPLAT_DIR}/src/

# Configure LLSPLAT and build
if [ ${BUILD_RELEASE_LLSPLAT} -eq 1 ]; then
    echo -e "${textcolor}*** LLSPLAT BUILD: Building Release Version ***${nocolor}"
    mkdir -p ${LLSPLAT_DIR}/build
    cd ${LLSPLAT_DIR}/build/
    cmake -DLLVM_DIR=${LLVM_DIR}/install/share/llvm/cmake \
        -DCMAKE_CXX_COMPILER="${LLVM_DIR}/install/bin/clang++" \
        -DCMAKE_C_COMPILER="${LLVM_DIR}/install/bin/clang" \
        -DCMAKE_INCLUDE_PATH="${Z3_DIR}/install/include" \
        -DCMAKE_BUILD_TYPE=Release \
        -DLLVM_ENABLE_ASSERTIONS=ON \
        -DCMAKE_INSTALL_PREFIX="${LLSPLAT_DIR}/install" \
        ${LLSPLAT_DIR}/src
else
    echo -e "${textcolor}*** LLSPLAT BUILD: Building Debug Version ***${nocolor}"
    mkdir -p ${LLSPLAT_DIR}/build_debug
    cd ${LLSPLAT_DIR}/build_debug/
    cmake -DLLVM_DIR=${LLVM_DIR}/install/share/llvm/cmake \
        -DCMAKE_CXX_COMPILER="${LLVM_DIR}/install/bin/clang++" \
        -DCMAKE_C_COMPILER="${LLVM_DIR}/install/bin/clang" \
        -DCMAKE_INCLUDE_PATH="${Z3_DIR}/install/include" \
        -DCMAKE_BUILD_TYPE=Debug \
        -DLLVM_ENABLE_ASSERTIONS=ON \
        -DCMAKE_INSTALL_PREFIX="${LLSPLAT_DIR}/install" \
        ${LLSPLAT_DIR}/src
fi

make
make install
cd ${BASE_DIR}

echo -e "${textcolor}*** LLSPLAT BUILD: Installed LLSPLAT ***${nocolor}"

fi

# Set required paths and environment variables
echo -e "${textcolor}*** LLSPLAT BUILD: Please set required environment variables. ***${nocolor}"
echo "################ LLSPLAT SETUP START ###############"
echo "export BASE_DIR=${BASE_DIR}"
echo "export Z3_DIR=${Z3_DIR}"
echo "export LLVM_DIR=${LLVM_DIR}"
echo "export LLSPLAT_DIR=${LLSPLAT_DIR}"
echo "export LLSPLAT_SOURCE_DIR=${LLSPLAT_DIR}/src"
if [ ${BUILD_RELEASE_LLSPLAT} -eq 1 ]; then
    echo "export LLSPLAT_BUILD_DIR=${LLSPLAT_DIR}/build"
else
    echo "export LLSPLAT_BUILD_DIR=${LLSPLAT_DIR}/build_debug"
fi
echo "export PATH=${LLVM_DIR}/install/bin":\${PATH}

if [[ "$OSTYPE" == "linux-gnu" ]]; then
  # Linux
  echo "export LD_LIBRARY_PATH=${LD_LIBRARY_PATH}:${Z3_DIR}/install/lib"
elif [[ "$OSTYPE" == "darwin"* ]]; then
  # Mac OSX
  echo "export DYLD_LIBRARY_PATH=${DYLD_LIBRARY_PATH}:${Z3_DIR}/install/lib"
else
  echo "$OSTYPE is not supported yet." 1>&2
  exit 1
fi

echo "################  LLSPLAT SETUP END  ###############"

echo -e "${textcolor}*** LLSPLAT BUILD: You have to set the required environment variables! ***${nocolor}"

