// RUN: %instrument %s -o %t.ll
// RUN: %instrumenter %t.ll
// RUN: %compile
// RUN: %t.exe %ARGS | FileCheck %s

#include "instrument.h"

void llsplatInstrumented(){
  int a;
  int b;
  llsplatInitInput((address_t) &a);
  llsplatInitInput((address_t) &b);

  if (a == b - 19) {
    a = b + a;
  }
  else {
    a = b + 1;
  }

  int c = a;
  return ;

  // CHECK: Llsplat is over. The program is safe. :-)
  // CHECK: runs = 1
}

