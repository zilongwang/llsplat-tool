// RUN: %instrument %s -o %t.ll
// RUN: %instrumenter %t.ll
// RUN: %compile
// RUN: %t.exe %ARGS | FileCheck %s

#include "instrument.h"
/* #include <assert.h> */
#include <stdio.h>

void llsplatInstrumented() {
  int a,b,c, match=0;
  llsplatInitInput((address_t) &a);
  llsplatInitInput((address_t) &b);
  llsplatInitInput((address_t) &c);
  // filtering out invalid inputs
  if (a <= 0 || b<= 0 || c<= 0)
    return;
  printf("a,b,c = %d,%d,%d:",a,b,c);
  //0: Equilateral, 1:Isosceles,
  // 2: Not a traiangle, 3:Scalene
  int result = -1;
  if (a == b)
    match = match + 1;
  if (a == c)
    match = match + 2;
  if (b == c)
    match = match + 3;
  if (match == 0) {
    if (a + b <= c)
      result = 2;
    else if (b + c <= a)
      result = 2;
    else if (a + c <= b)
      result = 2;
    else
      result = 3;
  } else {
    if (match == 1) {
      if (a + b <= c)
        result = 2;
      else
        result = 1;
    } else {
      if (match == 2) {
        if (a + c <= b)
          result = 2;
        else
          result = 1;
      } else {
        if (match == 3) {
          if (b + c <= a)
            result = 2;
          else
            result = 1;
        } else
          result = 0;
      }
    }
  }

  printf("result=%d\n",result);
  /* assert(result != 1); */

  // CHECK: Llsplat is over. The program is safe. :-)
  // CHECK: runs = 2

}
