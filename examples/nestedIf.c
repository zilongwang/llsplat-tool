// RUN: %instrument %s -o %t.ll
// RUN: %instrumenter %t.ll
// RUN: %compile
// RUN: %t.exe %ARGS | FileCheck %s

#include "instrument.h"
#include <assert.h>

void llsplatInstrumented() {
  int a;
  int b;
  llsplatInitInput((address_t) &a);
  llsplatInitInput((address_t) &b);

  int *p;
  int c;
  if (a == b - 19) {
    if (a == 10) {
      p = &a;
      a = b + 2;
    }
    if (b == 30) {
      c = a + b;
    }
  }
  else {
    p = &a;
    a = b + 1;
  }

  assert(a != b + 2);
  c = 1;
  return ;

  // CHECK: Llsplat: in 3th run  Instrumented code returned on abort.
  // CHECK: runs = 3
}

