// RUN: %instrument %s -o %t.ll
// RUN: %instrumenter %t.ll
// RUN: %compile %t.instr.bc -o %t.exe
// RUN: %t.exe -bmc=%t.ll

#include "instrument.h"
#include "assert.h"

void llsplatInstrumented(){
  char a;
  int b;
  llsplatInitInput((address_t) &a);
  llsplatInitInput((address_t) &b);

  if (a == 'a') {
    a = b + 10;
  }
  else {
    a = b;
  }

  assert(a != b + 10);
  return ;
}

